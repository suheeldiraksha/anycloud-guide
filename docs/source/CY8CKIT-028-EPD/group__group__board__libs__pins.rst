=====
Pins
=====

.. toctree::
   
   group__group__board__libs__microphone.rst
   group__group__board__libs__motion.rst
   group__group__board__libs__temp.rst
   group__group__board__libs__display.rst


.. doxygengroup:: group_board_libs_pins
   :project: CY8CKIT-028-EPD
   :members:
   :protected-members:
   :private-members:
   :undoc-members: