==============
E-Ink Display
==============

      The display is handled by the display-eink-e2271cs021 library,
      details are available at
      https://github.com/cypresssemiconductorco/display-eink-e2271cs021.
