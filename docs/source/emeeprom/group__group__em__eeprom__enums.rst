=================
Enumerated Types
=================

.. doxygengroup:: group_em_eeprom_enums
   :project: emeeprom
   :members:
   :protected-members:
   :private-members:
   :undoc-members: