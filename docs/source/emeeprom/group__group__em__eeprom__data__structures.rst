================
Data Structures
================

.. doxygengroup:: group_em_eeprom_data_structures
   :project: emeeprom
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
   

   structcy__stc__eeprom__config__t.rst
   structcy__stc__eeprom__context__t.rst
   
