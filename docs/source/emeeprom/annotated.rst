=========================================================
Cypress Em_EEPROM Middleware Library 2.0: Data Structures
=========================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress Em_EEPROM Middleware   |
      |                                   |    Library 2.0                    |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         Here are the data structures with brief descriptions:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  C\ `c                           | Em_EEPROM configuration          |
         | y_stc_eeprom_config_t <structcy_ | structure                        |
         | _stc__eeprom__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_                         | The Em_EEPROM context data       |
         | stc_eeprom_context_t <structcy__ | structure                        |
         | stc__eeprom__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
