============================================================================
Data Structures
============================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         Here are the data structures with brief descriptions:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_dfu_enter_t <struc   | Only used inside DFU             |
         | tcy__stc__dfu__enter__t.html>`__ | Command_Enter()                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_dfu_params_t <struct | Working parameters for some DFU  |
         | cy__stc__dfu__params__t.html>`__ | SDK APIs to be initialized       |
         |                                  | before calling DFU API           |
         +----------------------------------+----------------------------------+


