=================
Enumerated Types
=================

.. doxygengroup:: group_dfu_enums
   :project: dfu
   :members:
   :protected-members:
   :private-members:
   :undoc-members: