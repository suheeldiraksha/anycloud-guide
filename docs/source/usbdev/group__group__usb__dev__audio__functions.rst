==========
Functions
==========

.. doxygengroup:: group_usb_dev_audio_functions
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members: