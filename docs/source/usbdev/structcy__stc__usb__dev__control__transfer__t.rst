=========================================
cy_stc_usb_dev_control_transfer_t Struct
=========================================

.. doxygenstruct:: cy_stc_usb_dev_control_transfer_t
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
