====================================
cy_stc_usb_dev_cdc_context_t Struct
====================================

.. doxygenstruct:: cy_stc_usb_dev_cdc_context_t
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
