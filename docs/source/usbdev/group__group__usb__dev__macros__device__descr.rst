============
Descriptors
============

.. doxygengroup:: group_usb_dev_macros_device_descr
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members: