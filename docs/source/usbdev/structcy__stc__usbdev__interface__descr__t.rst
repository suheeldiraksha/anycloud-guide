=================================================
cy_stc_usbdev_interface_descr_t Struct Reference
=================================================

.. doxygenstruct:: cy_stc_usbdev_interface_descr_t
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
