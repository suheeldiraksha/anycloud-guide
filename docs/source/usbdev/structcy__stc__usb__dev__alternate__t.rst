============================================
cy_stc_usb_dev_alternate_t Struct Reference
============================================

.. doxygenstruct:: cy_stc_usb_dev_alternate_t  
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
