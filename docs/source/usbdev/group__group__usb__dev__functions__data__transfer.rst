========================
Data Transfer Functions
========================

.. doxygengroup:: group_usb_dev_functions_data_transfer
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
