=================
Enumerated Types
=================

.. doxygengroup:: group_usb_dev_cdc_enums
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members: