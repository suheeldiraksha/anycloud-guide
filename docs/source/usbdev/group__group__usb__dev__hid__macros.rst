=======
Macros
=======

.. doxygengroup:: group_usb_dev_hid_macros
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members: