=================
Enumerated Types
=================

.. doxygengroup:: group_usb_dev_hid_enums
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
