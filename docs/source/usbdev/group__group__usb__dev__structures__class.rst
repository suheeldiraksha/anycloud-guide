==============
Class Support
==============


.. doxygengroup:: group_usb_dev_structures_class
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::

   structcy__stc__usb__dev__class__t.rst
   structcy__stc__usb__dev__class__ll__item__t.rst
   

