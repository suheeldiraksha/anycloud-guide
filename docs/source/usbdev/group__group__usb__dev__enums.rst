=================
Enumerated Types
=================

.. doxygengroup:: group_usb_dev_enums
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members: