=================================
cy_stc_usb_dev_endpoint_t Struct
=================================

.. doxygenstruct:: cy_stc_usb_dev_endpoint_t
   :project: usbdev
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
