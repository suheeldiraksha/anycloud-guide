==================================================================
Cypress USB Device Middleware Library 2.0: Data Fields - Variables
==================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress USB Device Middleware  |
      |                                   |    Library 2.0                    |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  alternates :
         `cy_stc_usb_dev_interface_t <structcy__stc__usb__dev__interface__t.html#af49504d16931dee60e726c4496ce3282>`__

      .. rubric:: - b -
         :name: b--

      -  bAlternateSetting :
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#abdaec91851978ad55037fef4a986e4ff>`__
      -  bcdDevice :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#ac6163f32cdd505a3c79bff9cb0798902>`__
      -  bcdUSB :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#ac56838bd4119bb5094df9468e6a5c6ca>`__
      -  bConfigurationValue :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#a28290fc090d1300ce689675dbf03d516>`__
      -  bDescriptorType :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#aff0680f735725beac0d5864faddb54d3>`__
         ,
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#ae8fc0e6c17397e9f19e76b2d52cab4a8>`__
         ,
         `cy_stc_usbdev_endpoint_descr_t <structcy__stc__usbdev__endpoint__descr__t.html#a0c1a90e2ae955eee9543879e5a664e6d>`__
         ,
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#aa37e85cf76e7095200b6fd4e294436f7>`__
      -  bDeviceClass :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#aaf704494fc8e026e78af2abbc9c81e32>`__
      -  bDeviceProtocol :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#af5ae81fcf8bd3f21d118deb05c64b99c>`__
      -  bDeviceSubClass :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#a688f054dcbda5a8afcc5e0f03a01989d>`__
      -  bEndpointAddress :
         `cy_stc_usbdev_endpoint_descr_t <structcy__stc__usbdev__endpoint__descr__t.html#a02f288a3116dfef6e82d25796fa7ebef>`__
      -  bInterfaceClass :
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#abe8e150e34fa7dcdbe2b0af11ca27477>`__
      -  bInterfaceNumber :
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#a65aebee33eca7791ad41ac8347868221>`__
      -  bInterfaceProtocol :
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#a010297774bd7ae3f718ee98568748898>`__
      -  bInterfaceSubClass :
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#ab41a710838240522c1860420f9924a67>`__
      -  bInterval :
         `cy_stc_usbdev_endpoint_descr_t <structcy__stc__usbdev__endpoint__descr__t.html#ae48eec6ba3bae7f7c1e6f3ed7de2cfdf>`__
      -  bLength :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#a5e57a1c41471aabd2add6513974f0464>`__
         ,
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#aecbd813046544b9250117a0566aadd14>`__
         ,
         `cy_stc_usbdev_endpoint_descr_t <structcy__stc__usbdev__endpoint__descr__t.html#a443da99568684d1973b5424a0c89a29e>`__
         ,
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#ab011bb57b0b7abe3586579dd9172cf23>`__
      -  bmAttributes :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#afcb2b8c7eea77e13d12b25e34640bab8>`__
         ,
         `cy_stc_usbdev_endpoint_descr_t <structcy__stc__usbdev__endpoint__descr__t.html#a94505d0dbed1f7b18fb138aab0f55756>`__
      -  bMaxPacketSize :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#acd3e640f123462bdead0e5df36723f5c>`__
      -  bMaxPower :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#a66639e32037402566bf51b27936b1317>`__
      -  bmRequestType :
         `cy_stc_usb_dev_setup_packet_t <structcy__stc__usb__dev__setup__packet__t.html#ab9cb2efd5a3d00e9ac03300af3595495>`__
      -  bNumConfigurations :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#ac31afb5eb41cec65609c2c290877b268>`__
      -  bNumEndpoints :
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#a75309a6a1b724a3a5c2bf57ab05a4e9c>`__
      -  bNumInterfaces :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#a8e0f1164631005d3ab62e9617bb47550>`__
      -  bosDescriptor :
         `cy_stc_usb_dev_device_t <structcy__stc__usb__dev__device__t.html#a5ca8d248ab89bb4cccdab2f15c3fb0c8>`__
      -  bRequest :
         `cy_stc_usb_dev_setup_packet_t <structcy__stc__usb__dev__setup__packet__t.html#a0842936f9d00ef0f75c28afe9827dda3>`__
      -  buffer :
         `cy_stc_usb_dev_cdc_config_t <structcy__stc__usb__dev__cdc__config__t.html#ae96d5c2ad16b940ab4f9d1847754fc13>`__
         ,
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#af228b416d7c0725c487a258074236880>`__
      -  bufferSize :
         `cy_stc_usb_dev_cdc_config_t <structcy__stc__usb__dev__cdc__config__t.html#a846a7527e69e3b4e77f39bd0f7e2ac7b>`__
         ,
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#add72c6a43cd0be4fea770a6bf0555352>`__
      -  busReset :
         `cy_stc_usb_dev_class_t <structcy__stc__usb__dev__class__t.html#a503cd36eebb5c1e3b12d75028a2481ba>`__

      .. rubric:: - c -
         :name: c--

      -  classData :
         `cy_stc_usb_dev_class_ll_item_t <structcy__stc__usb__dev__class__ll__item__t.html#af20af14f197b59640794d4eb6ccadbf2>`__
      -  classObj :
         `cy_stc_usb_dev_class_ll_item_t <structcy__stc__usb__dev__class__ll__item__t.html#ab664a4c86bee67eb0b4f995cdb63cdc3>`__
      -  configDescriptor :
         `cy_stc_usb_dev_configuration_t <structcy__stc__usb__dev__configuration__t.html#a6189ea5773ef735287936e3e7b05e539>`__
      -  configurations :
         `cy_stc_usb_dev_device_t <structcy__stc__usb__dev__device__t.html#a34f2a84a67e79a5eda6ce2205684070c>`__

      .. rubric:: - d -
         :name: d--

      -  deviceDescriptor :
         `cy_stc_usb_dev_device_t <structcy__stc__usb__dev__device__t.html#a685c419bd8298dc6715cd133e975be5d>`__
      -  direction :
         `cy_stc_usb_dev_bm_request <structcy__stc__usb__dev__bm__request.html#a8cbda434a4858f6bcbc1afa2eda2f2cd>`__
         ,
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#a9c8f84635ed0847187969f097179e85f>`__

      .. rubric:: - e -
         :name: e--

      -  enableWindowsOsDescriptor :
         `cy_stc_usb_dev_string_t <structcy__stc__usb__dev__string__t.html#a02e123dfb487a9e22fe716e374f3bc73>`__
      -  endpointDescriptor :
         `cy_stc_usb_dev_endpoint_t <structcy__stc__usb__dev__endpoint__t.html#ae9d4c2900e45fccbbe3eb56a7fcbf0f0>`__
      -  endpoints :
         `cy_stc_usb_dev_alternate_t <structcy__stc__usb__dev__alternate__t.html#a3fe8acef267c90b9071112b651f34ae1>`__
      -  endpointsMask :
         `cy_stc_usb_dev_interface_t <structcy__stc__usb__dev__interface__t.html#a8415563edb976c4ae2d7c93743182a10>`__
      -  ep0Buffer :
         `cy_stc_usb_dev_config_t <structcy__stc__usb__dev__config__t.html#a7adc52048bd8d3e28786b1d6348fc789>`__
      -  ep0BufferSize :
         `cy_stc_usb_dev_config_t <structcy__stc__usb__dev__config__t.html#a50c257d5b2319ed93596121084293db6>`__
      -  extCompatIdDescriptor :
         `cy_stc_usb_dev_ms_os_string_t <structcy__stc__usb__dev__ms__os__string__t.html#aa129d3d88701b9cbaf057be63ffc6a25>`__
      -  extPropertiesDescriptor :
         `cy_stc_usb_dev_ms_os_string_t <structcy__stc__usb__dev__ms__os__string__t.html#a4987b03af76a60ba4181fa6f0d393f2c>`__

      .. rubric:: - h -
         :name: h--

      -  hid :
         `cy_stc_usb_dev_alternate_t <structcy__stc__usb__dev__alternate__t.html#a795a71c8dbc0452ad693edf2ea29a939>`__
      -  hidDescriptor :
         `cy_stc_usb_dev_hid_t <structcy__stc__usb__dev__hid__t.html#a926e62d147abf63ad183d7892b051d78>`__

      .. rubric:: - i -
         :name: i--

      -  iConfiguration :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#ac2e058ba9bf10e7609d6ec7ad2acb338>`__
      -  idProduct :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#afa761db25b797feb1aba98a02c5e27a3>`__
      -  idVendor :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#abbc0f9e538526d6e3b1945275418d1cc>`__
      -  iInterface :
         `cy_stc_usbdev_interface_descr_t <structcy__stc__usbdev__interface__descr__t.html#a4776c5bec4daa50f11789c9879c8cb8a>`__
      -  iManufacturer :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#a61f92b9a09bc8729c445cd11a7c4951a>`__
      -  inputReportIdx :
         `cy_stc_usb_dev_hid_t <structcy__stc__usb__dev__hid__t.html#af5bacc172028ab6342ef2e7a24bbadb5>`__
      -  inputReportIdxSize :
         `cy_stc_usb_dev_hid_t <structcy__stc__usb__dev__hid__t.html#aa1a4adec2e0033e4ac18683bd11c19bb>`__
      -  inputReportPos :
         `cy_stc_usb_dev_hid_t <structcy__stc__usb__dev__hid__t.html#a67be9900d3911f977019e54b906ae8cb>`__
      -  interfaceDescriptor :
         `cy_stc_usb_dev_alternate_t <structcy__stc__usb__dev__alternate__t.html#ad9a20b7d91d73ee6a4cbaf72694a0ecc>`__
      -  interfaces :
         `cy_stc_usb_dev_configuration_t <structcy__stc__usb__dev__configuration__t.html#a84e2ae881e023b0719437251db69ee3c>`__
      -  iProduct :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#a0a48ffc9d33d53a530adef55bf139ee0>`__
      -  iSerialNumber :
         `cy_stc_usbdev_device_descr_t <structcy__stc__usbdev__device__descr__t.html#a01e1fc974ba95d20cb473298ae976d3f>`__

      .. rubric:: - m -
         :name: m--

      -  msOsDescriptor :
         `cy_stc_usb_dev_ms_os_string_t <structcy__stc__usb__dev__ms__os__string__t.html#a228033f20928ae1f1744757daf98ad14>`__
      -  msVendorCode :
         `cy_stc_usb_dev_ms_os_string_t <structcy__stc__usb__dev__ms__os__string__t.html#a1a964ea38ee9a11d162ff44fcce0b6c7>`__

      .. rubric:: - n -
         :name: n--

      -  next :
         `cy_stc_usb_dev_class_ll_item_t <structcy__stc__usb__dev__class__ll__item__t.html#a466e1619be2ddc3d8349a22b54f750f6>`__
      -  notify :
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#a504cb022673eaec9d7e05cd8783c916e>`__
      -  numAlternates :
         `cy_stc_usb_dev_interface_t <structcy__stc__usb__dev__interface__t.html#aa916744fe431cf674283079749baf38a>`__
      -  numConfigurations :
         `cy_stc_usb_dev_device_t <structcy__stc__usb__dev__device__t.html#ad49158f747f355b4ecaa2a5795fd89f5>`__
      -  numEndpoints :
         `cy_stc_usb_dev_alternate_t <structcy__stc__usb__dev__alternate__t.html#a8563b62feb70137fcb51627e72104e64>`__
      -  numInputReports :
         `cy_stc_usb_dev_hid_t <structcy__stc__usb__dev__hid__t.html#a8dce054b73e15149cc2122f52a877f47>`__
      -  numInterfaces :
         `cy_stc_usb_dev_configuration_t <structcy__stc__usb__dev__configuration__t.html#a19e2848efee55af859e8c7de5e10e156>`__
      -  numStrings :
         `cy_stc_usb_dev_string_t <structcy__stc__usb__dev__string__t.html#af614fbdb115198cdee79fa47bfc1f8e5>`__

      .. rubric:: - o -
         :name: o--

      -  osStringDescriptors :
         `cy_stc_usb_dev_string_t <structcy__stc__usb__dev__string__t.html#abb5c17aa695bc35907fe957e9c6a0044>`__

      .. rubric:: - p -
         :name: p--

      -  ptr :
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#a2d7d027a0888f204f958a9a71e11dafb>`__

      .. rubric:: - r -
         :name: r--

      -  recipient :
         `cy_stc_usb_dev_bm_request <structcy__stc__usb__dev__bm__request.html#a089186e8e7b77af303b6a96d078f264a>`__
      -  remaining :
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#af7f3075f7c992d51b94012e82826a334>`__
      -  reportDescriptor :
         `cy_stc_usb_dev_hid_t <structcy__stc__usb__dev__hid__t.html#a99129f9cbbd9a2ecdab10f490a85942c>`__
      -  reportDescriptorSize :
         `cy_stc_usb_dev_hid_t <structcy__stc__usb__dev__hid__t.html#a6e958dec1a35a4b54691572374adc217>`__
      -  requestCompleted :
         `cy_stc_usb_dev_class_t <structcy__stc__usb__dev__class__t.html#af498868795ad7fdc5da2cf4e2949b6bd>`__
      -  requestReceived :
         `cy_stc_usb_dev_class_t <structcy__stc__usb__dev__class__t.html#ab580b6b05e25a1fc43040c2b42540465>`__

      .. rubric:: - s -
         :name: s--

      -  setConfiguration :
         `cy_stc_usb_dev_class_t <structcy__stc__usb__dev__class__t.html#ad460bded0f5dc7fab26727c9a4302137>`__
      -  setInterface :
         `cy_stc_usb_dev_class_t <structcy__stc__usb__dev__class__t.html#a0f4844f6f51206be703add0ff1eaa2ac>`__
      -  setup :
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#af38eb6ea917b51d00f4840bc0a18f854>`__
      -  size :
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#a80311bdcebd3f0e0f3508aee417f3a04>`__
      -  stringDescriptors :
         `cy_stc_usb_dev_string_t <structcy__stc__usb__dev__string__t.html#a3ecf7701b1b97b3a1f5bd859211e376d>`__
      -  strings :
         `cy_stc_usb_dev_device_t <structcy__stc__usb__dev__device__t.html#a62bd21476cfd2035c8ce6e82c5b554ca>`__

      .. rubric:: - t -
         :name: t--

      -  timers :
         `cy_stc_usb_dev_hid_config_t <structcy__stc__usb__dev__hid__config__t.html#a5b7720207db9a5910233571b7ede35e6>`__
      -  timersNum :
         `cy_stc_usb_dev_hid_config_t <structcy__stc__usb__dev__hid__config__t.html#a6fcb3f02d21b149099457fa2d74a9471>`__
      -  type :
         `cy_stc_usb_dev_bm_request <structcy__stc__usb__dev__bm__request.html#a0cc454375f0c545389782837320fb608>`__

      .. rubric:: - w -
         :name: w--

      -  wIndex :
         `cy_stc_usb_dev_setup_packet_t <structcy__stc__usb__dev__setup__packet__t.html#a03313abe6497667bddd0eece8046a4ae>`__
      -  wLength :
         `cy_stc_usb_dev_setup_packet_t <structcy__stc__usb__dev__setup__packet__t.html#af6b129674006959fcf0d9602dec9784c>`__
      -  wMaxPacketSize :
         `cy_stc_usbdev_endpoint_descr_t <structcy__stc__usbdev__endpoint__descr__t.html#a788154a9655bed34e586df4c975639ca>`__
      -  wTotalLength :
         `cy_stc_usbdev_config_descr_t <structcy__stc__usbdev__config__descr__t.html#a234eeb09dab42628e330b0d5eef3e8b0>`__
      -  wValue :
         `cy_stc_usb_dev_setup_packet_t <structcy__stc__usb__dev__setup__packet__t.html#aecfaa2d162fee8bcbe8d1be67ee51224>`__

      .. rubric:: - z -
         :name: z--

      -  zlp :
         `cy_stc_usb_dev_control_transfer_t <structcy__stc__usb__dev__control__transfer__t.html#a517e6d7516cd5f0373e8ed4efdf8ecf4>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
