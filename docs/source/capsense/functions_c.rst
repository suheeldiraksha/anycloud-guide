=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - c -
         :name: c--

      -  calTarget :
         `cy_stc_capsense_auto_tune_config_t <structcy__stc__capsense__auto__tune__config__t.html#a48acefee1f92f23853cc7d58511327a2>`__
      -  capacitorSettlingTime :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a2adb7abe65479371b8474bed393b719b>`__
      -  centroidConfig :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#ad2825a42c272b5c46dbcb15a21ec913f>`__
      -  cIntACap :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a0844067d1e2f03296d01c0946542673f>`__
      -  cIntBCap :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a5c87c14d0ef2d932eeb01a53eb77501b>`__
      -  clickDistanceMax :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a4bce321bd5bc00feafad1e32f35c6c0b>`__
      -  clickTimeoutMax :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a3b1107f255188a2b7ad34f2445d45ca7>`__
      -  clickTimeoutMin :
         `cy_stc_capsense_gesture_config_t <structcy__stc__capsense__gesture__config__t.html#a3fd78e04e45942e2780ed93e8bfabcf2>`__
      -  cModCap :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a5456d1702855400f82a5e896cf426583>`__
      -  colMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a53be7999d05b8199561fad7160b0ff8a>`__
      -  configId :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#aa9d6aeaa1e73aac634d4ca090d353af2>`__
      -  configParam0 :
         `cy_stc_capsense_alp_fltr_config_t <structcy__stc__capsense__alp__fltr__config__t.html#a1899bfbe169a38238c06904b75dede02>`__
      -  configParam1 :
         `cy_stc_capsense_alp_fltr_config_t <structcy__stc__capsense__alp__fltr__config__t.html#a5c8856d048cb9985f4ce1f3f437d8408>`__
      -  configParam2 :
         `cy_stc_capsense_alp_fltr_config_t <structcy__stc__capsense__alp__fltr__config__t.html#a591b9d314484d0279c3addcbbe3f334a>`__
      -  configParam3 :
         `cy_stc_capsense_alp_fltr_config_t <structcy__stc__capsense__alp__fltr__config__t.html#ad3d404cffa7e3a5dc7a3f473fee0b2ab>`__
      -  configParam4 :
         `cy_stc_capsense_alp_fltr_config_t <structcy__stc__capsense__alp__fltr__config__t.html#a6e088e048f665d7517d05ca7c1742832>`__
      -  configParam5 :
         `cy_stc_capsense_alp_fltr_config_t <structcy__stc__capsense__alp__fltr__config__t.html#aea8c1e50bc2f69b60a0ac593571baeb7>`__
      -  connectedSnsState :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#a8b3a5ccfdf997d96aafce707e39de42f>`__
      -  convNum :
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#a064aef0add09fe3405402613b9195060>`__
      -  cpuClkHz :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a53051c09692058cebfe2d5a4faa935c8>`__
      -  crcWdgtId :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a8a26753c42ea1a7f25a5018246c6d941>`__
      -  crossCouplingTh :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a09416d6547439c3fd7ec944f3aa7893f>`__
         ,
         `cy_stc_capsense_advanced_touchpad_config_t <structcy__stc__capsense__advanced__touchpad__config__t.html#ad86060f2b4310c6150e555277c209791>`__
      -  csdAutotuneEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a46dabab191776af278a263e931f0c77b>`__
      -  csdCalibrationError :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a83913223b272b76642a87af8b4e78153>`__
      -  csdChargeTransfer :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a5e64ba40e4bc04161f1cfff08365b55b>`__
      -  csdCmodConnection :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a52536fed664d3fc02213044a637d9d80>`__
      -  csdCshConnection :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a3a30b3c133e73edbb7247dde94b8cfd9>`__
      -  csdCTankShieldEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a74c2e88f6c338ad404e26b6e11e5bbc7>`__
      -  csdEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a7fddda182e82215e18769618b498357c>`__
      -  csdFineInitTime :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a937ca9a7b6a86c4eb8969b4ea1b9fa8f>`__
      -  csdIdacAConfig :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#aa322ece65334e5f6bdc305e196c174e4>`__
      -  csdIdacAutocalEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a2ba2ca9a903e0f7414351629b25f0e92>`__
      -  csdIdacAutoGainEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a5302a149824787161c2695232b32c9fa>`__
      -  csdIdacBConfig :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a60ad882d18e3b4c5ce1137b158ad3676>`__
      -  csdIdacCompEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a437d937b2f790a23c4009af497bd3e35>`__
      -  csdIdacGainInitIndex :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a50f450472fd4ff69bbfcc992cd1e0a03>`__
      -  csdIdacMin :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a4fbf20312d5350474cd3b1c6c90c1ce8>`__
      -  csdIdacRowColAlignEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a3abde8bd7d00d217b698b202dcd2bb34>`__
      -  csdInactiveSnsConnection :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#abe52b6e2f195fc5f638808538c0c0df5>`__
      -  csdInactiveSnsDm :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a2f898bf4518847f17b47956a335f6353>`__
      -  csdInactiveSnsHsiom :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a911c7ad42212f5ebc0d82849c27b6e04>`__
      -  csdInitSwRes :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#adc75f58a3536e80f4aeddbd1b749ff7b>`__
      -  csdMfsDividerOffsetF1 :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a2086ec7840bcff2a38a1f4dbc96297ee>`__
      -  csdMfsDividerOffsetF2 :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ae84b54901b2a622357902f34444502ca>`__
      -  csdRawTarget :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ab21e95353a8fa1abae498d2a110db030>`__
      -  csdRConst :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a3023ed9a75800748f3af86571b42a5b0>`__
      -  csdRegAmuxbufInit :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a6c28b93814ffaa55e6fb872700b4ca39>`__
      -  csdRegConfig :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a904304439ab60e08be051b7f3d41e44d>`__
      -  csdRegHscmpInit :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a449bf02b29d79cf2a38e8681041d5552>`__
      -  csdRegHscmpScan :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a9b8255706a4d3e0206a7afdbd4373c94>`__
      -  csdRegIoSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a69a75b1f9b0b9ee96d91fec2150e0b2e>`__
      -  csdRegRefgen :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#aea1919ff206aa3b96e18e0f584b41e23>`__
      -  csdRegSwAmuxbufSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a19d99d39617a9c0e0282028ddd1ba2fe>`__
      -  csdRegSwBypSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a41e9700e4d9869a223efa4fb8c208011>`__
      -  csdRegSwCmpNSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a14ee87b414f48d9a8c70c11032c823f0>`__
      -  csdRegSwCmpPSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a67205084e46e007d0a6d4c8811047845>`__
      -  csdRegSwDsiSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a988ff22fa06616b19a39c39d169a02f0>`__
      -  csdRegSwHsPSelCmodInit :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#af19d498cb29cbaa134653395eafd7ec6>`__
      -  csdRegSwHsPSelCtankInit :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#ad2aeacf04ec2d90fa491b0eef451010a>`__
      -  csdRegSwHsPSelScan :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a589c74e9be04b3416fadad444150da66>`__
      -  csdRegSwRefGenSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#afc2655e619b06ba8b961a434b14d30d1>`__
      -  csdRegSwResInit :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#aa6a2613fbe056600e359d10d3865c723>`__
      -  csdRegSwResScan :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a85cb2b96016d55f3df77b52e9f8a326f>`__
      -  csdRegSwShieldSelScan :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a0319bc9875f2c2f7cd7eea16cd17b8c2>`__
      -  csdShieldDelay :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ae4bc5430219c2d2613941168464acc96>`__
      -  csdShieldEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ad8fa7f6bb76ee337e4c8650a105e30f4>`__
      -  csdShieldNumPin :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#add23287dd34a8fdbf49d4e4bfaf9600a>`__
      -  csdShieldSwRes :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a9d116265ff9d889b1b76fa80ff292e32>`__
      -  csdVref :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ab982977927531f92c5ac748fa316c06f>`__
      -  csdVrefGain :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a4973e3bb2c5cf514f7950d7bf44268cc>`__
      -  csdVrefVoltageMv :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a30c3792b5d6a3df45fe801712fae87d4>`__
      -  cShieldCap :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#ad6d854327c533a7e1c2eb625a02dd8cb>`__
      -  csxCalibrationError :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a0be983572fd16014ccef16c90b3521e7>`__
      -  csxEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a816a9d211280fa1541cb9d79e790926d>`__
      -  csxFineInitTime :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a5fc5e888dc56992a0c194b4c425a8a5d>`__
      -  csxIdacAutocalEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a0bb810d87bef97c73582aeb5e44f86cc>`__
      -  csxIdacGainInitIndex :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a0cd8747f6c21cb813ecd18edf9d54229>`__
      -  csxInitShieldSwRes :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#afde18fd9f1dfb9d859cd24ec0814a57c>`__
      -  csxInitSwRes :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#aaa99b5e4cc5ed3211dc14941064a082a>`__
      -  csxMfsDividerOffsetF1 :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a07e6050b71ba83dd4a5baf3e1380956f>`__
      -  csxMfsDividerOffsetF2 :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a9b7eca3dd5ff0f135ce8785fa3fd4f2f>`__
      -  csxRawTarget :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#aa5656822ae92239213960e8c8e8e5d8e>`__
      -  csxRefGain :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#ac5354e52ee64d8d1afac351a9d8a0428>`__
      -  csxRegAMuxBuf :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a930ea5e7b3a0ab55e3d245a4c7fcaa44>`__
      -  csxRegConfigInit :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a4769ef311a77513a99e8aa77ae0be92e>`__
      -  csxRegConfigScan :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a6af1dd5754837d8be8da11a72241cc15>`__
      -  csxRegRefgen :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a43ebfc2e417b5727b9968f3a9f9d5446>`__
      -  csxRegRefgenSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a1aae8b3da9ccfdc7af195dc760390588>`__
      -  csxRegSwCmpNSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#afdca09ebf3e847d35b4175a20761b09b>`__
      -  csxRegSwRefGenSel :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#af4fab337c4d3a776ad5aacf47da8c31f>`__
      -  csxRegSwResInit :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a1f11ddec8ecd0a2aed8347d237e5495f>`__
      -  csxRegSwResPrech :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a0c1d55db8fd5b06a7f94d7ce8fa74966>`__
      -  csxRegSwResScan :
         `cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html#a4a22eb64852220cd5ca1197e384dd737>`__
      -  csxScanShieldSwRes :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#aa6917c3c8a71cb3c44e47cbe32742de5>`__
      -  csxScanSwRes :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a01c2adce03bd2616b2d1c7acd13a158d>`__
      -  currentISC :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a5ec842573f2642d54e2d76c26556d674>`__
      -  currentSenseMethod :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#ae33699d5e9d6a6e62f5a8177042d2ced>`__
      -  currentTimestamp :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#a1bc85f04ebaefcabd217c5cd6edcd701>`__
      -  customISC :
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#a0454367cc4a77a7e20a1864fa3d3e5f3>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
