===========================================
cy_stc_capsense_ballistic_context_t Struct
===========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares Ballistics Multiplier Configuration data structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`currentTimestamp <structcy__stc__capsense__ballistic__context__t.html#a1bc85f04ebaefcabd217c5cd6edcd701>`__
 
Current timestamp.
 
uint32_t 
`oldTimestamp <structcy__stc__capsense__ballistic__context__t.html#a38d8589f93a3bc5fa8fb53e5076d6da6>`__
 
Previous timestamp.
 
int32_t 
`deltaXfrac <structcy__stc__capsense__ballistic__context__t.html#a8fe77855d41716420ecb5b8a6a4726d5>`__
 
Fraction of X-axis displacement.
 
int32_t 
`deltaYfrac <structcy__stc__capsense__ballistic__context__t.html#ada5af6548b412a22be7317001ceafa1d>`__
 
Fraction of Y-axis displacement.
 
uint16_t 
`x <structcy__stc__capsense__ballistic__context__t.html#ae07addb7a565119d5b2244a5bd64dde6>`__
 
X-axis position.
 
uint16_t 
`y <structcy__stc__capsense__ballistic__context__t.html#abadcc7e1af463b1668eb93183cb87252>`__
 
Y-axis position.
 
uint8_t 
`touchNumber <structcy__stc__capsense__ballistic__context__t.html#a8b2a6fe6bc8bf68463bf94a93c7e3a2b>`__
 
Current number of touches.
 
uint8_t 
`oldTouchNumber <structcy__stc__capsense__ballistic__context__t.html#a74a898f524e561622e4fb8d5b302fa77>`__
 
Previous number of touches.
 
uint8_t 
`reserved0 <structcy__stc__capsense__ballistic__context__t.html#aaf3e7d3b18b2ecf51031b644db4292a3>`__
 
Reserved field.
 
uint8_t 
`reserved1 <structcy__stc__capsense__ballistic__context__t.html#accd9ff41ad610ac269d04e3065af52c2>`__
 
Reserved field.



