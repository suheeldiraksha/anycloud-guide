================================
cy_stc_active_scan_sns_t Struct
================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares active sensor details.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

void(* 
`ptrISRCallback <structcy__stc__active__scan__sns__t.html#a7f72ba89653be42335b33201907e4c5b>`__
)(void \*context)
 
Pointer to the interrupt handler of the active sensor.
 
const
`cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html>`__
\* 
`ptrWdConfig <structcy__stc__active__scan__sns__t.html#ab57fc4e1bdf74350bf3c6a701ad3c56a>`__
 
Pointer to the widget configuration structure of the active sensor.
 
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
\* 
`ptrWdContext <structcy__stc__active__scan__sns__t.html#a81beffc93033c90901da1f8debfbd59c>`__
 
Pointer to the widget context structure of the active sensor.
 
const
`cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html>`__
\* 
`ptrEltdConfig <structcy__stc__active__scan__sns__t.html#abc6f5af6f05f765faaca0aa5f4d9da40>`__
 
Pointer to the electrode configuration structure of the active sensor.
 
const
`cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html>`__
\* 
`ptrRxConfig <structcy__stc__active__scan__sns__t.html#ada5061a0a2b46e3bff3553d98644c041>`__
 
Pointer to the Rx electrode configuration structure of the active
sensor.
 
const
`cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html>`__
\* 
`ptrTxConfig <structcy__stc__active__scan__sns__t.html#a9cceebad50f0ca6d92bbfc2408d2c21a>`__
 
Pointer to the Tx electrode configuration structure of the active
sensor.
 
`cy_stc_capsense_sensor_context_t <structcy__stc__capsense__sensor__context__t.html>`__
\* 
`ptrSnsContext <structcy__stc__active__scan__sns__t.html#aeb8898be429a7205311694ac340b0d2e>`__
 
Pointer to the sensor context structure.
 
volatile uint16_t 
`sensorIndex <structcy__stc__active__scan__sns__t.html#ad8da18f16f62c0855f70ec8ba044f7aa>`__
 
Current sensor ID.
 
volatile uint8_t 
`widgetIndex <structcy__stc__active__scan__sns__t.html#a013e17535d7b3ed5bb4268a5baf5863b>`__
 
Current widget ID.
 
volatile uint8_t 
`rxIndex <structcy__stc__active__scan__sns__t.html#aa4ba26b22efa56788c20fb2c2ce1fb81>`__
 
Current Rx ID.
 
volatile uint8_t 
`txIndex <structcy__stc__active__scan__sns__t.html#a61757766f7b312242d258d584b8f62a9>`__
 
Current Tx ID.
 
volatile uint8_t 
`connectedSnsState <structcy__stc__active__scan__sns__t.html#a8b3a5ccfdf997d96aafce707e39de42f>`__
 
Shows if the current sensor is connected to analog bus.
 
volatile uint8_t 
`scanScopeAll <structcy__stc__active__scan__sns__t.html#abf320502877dad97fd6227d8ee3202ca>`__
 
Request of scanning all widgets.
 
volatile uint8_t 
`scanScopeSns <structcy__stc__active__scan__sns__t.html#ac0ec073645f66018f477e711dad937a7>`__
 
Request of scanning a single sensor.
 
volatile uint8_t 
`mfsChannelIndex <structcy__stc__active__scan__sns__t.html#a159ba1e2d4ff19b16d0aa6695b8f1499>`__
 
MFS channel index.
 
volatile uint8_t 
`currentSenseMethod <structcy__stc__active__scan__sns__t.html#ae33699d5e9d6a6e62f5a8177042d2ced>`__
 
Current sensing method.

