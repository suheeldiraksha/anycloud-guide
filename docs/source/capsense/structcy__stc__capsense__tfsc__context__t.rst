======================================
cy_stc_capsense_tfsc_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture Two Finger Single Click context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`touchStartTime1 <structcy__stc__capsense__tfsc__context__t.html#a8c266f5d2b658bb7e3fa61d33c02d02e>`__
 
Touchdown time of the first touch.
 
uint32_t 
`touchStartTime2 <structcy__stc__capsense__tfsc__context__t.html#ad05feec06a31053de7213da66168c4ff>`__
 
Touchdown time of the second touch.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__tfsc__context__t.html#ac2bb0da57282c795cbf0f3d4c1c50e94>`__
 
Touchdown position of the first touch.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition2 <structcy__stc__capsense__tfsc__context__t.html#a8b6bd5906acd0001b1ceae797db7a8a3>`__
 
Touchdown position of the second touch.
 
uint8_t 
`state <structcy__stc__capsense__tfsc__context__t.html#a8e22783a5b0ccb54c439fceb105978e4>`__
 
Gesture state.

