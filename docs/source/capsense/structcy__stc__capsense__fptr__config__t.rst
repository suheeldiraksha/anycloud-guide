=====================================
cy_stc_capsense_fptr_config_t Struct
=====================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Function pointers configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_func_capsense_csd_setup_widget_ptr_t <group__group__capsense__structures.html#ga6b3f78dea109bdccbd5d665198b3261c>`__ 
`fptrCSDSetupWidget <structcy__stc__capsense__fptr__config__t.html#aeb2b9a96c64e8ff976d066db67950f7f>`__
 
The
`Cy_CapSense_CSDSetupWidget() <group__group__capsense__low__level.html#gaff00959c95147f8374b1629caa45c43c>`__
function pointer.
 
`cy_func_capsense_csd_scan_ptr_t <group__group__capsense__structures.html#gaaa9f4adf79864cb281e5323efdd9c27a>`__ 
`fptrCSDScan <structcy__stc__capsense__fptr__config__t.html#a64546c38346006ec7c1137dae13e210a>`__
 
The
`Cy_CapSense_CSDScan() <group__group__capsense__low__level.html#gad5faac7e10eb8c4193d7c93b5615ac70>`__
function pointer.
 
`cy_func_capsense_dp_process_csd_widget_raw_counts_ptr_t <group__group__capsense__structures.html#gad59fc5e89ddae5601435b04c97b92a84>`__ 
`fptrDpProcessCsdWidgetRawCounts <structcy__stc__capsense__fptr__config__t.html#ab714659f9debd5ce3946592c5bc58e82>`__
 
The Cy_CapSense_DpProcessCsdWidgetRawCounts() function pointer.
 
`cy_func_capsense_dp_process_csd_widget_status_ptr_t <group__group__capsense__structures.html#ga62876ab34d323d8b38ed807dfcb3d4e9>`__ 
`fptrDpProcessCsdWidgetStatus <structcy__stc__capsense__fptr__config__t.html#a04cd8fabd3aae362d8ed413e5b02d1e5>`__
 
The Cy_CapSense_DpProcessCsdWidgetStatus() function pointer.
 
`cy_func_capsense_csd_disable_mode_ptr_t <group__group__capsense__structures.html#gad6c5af43b24207594de60f2cdc92d465>`__ 
`fptrCSDDisableMode <structcy__stc__capsense__fptr__config__t.html#adaedd2f3df11f6560c9a548327c4f083>`__
 
The Cy_CapSense_CSDDisableMode() function pointer.
 
`cy_func_capsense_csd_initialize_ptr_t <group__group__capsense__structures.html#ga8f05dac4d107c9a521408ad2ae815261>`__ 
`fptrCSDInitialize <structcy__stc__capsense__fptr__config__t.html#abeb84b1ed0e6c899e0ac8775aa53ee72>`__
 
The Cy_CapSense_CSDInitialize() function pointer.
 
`cy_func_capsense_csd_scan_isr_ptr_t <group__group__capsense__structures.html#ga7b78283a72feffcf94948e7a26b20b0f>`__ 
`fptrCSDScanISR <structcy__stc__capsense__fptr__config__t.html#a746ac7fd5454903e32338d5ceb85a7e8>`__
 
The Cy_CapSense_CSDScanISR() function pointer.
 
`cy_func_capsense_csx_setup_widget_ptr_t <group__group__capsense__structures.html#ga65e3969643e61bb33972707771e3e618>`__ 
`fptrCSXSetupWidget <structcy__stc__capsense__fptr__config__t.html#a8a7e1b460e9e6cc73724def7469c5962>`__
 
The
`Cy_CapSense_CSXSetupWidget() <group__group__capsense__low__level.html#ga4a13b66055d7d1649c47cb75d1d65f62>`__
function pointer.
 
`cy_func_capsense_csx_scan_ptr_t <group__group__capsense__structures.html#gaa9ee3e90f9c2d6574e97b581917dfc3d>`__ 
`fptrCSXScan <structcy__stc__capsense__fptr__config__t.html#a5a7709afc918b5a0ac19aececd2caf0e>`__
 
The
`Cy_CapSense_CSXScan() <group__group__capsense__low__level.html#gaa76f71d361d42796da2a716a207766cd>`__
function pointer.
 
`cy_capsense_dp_process_csx_widget_raw_counts_ptr_t <group__group__capsense__structures.html#ga641cf5855af13800c3213a3af5f74dbe>`__ 
`fptrDpProcessCsxWidgetRawCounts <structcy__stc__capsense__fptr__config__t.html#a9e40c13bb8b7e169f3d8528c0229b554>`__
 
The Cy_CapSense_DpProcessCsxWidgetRawCounts() function pointer.
 
`cy_func_capsense_dp_process_csx_widget_status_ptr_t <group__group__capsense__structures.html#ga4d7ec63e37f2ec69cda8bd16a5d3c36a>`__ 
`fptrDpProcessCsxWidgetStatus <structcy__stc__capsense__fptr__config__t.html#a2ae6f6acf2aaa8a58171f43b52812ae3>`__
 
The Cy_CapSense_DpProcessCsxWidgetStatus() function pointer.
 
`cy_func_capsense_csx_initialize_ptr_t <group__group__capsense__structures.html#ga003cffa17c435e52e7bd175dac5df2b3>`__ 
`fptrCSXInitialize <structcy__stc__capsense__fptr__config__t.html#a03e9ccdda20440a5337a38bab8b478a6>`__
 
The Cy_CapSense_CSXInitialize() function pointer.
 
`cy_func_capsense_csx_disablemode_ptr_t <group__group__capsense__structures.html#ga5bb635cf39877356f09d7c91b9181d9a>`__ 
`fptrCSXDisableMode <structcy__stc__capsense__fptr__config__t.html#aa7757a0d1424e30d581c3bd80a7afc0e>`__
 
The Cy_CapSense_CSXDisableMode() function pointer.
 
`cy_func_capsense_csx_scan_isr_ptr_t <group__group__capsense__structures.html#ga8e2f24c2e9f639004e08f551612b0ddd>`__ 
`fptrCSXScanISR <structcy__stc__capsense__fptr__config__t.html#ad45248eed224a9a3ae3e04adbfec90c9>`__
 
The Cy_CapSense_CSXScanISR() function pointer.
 
`cy_func_capsense_adaptive_filter_initialize_lib_ptr_t <group__group__capsense__structures.html#ga42f17205210e0402c0eac7cf9a2f3d05>`__ 
`fptrAdaptiveFilterInitializeLib <structcy__stc__capsense__fptr__config__t.html#a41770300c55cd8bb831388529fdda828>`__
 
The Cy_CapSense_AdaptiveFilterInitialize_Lib() function pointer.
 
`cy_func_capsense_adaptive_filter_run_lib_ptr_t <group__group__capsense__structures.html#ga6381f67df3d3de4ab2759609865bfbe6>`__ 
`fptrAdaptiveFilterRunLib <structcy__stc__capsense__fptr__config__t.html#a38bca06b3f27677471df626f15185085>`__
 
The Cy_CapSense_AdaptiveFilterRun_Lib() function pointer.
 
`cy_func_capsense_ballistic_multiplier_lib_ptr_t <group__group__capsense__structures.html#ga40875da5af6057e6a2d8d3e67fc9cb4e>`__ 
`fptrBallisticMultiplierLib <structcy__stc__capsense__fptr__config__t.html#a57be94f71434afa9dd47cdae2b2e052d>`__
 
The Cy_CapSense_BallisticMultiplier_Lib() function pointer.
 
`cy_func_capsense_initialize_all_filters_ptr_t <group__group__capsense__structures.html#ga353a205f17b5c3c66abe2c72e2c430f9>`__ 
`fptrInitializeAllFilters <structcy__stc__capsense__fptr__config__t.html#aaf36b219b6ed11505273e20b523ba7a0>`__
 
The
`Cy_CapSense_InitializeAllFilters() <group__group__capsense__low__level.html#ga1fcb002d0216bb96ab818276954606e7>`__
function pointer.
 
`cy_func_capsense_ft_run_enabled_filters_internal_ptr_t <group__group__capsense__structures.html#ga941f807ce3bac3ef024d969e5402719a>`__ 
`fptrFtRunEnabledFiltersInternal <structcy__stc__capsense__fptr__config__t.html#a9077cb0eccfed8647c91080e501fb18e>`__
 
The Cy_CapSense_FtRunEnabledFiltersInternal() function pointer.
 
`cy_func_capsense_process_position_filters_ptr_t <group__group__capsense__structures.html#ga6324a408708a58794f1a94bceb8d15f2>`__ 
`fptrProcessPositionFilters <structcy__stc__capsense__fptr__config__t.html#af14258d82e10a39eec262afdbe657866>`__
 
The Cy_CapSense_ProcessPositionFilters() function pointer.
 
`cy_func_capsense_run_position_filters_ptr_t <group__group__capsense__structures.html#ga659199bc1e80b1ac967718564ad92f8d>`__ 
`fptrRunPositionFilters <structcy__stc__capsense__fptr__config__t.html#af5bb2f2a934df34465fa7dd9fd2358a3>`__
 
The Cy_CapSense_RunPositionFilters() function pointer.
 
`cy_func_capsense_init_position_filters_ptr_t <group__group__capsense__structures.html#gae9e7c8faac62175188467af0ef99f7a1>`__ 
`fptrInitPositionFilters <structcy__stc__capsense__fptr__config__t.html#a6cb6d8d58d21dbd4920246b84b30175e>`__
 
The Cy_CapSense_InitPositionFilters() function pointer.
 
`cy_func_capsense_dp_process_button_ptr_t <group__group__capsense__structures.html#gac94e047f6bc3f4d6dd05cdf8edd352cf>`__ 
`fptrDpProcessButton <structcy__stc__capsense__fptr__config__t.html#a2fcc6b9890c2741ce4d033bb22ae594d>`__
 
The Cy_CapSense_DpProcessButton() function pointer.
 
`cy_func_capsense_dp_process_slider_ptr_t <group__group__capsense__structures.html#ga2ad1ae2a39f5b844642bfc731c379321>`__ 
`fptrDpProcessSlider <structcy__stc__capsense__fptr__config__t.html#a1a9b01b42256b85a945033b75a101733>`__
 
The Cy_CapSense_DpProcessSlider() function pointer.
 
`cy_func_capsense_dp_process_csd_matrix_ptr_t <group__group__capsense__structures.html#ga60c5b06065bab0e4768ce6d902a1f7db>`__ 
`fptrDpProcessCsdMatrix <structcy__stc__capsense__fptr__config__t.html#a54c1a05c2bdda9bb936f50ab37e3f304>`__
 
The Cy_CapSense_DpProcessCsdMatrix() function pointer.
 
`cy_func_capsense_dp_process_csd_touchpad_ptr_t <group__group__capsense__structures.html#gac3ec08697ec2c52ab9ea9fa822a3afc9>`__ 
`fptrDpProcessCsdTouchpad <structcy__stc__capsense__fptr__config__t.html#a757bd8341a43081a8d893084a2ad472e>`__
 
The Cy_CapSense_DpProcessCsdTouchpad() function pointer.
 
`cy_func_capsense_dp_advanced_centroid_touchpad_ptr_t <group__group__capsense__structures.html#ga02536b0f6d3d92b0c6c0c9debc69f059>`__ 
`fptrDpAdvancedCentroidTouchpad <structcy__stc__capsense__fptr__config__t.html#a48fb912241f04ed457189575eaabc160>`__
 
The Cy_CapSense_DpAdvancedCentroidTouchpad() function pointer.
 
`cy_func_capsense_dp_process_proximity_ptr_t <group__group__capsense__structures.html#ga981076b8e7b6ed50b7bf263933bef8dd>`__ 
`fptrDpProcessProximity <structcy__stc__capsense__fptr__config__t.html#a66ee988edf687d26fe19cb4ed38f3f36>`__
 
The Cy_CapSense_DpProcessProximity() function pointer.
 
`cy_func_capsense_dp_process_csx_touchpad_ptr_t <group__group__capsense__structures.html#ga2a1df1eae8b8eda4dfba54428a767775>`__ 
`fptrDpProcessCsxTouchpad <structcy__stc__capsense__fptr__config__t.html#a7609a9959e55ddefc5af6769b4e4b355>`__
 
The Cy_CapSense_DpProcessCsxTouchpad() function pointer.
 
`cy_func_capsense_calibrate_all_csd_widgets_ptr_t <group__group__capsense__structures.html#gabfc57fe60da3198143ad631ac928678f>`__ 
`fptrCalibrateAllCsdWidgets <structcy__stc__capsense__fptr__config__t.html#a2a94c5e6a622b2983a34f90fb057b2ab>`__
 
The
`Cy_CapSense_CalibrateAllCsdWidgets() <group__group__capsense__low__level.html#ga45d20fdf506fc902a43367938265e4f6>`__
function pointer.
 
`cy_func_capsense_csd_calibrate_widget_ptr_t <group__group__capsense__structures.html#ga1da7eba114e21cc7be97a0e1703b741e>`__ 
`fptrCSDCalibrateWidget <structcy__stc__capsense__fptr__config__t.html#ad7a1e474c2dc9408e6610ef68313b049>`__
 
The
`Cy_CapSense_CSDCalibrateWidget() <group__group__capsense__low__level.html#ga267502081ebfd5ed1221debd9167e7fa>`__
function pointer.
 
`cy_func_capsense_calibrate_all_csx_widgets_ptr_t <group__group__capsense__structures.html#ga74c5279fde94519d94bc5acb30cf3f25>`__ 
`fptrCalibrateAllCsxWidgets <structcy__stc__capsense__fptr__config__t.html#aa9b696dcb3ceee40a579668c384e04f3>`__
 
The
`Cy_CapSense_CalibrateAllCsxWidgets() <group__group__capsense__low__level.html#ga8872c55dc3ac6f8f81db10b130708edf>`__
function pointer.
 
`cy_func_capsense_ss_auto_tune_ptr_t <group__group__capsense__structures.html#gaf66c75b4d79fb156c2cd9b34f0c577f7>`__ 
`fptrSsAutoTune <structcy__stc__capsense__fptr__config__t.html#a61792eab9d1c4f9b16ee5cbab70345c3>`__
 
The Cy_CapSense_SsAutoTune() function pointer.
 
`cy_func_capsense_run_noise_envelope_lib_ptr_t <group__group__capsense__structures.html#gae938941d618b2e112b0070dba1cc502b>`__ 
`fptrRunNoiseEnvelopeLib <structcy__stc__capsense__fptr__config__t.html#a0f9cf34d40eb21dcc0d8a6068ba1edaf>`__
 
The Cy_CapSense_RunNoiseEnvelope_Lib() function pointer.
 
`cy_func_capsense_dp_update_thresholds_ptr_t <group__group__capsense__structures.html#ga85114ad19013e4b02b4f098aa7ad9a32>`__ 
`fptrDpUpdateThresholds <structcy__stc__capsense__fptr__config__t.html#a27eba5a24e091a1410ac07b486c39def>`__
 
The Cy_CapSense_DpUpdateThresholds() function pointer.
 
`cy_func_capsense_initialize_noise_envelope_lib_ptr_t <group__group__capsense__structures.html#gae167b286c60d254601a2d5b7f1997188>`__ 
`fptrInitializeNoiseEnvelopeLib <structcy__stc__capsense__fptr__config__t.html#a9d86ff861bf65d65ed110221c8fb721d>`__
 
The Cy_CapSense_InitializeNoiseEnvelope_Lib() function pointer.
 
`cy_func_capsense_bist_initialize_ptr_t <group__group__capsense__structures.html#ga2b1c51db9f5e2ae34f13a9d7be2fa90c>`__ 
`fptrBistInitialize <structcy__stc__capsense__fptr__config__t.html#a6daf21c1e84deeae72bf8c3b6e6bcbcf>`__
 
The Cy_CapSense_BistInitialize() function pointer.
 
`cy_func_capsense_bist_disable_mode_ptr_t <group__group__capsense__structures.html#ga3c36c89033827129e099f8de4a248615>`__ 
`fptrBistDisableMode <structcy__stc__capsense__fptr__config__t.html#a3e5bdaa1c968b2a28921aa71b23554a7>`__
 
The Cy_CapSense_BistDisableMode() function pointer.
 
`cy_func_capsense_bist_ds_initialize_ptr_t <group__group__capsense__structures.html#gadf5e95ff3a7b27b3e632eaeda5b23a93>`__ 
`fptrBistDsInitialize <structcy__stc__capsense__fptr__config__t.html#abb32fecc47613c7104f1d437224c2222>`__
 
The Cy_CapSense_BistDsInitialize() function pointer.

