=========================================================
Cypress CapSense Middleware Library 2.10: Data Structures
=========================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         Here are the data structures with brief descriptions:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  C\ `cy_stc                      | Declares active sensor details   |
         | _active_scan_sns_t <structcy__st |                                  |
         | c__active__scan__sns__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_                     | Declares Adaptive Filter         |
         | capsense_adaptive_filter_config_ | configuration parameters         |
         | t <structcy__stc__capsense__adap |                                  |
         | tive__filter__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_caps                 | Declares Advanced Centroid       |
         | ense_advanced_centroid_config_t  | configuration parameters         |
         | <structcy__stc__capsense__advanc |                                  |
         | ed__centroid__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_caps                 | Configuration structure of       |
         | ense_advanced_touchpad_config_t  | advanced touchpad                |
         | <structcy__stc__capsense__advanc |                                  |
         | ed__touchpad__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_alp_fltr_ch | Declares ALP filter data         |
         | annel_t <structcy__stc__capsense | structure                        |
         | __alp__fltr__channel__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_alp_fltr_   | Declares ALP filter              |
         | config_t <structcy__stc__capsens | configuration structure          |
         | e__alp__fltr__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_auto_tune_c | Declares HW SmartSense data      |
         | onfig_t <structcy__stc__capsense | structure for CSD widgets        |
         | __auto__tune__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_ballistic_  | Declares Ballistics Multiplier   |
         | config_t <structcy__stc__capsens | Configuration data structure     |
         | e__ballistic__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |                                  | Declares Ballistics Multiplier   |
         | C\ `cy_stc_capsense_ballistic_co | Configuration data structure     |
         | ntext_t <structcy__stc__capsense |                                  |
         | __ballistic__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_ballisti    | Declares Ballistic Displacement  |
         | c_delta_t <structcy__stc__capsen | structure                        |
         | se__ballistic__delta__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_bi          | Declares BIST Context Data       |
         | st_context_t <structcy__stc__cap | Structure                        |
         | sense__bist__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_                     | Declares the BIST structure with |
         | capsense_bist_custom_parameters_ | custom scan parameters           |
         | t <structcy__stc__capsense__bist |                                  |
         | __custom__parameters__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_comm        | Common configuration structure   |
         | on_config_t <structcy__stc__caps |                                  |
         | ense__common__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_common      | Declares top-level Context Data  |
         | _context_t <structcy__stc__capse | Structure                        |
         | nse__common__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_                     | Declares top-level CapSense      |
         | capsense_context_t <structcy__st | context data structure           |
         | c__capsense__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_csx_touch_b | Internal CSX Touchpad buffer     |
         | uffer_t <structcy__stc__capsense | structure for CSX for Touchpads' |
         | __csx__touch__buffer__t.html>`__ | processing                       |
         +----------------------------------+----------------------------------+
         |  C                               | CSX Touchpad touch tracking      |
         | \ `cy_stc_capsense_csx_touch_his | history                          |
         | tory_t <structcy__stc__capsense_ |                                  |
         | _csx__touch__history__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_electrode_  | Electrode objects configuration  |
         | config_t <structcy__stc__capsens | structure                        |
         | e__electrode__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_            | Function pointers configuration  |
         | fptr_config_t <structcy__stc__ca | structure                        |
         | psense__fptr__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_gestur      | Gesture configuration structure  |
         | e_config_t <structcy__stc__capse |                                  |
         | nse__gesture__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_gesture_    | Gesture global context structure |
         | context_t <structcy__stc__capsen |                                  |
         | se__gesture__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_gesture_po  | Gesture position structure       |
         | sition_t <structcy__stc__capsens |                                  |
         | e__gesture__position__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_idac_gain   | Declares the IDAC gain table     |
         | _table_t <structcy__stc__capsens |                                  |
         | e__idac__gain__table__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_internal_c  | Declares internal Context Data   |
         | ontext_t <structcy__stc__capsens | Structure                        |
         | e__internal__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_of          | Gesture One Finger Click and     |
         | cd_context_t <structcy__stc__cap | Drag context structure           |
         | sense__ofcd__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_of          | Gesture One Finger Double Click  |
         | dc_context_t <structcy__stc__cap | context structure                |
         | sense__ofdc__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_of          | Gesture One Finger Edge Swipe    |
         | es_context_t <structcy__stc__cap | context structure                |
         | sense__ofes__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_of          | Gesture One Finger Flick context |
         | fl_context_t <structcy__stc__cap | structure                        |
         | sense__offl__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_of          | Gesture One Finger Rotate        |
         | rt_context_t <structcy__stc__cap | context structure                |
         | sense__ofrt__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_of          | Gesture One Finger Single Click  |
         | sc_context_t <structcy__stc__cap | context structure                |
         | sense__ofsc__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_of          | Gesture One Finger Scroll        |
         | sl_context_t <structcy__stc__cap | context structure                |
         | sense__ofsl__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsens              | Pin configuration structure      |
         | e_pin_config_t <structcy__stc__c |                                  |
         | apsense__pin__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_ca                   | Declares position structure that |
         | psense_position_t <structcy__stc | keep information of a single     |
         | __capsense__position__t.html>`__ | touch                            |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_sensor      | Sensor context structure         |
         | _context_t <structcy__stc__capse |                                  |
         | nse__sensor__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_smarts      | Declares Noise envelope data     |
         | ense_csd_noise_envelope_t <struc | structure for CSD widgets when   |
         | tcy__stc__capsense__smartsense__ | SmartSense is enabled            |
         | csd__noise__envelope__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_sma         | Declares Update Thresholds       |
         | rtsense_update_thresholds_t <str | structure                        |
         | uctcy__stc__capsense__smartsense |                                  |
         | __update__thresholds__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_tf          | Gesture Two Finger Single Click  |
         | sc_context_t <structcy__stc__cap | context structure                |
         | sense__tfsc__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_tf          | Gesture Two Finger Scroll        |
         | sl_context_t <structcy__stc__cap | context structure                |
         | sense__tfsl__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_tf          | Gesture Two Finger Zoom context  |
         | zm_context_t <structcy__stc__cap | structure                        |
         | sense__tfzm__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_                         | Declares touch structure used to |
         | stc_capsense_touch_t <structcy__ | store positions of Touchpad,     |
         | stc__capsense__touch__t.html>`__ | Matrix buttons and Slider        |
         |                                  | widgets                          |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_widg        | Widget configuration structure   |
         | et_config_t <structcy__stc__caps |                                  |
         | ense__widget__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_widget      | Widget context structure         |
         | _context_t <structcy__stc__capse |                                  |
         | nse__widget__context__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_stc_capsense_widget_cr   | Declares the structure that is   |
         | c_data_t <structcy__stc__capsens | intended to store the            |
         | e__widget__crc__data__t.html>`__ | `cy_stc_capsense_widget          |
         |                                  | _context_t <structcy__stc__capse |
         |                                  | nse__widget__context__t.html>`__ |
         |                                  | data structure fields, the CRC   |
         |                                  | checking should be applied for   |
         +----------------------------------+----------------------------------+

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
