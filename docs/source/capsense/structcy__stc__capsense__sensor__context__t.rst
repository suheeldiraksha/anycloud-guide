========================================
cy_stc_capsense_sensor_context_t Struct
========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Sensor context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`raw <structcy__stc__capsense__sensor__context__t.html#a54bbaaded1ac1a6857ce84a996224ccc>`__
 
Sensor raw count.
 
uint16_t 
`bsln <structcy__stc__capsense__sensor__context__t.html#ae70bc85c4ea36acf3f7f24083ee12358>`__
 
Sensor baseline.
 
uint16_t 
`diff <structcy__stc__capsense__sensor__context__t.html#ae3a77556515876cf00e178ccc7cbd606>`__
 
Sensor difference count.
 
uint8_t 
`status <structcy__stc__capsense__sensor__context__t.html#ac0c3267b406b40d89fb8219b75536eee>`__
 
Sensor status.
 
uint8_t 
`negBslnRstCnt <structcy__stc__capsense__sensor__context__t.html#a0dfea7d7b4a9b3858a203a54df8bc3fc>`__
 
Negative baseline reset counter.
 
uint8_t 
`idacComp <structcy__stc__capsense__sensor__context__t.html#a242877120ccdac585a0f001736fe7be2>`__
 
Compensation IDAC of CSD or IDAC in CSX.
 
uint8_t 
`bslnExt <structcy__stc__capsense__sensor__context__t.html#a00e13c949413eec88791b6b89215dc67>`__
 
Sensor baseline fractional.

