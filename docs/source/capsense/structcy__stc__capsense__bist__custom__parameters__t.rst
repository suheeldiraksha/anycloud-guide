================================================
cy_stc_capsense_bist_custom_parameters_t Struct
================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares the BIST structure with custom scan parameters.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_en_capsense_bist_io_state_t <group__group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ 

`customISC <structcy__stc__capsense__bist__custom__parameters__t.html#a0454367cc4a77a7e20a1864fa3d3e5f3>`__

 

| The inactive state of sensors during the custom scan.

 

uint16_t 

`modClk <structcy__stc__capsense__bist__custom__parameters__t.html#aafb50e8ce6fad46c3a8b0224ce1714b6>`__

 

| The ModClk divider for a custom scan.
  `More... <#aafb50e8ce6fad46c3a8b0224ce1714b6>`__

 

uint16_t 

`snsClk <structcy__stc__capsense__bist__custom__parameters__t.html#aa9db7044638f1e6ee7a56a850f31d67d>`__

 

| The SnsClk divider for a custom scan.
  `More... <#aa9db7044638f1e6ee7a56a850f31d67d>`__

 

uint16_t 

`convNum <structcy__stc__capsense__bist__custom__parameters__t.html#a064aef0add09fe3405402613b9195060>`__

 

| The number of conversions for a custom scan.
  `More... <#a064aef0add09fe3405402613b9195060>`__

 

uint8_t 

`vrefGain <structcy__stc__capsense__bist__custom__parameters__t.html#aa24f7a01ccebf1ad20e600ca27565eac>`__

 

| The Vref gain for a custom scan.

 

uint8_t 

`idacMod <structcy__stc__capsense__bist__custom__parameters__t.html#ada4f2a1109cc40af7ad1455e67252153>`__

 

| Sets the code of the modulation IDAC for a custom scan.

 

uint8_t 

`idacGainIndex <structcy__stc__capsense__bist__custom__parameters__t.html#a60c9a9e518194f83bd0bba955066474f>`__

 

| Index of IDAC gain in table
  `cy_stc_capsense_idac_gain_table_t <structcy__stc__capsense__idac__gain__table__t.html>`__.

 

uint8_t 

`fineInitTime <structcy__stc__capsense__bist__custom__parameters__t.html#a73d52513a1573f6fe1ad64ed47956660>`__

 

| Number of dummy SnsClk periods at fine initialization for a custom
  scan.

 

Field Documentation
--------------------

`◆  <#aafb50e8ce6fad46c3a8b0224ce1714b6>`__\ modClk
===================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------------------------+
      | uint16_t cy_stc_capsense_bist_custom_parameters_t::modClk |
      +-----------------------------------------------------------+

   .. container:: memdoc

      The ModClk divider for a custom scan.

      The minimum value is 1 and the maximum depends on a divider type,
      but for a reliable CSD HW block operation, it is recommended to
      provide a modulation clock frequency in the range from 1 to 50 MHz

`◆  <#aa9db7044638f1e6ee7a56a850f31d67d>`__\ snsClk
===================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------------------------+
      | uint16_t cy_stc_capsense_bist_custom_parameters_t::snsClk |
      +-----------------------------------------------------------+

   .. container:: memdoc

      The SnsClk divider for a custom scan.

      The minimum value is 4 and the maximum is 4095, but for a reliable
      CSD HW block operation, it is recommended to provide an sensor
      clock frequency in the range from 100 to 6000 kHz

`◆  <#a064aef0add09fe3405402613b9195060>`__\ convNum
====================================================

.. container:: memitem

   .. container:: memproto

      +------------------------------------------------------------+
      | uint16_t cy_stc_capsense_bist_custom_parameters_t::convNum |
      +------------------------------------------------------------+

   .. container:: memdoc

      The number of conversions for a custom scan.

      The maximum raw counts is equal (convNum \* snsClkDivider - 1),
      that corresponds to (2^Resolution - 1) in older notations. The
      minimum value is 4 and the maximum is 65535, but as the maximum
      raw counts is 65535, the convNum value should be less than (65536
      / snsClkDivider)

