=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  accelCoeff :
         `cy_stc_capsense_ballistic_config_t <structcy__stc__capsense__ballistic__config__t.html#af168ee9cd6deb06a3dd59c63cd63be27>`__
      -  advConfig :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a94b1400ac7cf425e79664f4e19f54d0b>`__
      -  aiirConfig :
         `cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html#a44c45fc836a80c8fe858ab3d5133d751>`__
      -  analogWakeupDelay :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a625ca1e473af912530d0da3225e0ca40>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
