=========================================
cy_stc_capsense_idac_gain_table_t Struct
=========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares the IDAC gain table.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`gainReg <structcy__stc__capsense__idac__gain__table__t.html#ad44c7d56219cb4db0e7aaaba7821fb40>`__
 
Register value of IDAC gain.
 
uint32_t 
`gainValue <structcy__stc__capsense__idac__gain__table__t.html#ae0e27b2d2c7a17bde0d7ed7c00bd0074>`__
 
Absolute gain value in pA.

