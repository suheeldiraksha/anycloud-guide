=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - h -
         :name: h--

      -  history :
         `cy_stc_capsense_ofrt_context_t <structcy__stc__capsense__ofrt__context__t.html#a4df4b5e9a02cba4b6bba8e6c10c2c68e>`__
      -  hwConfig :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a9bbdef6eb05a232f7086011dbea72715>`__
      -  hysteresis :
         `cy_stc_capsense_smartsense_update_thresholds_t <structcy__stc__capsense__smartsense__update__thresholds__t.html#a958e9740d154803f7fbf750862d8713c>`__
         ,
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a6db8e6c147c6b3da087fa96508d8704a>`__
      -  hysteresisVal :
         `cy_stc_capsense_widget_crc_data_t <structcy__stc__capsense__widget__crc__data__t.html#adaaf53405b8386927c4957cfc3726926>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
