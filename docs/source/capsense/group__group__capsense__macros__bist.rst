==========================
Built-in Self-test Macros
==========================


.. doxygengroup:: group_capsense_macros_bist
   :project: capsense
   :members:
   :protected-members:
   :private-members:
   :undoc-members: