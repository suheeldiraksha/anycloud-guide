====================================
cy_stc_capsense_pin_config_t Struct
====================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Pin configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

GPIO_PRT_Type \* 
`pcPtr <structcy__stc__capsense__pin__config__t.html#a6e73d425b113f359b2d0911e44b36e62>`__
 
Pointer to the base port register of the IO.
 
uint8_t 
`pinNumber <structcy__stc__capsense__pin__config__t.html#a87a0da3c8c5c9f372c4bbfbdbf3498d7>`__
 
Position of the IO in the port.

