==============
API Reference
==============

.. toctree::
   :hidden:

   group__group__capsense__high__level.rst
   group__group__capsense__low__level.rst
   group__group__capsense__data__structure.rst
   group__group__capsense__enums.rst
   group__group__capsense__macros.rst
   group__group__capsense__callbacks.rst
   
   
Provides the list of API Reference

.. raw:: html

   <hr>

+----------------------------------+----------------------------------+
| \ `High-level                    | High-level functions represent   |
| Functions <group__group          | the highest abstraction layer of |
| __capsense__high__level.html>`__ | the CapSense middleware          |
+----------------------------------+----------------------------------+
| \ `Low-level                     | The low-level functions          |
| Functions <group__grou           | represent the lower layer of     |
| p__capsense__low__level.html>`__ | abstraction in support of        |
|                                  | `High-level                      |
|                                  | Functions <group__group          |
|                                  | __capsense__high__level.html>`__ |
+----------------------------------+----------------------------------+
| \ `CapSense Data                 | The CapSense Data Structure      |
| Structure <group__group__ca      | organizes configuration          |
| psense__data__structure.html>`__ | parameters, input, and output    |
|                                  | data shared among different FW   |
|                                  | modules within the CapSense      |
+----------------------------------+----------------------------------+
| \ `CapSense                      | The CapSense structures          |
| Structures <group__grou          |                                  |
| p__capsense__structures.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Gesture                       | The Gesture-related structures   |
| Structures <group__group__capsen |                                  |
| se__gesture__structures.html>`__ |                                  |
+----------------------------------+----------------------------------+
|  \ `Enumerated                   | Documents CapSense related       |
| Types <group_                    | enumerated types                 |
| _group__capsense__enums.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Macros <group__               | Specifies constants used in      |
| group__capsense__macros.html>`__ | CapSense middleware              |
+----------------------------------+----------------------------------+
| \ `General                       | General macros                   |
| Macros <group__group__ca         |                                  |
| psense__macros__general.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Settings                      | Settings macros                  |
| Macros <group__group__cap        |                                  |
| sense__macros__settings.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Pin-related                   | Pin-related macros               |
| Macros <group__group             |                                  |
| __capsense__macros__pin.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Processing                    | Processing macros                |
| Macros <group__group__ca         |                                  |
| psense__macros__process.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Touch-related                 | Touch-related macros             |
| Macros <group__group__           |                                  |
| capsense__macros__touch.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Gesture                       | Gesture macros                   |
| Macros <group__group__ca         |                                  |
| psense__macros__gesture.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Miscellaneous                 | Miscellaneous macros             |
| Macros <group__group__capsense   |                                  |
| __macros__miscellaneous.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Built-in Self-test            | Built-in Self-test macros        |
| Macros <group__group_            |                                  |
| _capsense__macros__bist.html>`__ |                                  |
+----------------------------------+----------------------------------+
| \ `Callbacks <group__gro         | Callbacks allow the user to      |
| up__capsense__callbacks.html>`__ | execute Custom code called from  |
|                                  | the CapSense middleware when an  |
|                                  | event occurs                     |
+----------------------------------+----------------------------------+


