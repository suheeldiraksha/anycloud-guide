==========================================
cy_stc_capsense_electrode_config_t Struct
==========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Electrode objects configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

const
`cy_stc_capsense_pin_config_t <structcy__stc__capsense__pin__config__t.html>`__
\* 
`ptrPin <structcy__stc__capsense__electrode__config__t.html#a2cdfd659151f2fc6d9e0f49aaa2058f7>`__
 
Pointer to pin configuration structure.
 
uint8_t 
`type <structcy__stc__capsense__electrode__config__t.html#a3f987328f5df9cd9fe4f6b4daf80ec8e>`__
 
Electrode type
`cy_en_capsense_eltd_t <group__group__capsense__enums.html#ga8d3df6022888bbb21e3e64e2b045274d>`__.
 
uint8_t 
`numPins <structcy__stc__capsense__electrode__config__t.html#a2ff8c3a5d8d8bd2fe260cf353064f54e>`__
 
Total number of pins in this sensor.

