======================================
cy_stc_capsense_tfzm_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture Two Finger Zoom context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__tfzm__context__t.html#a60cf7f1c4af96f6113fbe04db092a80e>`__
 
Touchdown position of the first touch.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition2 <structcy__stc__capsense__tfzm__context__t.html#a47690ba09642262dc60c9996471cf3a7>`__
 
Touchdown position of the second touch.
 
uint16_t 
`distanceX <structcy__stc__capsense__tfzm__context__t.html#aeba8c48b23a049c2c427c01d79f8cf4a>`__
 
History of X-axis displacement.
 
uint16_t 
`distanceY <structcy__stc__capsense__tfzm__context__t.html#af80b64aea8441ddd468baa43048846a4>`__
 
History of Y-axis displacement.
 
uint8_t 
`state <structcy__stc__capsense__tfzm__context__t.html#a4d55acf75ebf067266e51707eace00c2>`__
 
Gesture state.
 
uint8_t 
`debounce <structcy__stc__capsense__tfzm__context__t.html#a3a6d2543ec80c8af2b26ecd1e9cb4544>`__
 
Gesture debounce counter.

