================================================
cy_stc_capsense_offl_context_t Struct Reference
================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture One Finger Flick context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`touchStartTime1 <structcy__stc__capsense__offl__context__t.html#a3c51b81c7a63872b85d1abafc9f3e35d>`__
 
Touchdown time.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__offl__context__t.html#aa4bcab6ceb424cae93b48af4ba2ef1c0>`__
 
Touchdown position.
 
uint8_t 
`state <structcy__stc__capsense__offl__context__t.html#ab75c1891770f28ddf2e351352ad44ec9>`__
 
Gesture state.

