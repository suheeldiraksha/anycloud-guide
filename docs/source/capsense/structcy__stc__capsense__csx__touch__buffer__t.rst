==========================================
cy_stc_capsense_csx_touch_buffer_t Struct
==========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Internal CSX Touchpad buffer structure for CSX for Touchpads'
         processing.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

int32_t 
`distanceMap <structcy__stc__capsense__csx__touch__buffer__t.html#aea8ff4b04a46ed06f2e05ebdb85d2e7a>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__
\*\ `CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Buffer for distance map data.
 
int32_t 
`colMap <structcy__stc__capsense__csx__touch__buffer__t.html#a53be7999d05b8199561fad7160b0ff8a>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Buffer for column map data.
 
int32_t 
`rowMap <structcy__stc__capsense__csx__touch__buffer__t.html#a2235820c150af3e6594730e9031066de>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Buffer for row map data.
 
int32_t 
`minsMap <structcy__stc__capsense__csx__touch__buffer__t.html#a4478e1addf6cd6f858c74a742a76f79f>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Buffer for minimums map data.
 
`cy_stc_capsense_position_t <structcy__stc__capsense__position__t.html>`__ 
`newPeak <structcy__stc__capsense__csx__touch__buffer__t.html#a8ede0076f02b01e43bdf7d01a90e5d8b>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Touch Positions.
 
int8_t 
`fingerPosIndexMap <structcy__stc__capsense__csx__touch__buffer__t.html#af4749658a6668c77f5ed0c326cf6308e>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__\ +3u]
 
Buffer for index map data.
 
int8_t 
`linksMap <structcy__stc__capsense__csx__touch__buffer__t.html#a7046ab5dc8799356f0cc4730c855ed4a>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Buffer for linked map data.
 
int8_t 
`visitedMap <structcy__stc__capsense__csx__touch__buffer__t.html#a0c72b5063ccfa7e97c086c06d943ffc9>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Buffer for visited map data.
 
int8_t 
`markIndicesMap <structcy__stc__capsense__csx__touch__buffer__t.html#a895f5397bf9d29ad2c0cbd4fadb111fe>`__
[`CY_CAPSENSE_CSX_TOUCHPAD_MAX_PEAKS <group__group__capsense__macros__touch.html#ga3858658519030890061b252946477cd8>`__]
 
Buffer for mark map data.
 
uint8_t 
`newPeakNumber <structcy__stc__capsense__csx__touch__buffer__t.html#a497b8e96e84f765fc258f34f071aa999>`__
 
Number of detected peaks.
 
uint8_t 
`newActiveIdsMask <structcy__stc__capsense__csx__touch__buffer__t.html#a9267df7cd343b44ee9dddfdf64a4e210>`__
 
Mask of used IDs.
 

