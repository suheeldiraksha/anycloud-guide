======================================
cy_stc_capsense_bist_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares BIST Context Data Structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 

`testResultMask <structcy__stc__capsense__bist__context__t.html#a85203f9070ae007e98d8e86b716ea971>`__

 

| The bit mask of test results (PASS/FAIL)

 

uint16_t 

`wdgtCrcCalc <structcy__stc__capsense__bist__context__t.html#a21a8e2f2c74ad54372244ad94f9245b5>`__

 

| A calculated by test CRC for a widget context structure.

 

uint8_t 

`crcWdgtId <structcy__stc__capsense__bist__context__t.html#a8a26753c42ea1a7f25a5018246c6d941>`__

 

| The first CRC failed widget ID.

 

uint8_t 

`shortedWdId <structcy__stc__capsense__bist__context__t.html#a4d106f872eeea02719e146b4427c1d18>`__

 

| The first shorted to GND/VDDA/ELTD widget ID.

 

uint8_t 

`shortedSnsId <structcy__stc__capsense__bist__context__t.html#aefc11745803885a1b87cc4017288d7fa>`__

 

| The first shorted to GND/VDDA/ELTD sensor ID.

 

uint16_t \* 

`ptrWdgtCrc <structcy__stc__capsense__bist__context__t.html#af1dcb8fad4599fa893c49f69abac5724>`__

 

| The pointer to the widget CRC array.

 

uint32_t 

`shieldCap <structcy__stc__capsense__bist__context__t.html#ad9d7572ae68f02fff90133e676cc1bdb>`__

 

| The shield capacitance measurement result in femtofarads.

 

uint16_t 

`cModCap <structcy__stc__capsense__bist__context__t.html#a5456d1702855400f82a5e896cf426583>`__

 

| The Cmod capacitance measurement result in picofarads.

 

uint16_t 

`cIntACap <structcy__stc__capsense__bist__context__t.html#a0844067d1e2f03296d01c0946542673f>`__

 

| The CintA capacitance measurement result in picofarads.

 

uint16_t 

`cIntBCap <structcy__stc__capsense__bist__context__t.html#a5c87c14d0ef2d932eeb01a53eb77501b>`__

 

| The CIntB capacitance measurement result in picofarads.

 

uint16_t 

`cShieldCap <structcy__stc__capsense__bist__context__t.html#ad6d854327c533a7e1c2eb625a02dd8cb>`__

 

| The Cshield capacitance measurement result in picofarads.

 

uint16_t 

`vddaVoltage <structcy__stc__capsense__bist__context__t.html#acae5fb65a370e8e1c14d13c2da578f12>`__

 

| The result of VDDA measurement in millivolts.

 

`cy_en_capsense_bist_hw_config_t <group__group__capsense__enums.html#ga8f12924cdfe09cc22def0407e512c60b>`__ 

`hwConfig <structcy__stc__capsense__bist__context__t.html#a9bbdef6eb05a232f7086011dbea72715>`__

 

| A HW configuration for BIST operations.

 

`cy_en_capsense_bist_io_state_t <group__group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ 

`currentISC <structcy__stc__capsense__bist__context__t.html#a5ec842573f2642d54e2d76c26556d674>`__

 

| The current state of sensors when not being measured during the sensor
  capacitance measurement.

 

`cy_en_capsense_bist_io_state_t <group__group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ 

`shieldCapISC <structcy__stc__capsense__bist__context__t.html#a8e6f44610a21e97402349e14d6c090d1>`__

 

| The state of sensors when not being measured during the shield
  capacitance measurement.
  `More... <#a8e6f44610a21e97402349e14d6c090d1>`__

 

`cy_en_capsense_bist_io_state_t <group__group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ 

`eltdCapCsdISC <structcy__stc__capsense__bist__context__t.html#a22d5181fd636a7ff20a47559140e1e39>`__

 

| The state of sensors when not being measured during the CSD sensor
  capacitance measurement.
  `More... <#a22d5181fd636a7ff20a47559140e1e39>`__

 

`cy_en_capsense_bist_io_state_t <group__group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ 

`eltdCapCsxISC <structcy__stc__capsense__bist__context__t.html#a6370446fb7f72be84768cb8fb68c7375>`__

 

| The state of sensors when not being measured during the CSX sensor
  capacitance measurement.
  `More... <#a6370446fb7f72be84768cb8fb68c7375>`__

 

uint16_t 

`eltdCapModClk <structcy__stc__capsense__bist__context__t.html#a331645cdb0494af471f9bdd735ede166>`__

 

| The ModClk divider for electrode capacitance measurement scans.

 

uint16_t 

`eltdCapSnsClk <structcy__stc__capsense__bist__context__t.html#a6b8b7d2d90ea3d1dde78f8cea3f7f40c>`__

 

| The SnsClk divider for electrode capacitance measurement scans.

 

uint32_t 

`eltdCapSnsClkFreqHz <structcy__stc__capsense__bist__context__t.html#a3fecccd03e56148f208e64836aa070b4>`__

 

| The value of the SnsClk frequency is Hz.

 

uint16_t 

`eltdCapResolution <structcy__stc__capsense__bist__context__t.html#a234d9c4330bd5e4f6eaf0241d9b7da01>`__

 

| The resolution for electrode capacitance measurement scans.

 

uint16_t 

`eltdCapVrefMv <structcy__stc__capsense__bist__context__t.html#a00a9b297c1710c64237e41cf65b52dd4>`__

 

| The Vref value in mV for electrode capacitance measurement scans.

 

uint8_t 

`eltdCapVrefGain <structcy__stc__capsense__bist__context__t.html#a2acbf7fcbd54f316d5ed0cd6f0be6748>`__

 

| The Vref gain for electrode capacitance measurement scans.

 

uint8_t 

`fineInitTime <structcy__stc__capsense__bist__context__t.html#a5fc71d7b28788af5ec71a5e45c8ccf8e>`__

 

| Number of dummy SnsClk periods at fine initialization for BIST scans.

 

uint8_t 

`snsIntgShortSettlingTime <structcy__stc__capsense__bist__context__t.html#a5b5122344f3fac96dd7ba538eff80298>`__

 

| The sensor and shield short check time in microseconds.

 

uint16_t 

`capacitorSettlingTime <structcy__stc__capsense__bist__context__t.html#a2adb7abe65479371b8474bed393b719b>`__

 

| The maximum possible external capacitor charge/discharge time in
  microseconds.

 

uint16_t 

`vddaModClk <structcy__stc__capsense__bist__context__t.html#aa97a347c3230f3b8227df50a734e487c>`__

 

| The ModClk divider for VDDA measurements.

 

uint16_t 

`vddaVrefMv <structcy__stc__capsense__bist__context__t.html#a596d6e49bff364072a0ab877e14c90d5>`__

 

| The Vref value in mV for VDDA measurements.

 

uint8_t 

`vddaVrefGain <structcy__stc__capsense__bist__context__t.html#ada60c4a93a43cb77e509556a97962655>`__

 

| The Vref gain for VDDA measurements.

 

uint8_t 

`vddaIdacDefault <structcy__stc__capsense__bist__context__t.html#aa06208613dd9f24d6e61d762b57b7d5a>`__

 

| The IDAC default code for Vdda measurements.

 

uint8_t 

`vddaAzCycles <structcy__stc__capsense__bist__context__t.html#a069d1684c20cb8b73c04c4c5c109773e>`__

 

| The auto-zero time in Sns cycles for Vdda measurements.

 

uint8_t 

`vddaAcqCycles <structcy__stc__capsense__bist__context__t.html#a5d877d6bd67e8cf57bcb411cbf684abe>`__

 

| The acquisition time in Sns cycles - 1 for Vdda measurements.

 

uint16_t 

`extCapModClk <structcy__stc__capsense__bist__context__t.html#a8ffe130298947c266c56ddfdd64f8420>`__

 

| The ModClk divider for external capacitor capacity measurements.

 

uint16_t 

`extCapSnsClk <structcy__stc__capsense__bist__context__t.html#a35acc7ff82625d0f4ddd9e3c020ee406>`__

 

| The SnsClk divider for external capacitor capacity measurements.

 

uint16_t 

`extCapVrefMv <structcy__stc__capsense__bist__context__t.html#a64ff2a0e0441bb1d7c9c1062283e897b>`__

 

| The Vref value in mV for external capacitor capacity measurements.

 

uint8_t 

`extCapVrefGain <structcy__stc__capsense__bist__context__t.html#af41b354a7c567232a7572a73ed0bc5b9>`__

 

| The Vref gain for external capacitor capacitance measurements.

 

uint32_t 

`extCapIdacPa <structcy__stc__capsense__bist__context__t.html#a02ec16251b0f3ad575db0d179f879c87>`__

 

| The IDAC value in pA for external capacitor capacity measurements.

 

uint16_t 

`extCapWDT <structcy__stc__capsense__bist__context__t.html#a71c6e39161ae61c940eb57c948efbf3c>`__

 

| The SW watchdog timeout used to prevent a hang in case of short.

 

uint32_t 

`regSwHsPSelScan <structcy__stc__capsense__bist__context__t.html#ae9b8e85cb52436b653b5e7d1fb1e5133>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwHsPSelCmodInit <structcy__stc__capsense__bist__context__t.html#a8e341ec1a291ce5bc030fcf60610fca9>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwHsPSelCtankInit <structcy__stc__capsense__bist__context__t.html#a338ca675ef90b7fdb80ab9a95eba5f8d>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwDsiSel <structcy__stc__capsense__bist__context__t.html#ac8babb76107f68ffc3b92736fb5f3a72>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwShieldSelScan <structcy__stc__capsense__bist__context__t.html#af8eeec145da8af1555e3bea62c8a2b6a>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwResInit <structcy__stc__capsense__bist__context__t.html#a20d613bddf0e9ea1dcb6196a579ce414>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwResScan <structcy__stc__capsense__bist__context__t.html#ade86e6ef5b7e98386370aa2c567ee60e>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwBypSel <structcy__stc__capsense__bist__context__t.html#a3aeb8661cd85b1aa2f266cee60be0240>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwAmuxbufSel <structcy__stc__capsense__bist__context__t.html#a8e0f79069bf4362e97a1fb82d0166bb6>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regAmbuf <structcy__stc__capsense__bist__context__t.html#ae8f8770f51182beb960139917bceb6e3>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regHscmpScan <structcy__stc__capsense__bist__context__t.html#ac44249ae8e20f8dfb19e42566fad97d0>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwRefgenSel <structcy__stc__capsense__bist__context__t.html#a8d0e2a3ef5a233e8da6471eedd227f08>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regConfig <structcy__stc__capsense__bist__context__t.html#ae5f47666b04bc546cc716cd77c00446b>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regIoSel <structcy__stc__capsense__bist__context__t.html#aad9f6bc6d2d8f7e2e9843a0013c43f55>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regAmbufShield <structcy__stc__capsense__bist__context__t.html#a2ab8cac868956282420db3b54a7e0978>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regHscmpScanShield <structcy__stc__capsense__bist__context__t.html#ad6fcce2910023a51b7f479a62d0b12ce>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwShieldSelScanShield <structcy__stc__capsense__bist__context__t.html#ac1503ac45a53e4be199d38ed77fee26f>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwHsPSelScanShield <structcy__stc__capsense__bist__context__t.html#a25926bffe9b5af767f6db69988b7a506>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwBypSelShield <structcy__stc__capsense__bist__context__t.html#a68ebea3b267bf015d1e9e9802693a1c7>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regSwAmuxbufSelShield <structcy__stc__capsense__bist__context__t.html#a82f8b8b8d650377c292bd35b4188f070>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regConfigShield <structcy__stc__capsense__bist__context__t.html#a1ab075dfb6504435139ec6b7bf68a42b>`__

 

| Internal pre-calculated data for faster operation.

 

uint32_t 

`regIoSelShield <structcy__stc__capsense__bist__context__t.html#a20a4beb0abe24168be77ad9072590b57>`__

 

| Internal pre-calculated data for faster operation.

 

Field Documentation
--------------------

`◆  <#a8e6f44610a21e97402349e14d6c090d1>`__\ shieldCapISC
=========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_capsense_bist_io_state_t <group                               |
      | __group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ |
      | cy_stc_capsense_bist_context_t::shieldCapISC                         |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The state of sensors when not being measured during the shield
      capacitance measurement.

      -  CY_CAPSENSE_SNS_CONNECTION_HIGHZ
      -  CY_CAPSENSE_SNS_CONNECTION_SHIELD
      -  CY_CAPSENSE_SNS_CONNECTION_GROUND

`◆  <#a22d5181fd636a7ff20a47559140e1e39>`__\ eltdCapCsdISC
==========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_capsense_bist_io_state_t <group                               |
      | __group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ |
      | cy_stc_capsense_bist_context_t::eltdCapCsdISC                        |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The state of sensors when not being measured during the CSD sensor
      capacitance measurement.

      The states are the same as of the previous parameter

`◆  <#a6370446fb7f72be84768cb8fb68c7375>`__\ eltdCapCsxISC
==========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_capsense_bist_io_state_t <group                               |
      | __group__capsense__enums.html#ga0e370bf700c29e03461030db2779ceec>`__ |
      | cy_stc_capsense_bist_context_t::eltdCapCsxISC                        |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The state of sensors when not being measured during the CSX sensor
      capacitance measurement.

      -  CY_CAPSENSE_SNS_CONNECTION_HIGHZ
      -  CY_CAPSENSE_SNS_CONNECTION_GROUND

