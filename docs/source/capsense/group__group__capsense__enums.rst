=================
Enumerated Types
=================


.. doxygengroup:: group_capsense_enums
   :project: capsense
   :members:
   :protected-members:
   :private-members:
   :undoc-members: