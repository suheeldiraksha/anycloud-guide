====================
Arduino Header Pins
====================

.. doxygengroup:: group_bsp_pins_arduino
   :project: TARGET_CY8CKIT-064S0S2-4343W
   :members:
   :protected-members:
   :private-members:
   :undoc-members: