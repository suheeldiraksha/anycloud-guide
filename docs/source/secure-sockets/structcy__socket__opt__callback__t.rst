==========================================
cy_socket_opt_callback_t Struct Reference
==========================================



.. doxygenstruct:: cy_socket_opt_callback_t
   :project: secure-sockets
   :members:
   :protected-members:
   :private-members:
   :undoc-members: