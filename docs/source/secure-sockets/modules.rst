==============
API Reference
==============

.. raw:: html

   <hr>

.. toctree::
   :hidden:
   
   group__group__secure__sockets.rst
   group__group__cy__tls.rst
  
   


The following provides a list of driver API documentation

+----------------------------------+----------------------------------+
| \ `Cypress Secure Sockets        | The Secure Sockets API provides  |
| API <group_                      | functions to communicate over    |
| _group__secure__sockets.html>`__ | the IP network                   |
+----------------------------------+----------------------------------+
| \ `Cypress TLSAPI                | The TLS API provides functions   |
| <group__group__cy__tls.html>`__  | for secure communication over    |
|                                  | the IP network                   |
+----------------------------------+----------------------------------+


