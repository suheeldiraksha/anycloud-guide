======================================
cy_socket_sockaddr_t Struct Reference
======================================



.. doxygenstruct:: cy_socket_sockaddr_t
   :project: secure-sockets
   :members:
   :protected-members:
   :private-members:
   :undoc-members: