========================================
cy_socket_ip_address_t Struct Reference
========================================



.. doxygenstruct:: cy_socket_ip_address_t
   :project: secure-sockets
   :members:
   :protected-members:
   :private-members:
   :undoc-members: