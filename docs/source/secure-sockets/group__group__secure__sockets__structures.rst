===========
Structures
===========




.. doxygengroup:: group_secure_sockets_structures
   :project: secure-sockets
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


.. toctree::

   structcy__socket__ip__address__t.rst
   structcy__socket__sockaddr__t.rst
   structcy__socket__opt__callback__t.rst
   
   