=============
BSP Settings
=============

.. doxygengroup:: group_bsp_pins_settings
   :project: TARGET_PSVP-Player28
   :members:
   :protected-members:
   :private-members:
   :undoc-members: