=============
Pin Mappings
=============
.. doxygengroup:: group_bsp_pins
   :project: udb-sdio-whd
   :members:
   :protected-members:
   :private-members:
   :undoc-members: