<?xml version='1.0' encoding='UTF-8' standalone='no'?>
<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="compound.xsd" version="1.8.14">
  <compounddef id="_r_e_a_d_m_e_8doxygen_8md" kind="file" language="Markdown">
    <compoundname>README.doxygen.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp/>UDB<sp/>SDIO<sp/>for<sp/>Wi-Fi<sp/>Host<sp/>Driver</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">###<sp/>Overview</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">This<sp/>library<sp/>provides<sp/>a<sp/>UDB<sp/>based<sp/>SDIO<sp/>interface<sp/>that<sp/>allows<sp/>for<sp/>communicating<sp/>between<sp/>a<sp/>PSoC<sp/>6<sp/>and<sp/>a<sp/>wireless<sp/>device<sp/>such<sp/>as<sp/>the<sp/>CYW4343W,<sp/>CYW43438,<sp/>or<sp/>CYW43012.<sp/>This<sp/>library<sp/>allows<sp/>PSoC<sp/>6<sp/>devices<sp/>that<sp/>do<sp/>not<sp/>have<sp/>a<sp/>dedicated<sp/>SDHC<sp/>hardware<sp/>block,<sp/>but<sp/>do<sp/>have<sp/>UDBs,<sp/>to<sp/>work<sp/>with<sp/>the<sp/>[Wi-Fi<sp/>Host<sp/>Driver<sp/>(WHD)](https://github.com/cypresssemiconductorco/wifi-host-driver)<sp/>library.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">**NOTE:**<sp/>This<sp/>library<sp/>does<sp/>not<sp/>provide<sp/>a<sp/>complete<sp/>SDIO<sp/>implementation.<sp/>It<sp/>is<sp/>only<sp/>intended<sp/>for<sp/>use<sp/>with<sp/>a<sp/>wireless<sp/>device.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">**NOTE:**<sp/>This<sp/>library<sp/>is<sp/>only<sp/>compatible<sp/>with<sp/>PSoC<sp/>6<sp/>Board<sp/>Support<sp/>Packages<sp/>(BSPs)<sp/>version<sp/>1.2.0<sp/>and<sp/>later.<sp/>Prior<sp/>to<sp/>this<sp/>version,<sp/>portions<sp/>of<sp/>this<sp/>library<sp/>were<sp/>directly<sp/>included<sp/>as<sp/>part<sp/>of<sp/>the<sp/>BSP.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">###<sp/>Whats<sp/>included</highlight></codeline>
<codeline><highlight class="normal">There<sp/>are<sp/>three<sp/>configurations<sp/>of<sp/>this<sp/>library<sp/>to<sp/>choose<sp/>from<sp/>based<sp/>on<sp/>what<sp/>PSoC<sp/>6<sp/>pins<sp/>are<sp/>intended<sp/>for<sp/>SDIO<sp/>communication.<sp/>Selection<sp/>of<sp/>which<sp/>port<sp/>to<sp/>use<sp/>is<sp/>done<sp/>by<sp/>specifying<sp/>the<sp/>appropriate<sp/>component<sp/>to<sp/>the<sp/>makefile.<sp/>Details<sp/>for<sp/>this<sp/>are<sp/>described<sp/>in<sp/>the<sp/>Quick<sp/>Start<sp/>section<sp/>below.<sp/>The<sp/>table<sp/>below<sp/>shows<sp/>the<sp/>supported<sp/>ports<sp/>and<sp/>which<sp/>pins<sp/>on<sp/>that<sp/>port<sp/>map<sp/>to<sp/>which<sp/>SDIO<sp/>function.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">|<sp/>SDIO<sp/>Function<sp/>|<sp/>Port<sp/>2<sp/>|<sp/>Port<sp/>9<sp/>|<sp/>Port<sp/>12<sp/>|</highlight></codeline>
<codeline><highlight class="normal">|<sp/>-------------<sp/>|<sp/>------<sp/>|<sp/>------<sp/>|<sp/>-------<sp/>|</highlight></codeline>
<codeline><highlight class="normal">|<sp/>SDIO_D0<sp/><sp/><sp/><sp/><sp/><sp/><sp/>|<sp/>P2_0<sp/><sp/><sp/>|<sp/>P9_0<sp/><sp/><sp/>|<sp/>P12_0<sp/><sp/><sp/>|</highlight></codeline>
<codeline><highlight class="normal">|<sp/>SDIO_D1<sp/><sp/><sp/><sp/><sp/><sp/><sp/>|<sp/>P2_1<sp/><sp/><sp/>|<sp/>P9_1<sp/><sp/><sp/>|<sp/>P12_1<sp/><sp/><sp/>|</highlight></codeline>
<codeline><highlight class="normal">|<sp/>SDIO_D2<sp/><sp/><sp/><sp/><sp/><sp/><sp/>|<sp/>P2_2<sp/><sp/><sp/>|<sp/>P9_2<sp/><sp/><sp/>|<sp/>P12_2<sp/><sp/><sp/>|</highlight></codeline>
<codeline><highlight class="normal">|<sp/>SDIO_D3<sp/><sp/><sp/><sp/><sp/><sp/><sp/>|<sp/>P2_3<sp/><sp/><sp/>|<sp/>P9_3<sp/><sp/><sp/>|<sp/>P12_3<sp/><sp/><sp/>|</highlight></codeline>
<codeline><highlight class="normal">|<sp/>SDIO_CMD<sp/><sp/><sp/><sp/><sp/><sp/>|<sp/>P2_4<sp/><sp/><sp/>|<sp/>P9_4<sp/><sp/><sp/>|<sp/>P12_4<sp/><sp/><sp/>|</highlight></codeline>
<codeline><highlight class="normal">|<sp/>SDIO_CLK<sp/><sp/><sp/><sp/><sp/><sp/>|<sp/>P2_5<sp/><sp/><sp/>|<sp/>P9_5<sp/><sp/><sp/>|<sp/>P12_5<sp/><sp/><sp/>|</highlight></codeline>
<codeline></codeline>
<codeline></codeline>
<codeline><highlight class="normal">###<sp/>Quick<sp/>Start</highlight></codeline>
<codeline><highlight class="normal">1.<sp/>Update<sp/>the<sp/>application<sp/>or<sp/>BSP<sp/>makefile<sp/>to<sp/>indicate<sp/>which<sp/>port<sp/>to<sp/>use<sp/>for<sp/>SDIO<sp/>communication</highlight></codeline>
<codeline><highlight class="normal">*<sp/>For<sp/>Port<sp/>2:<sp/>```COMPONENTS+=UDB_SDIO_P2```</highlight></codeline>
<codeline><highlight class="normal">*<sp/>For<sp/>Port<sp/>9:<sp/>```COMPONENTS+=UDB_SDIO_P9```</highlight></codeline>
<codeline><highlight class="normal">*<sp/>For<sp/>Port<sp/>12:<sp/>```COMPONENTS+=UDB_SDIO_P12```</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">2.<sp/>Update<sp/>the<sp/>application<sp/>or<sp/>BSP<sp/>makefile<sp/>to<sp/>indicate<sp/>that<sp/>Wi-Fi<sp/>is<sp/>supported</highlight></codeline>
<codeline><highlight class="normal">```DEFINES+=CYBSP_WIFI_CAPABLE```</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">3.<sp/>Add<sp/>the<sp/>WHD<sp/>and<sp/>associated<sp/>wireless<sp/>libraries<sp/>to<sp/>the<sp/>application</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">4.<sp/>Refer<sp/>to<sp/>the<sp/>WHD<sp/>documentation<sp/>for<sp/>setting<sp/>up<sp/>the<sp/>networking<sp/>interface</highlight></codeline>
<codeline></codeline>
<codeline></codeline>
<codeline><highlight class="normal">###<sp/>Restrictions</highlight></codeline>
<codeline><highlight class="normal">To<sp/>use<sp/>this<sp/>library,<sp/>the<sp/>following<sp/>must<sp/>be<sp/>true:</highlight></codeline>
<codeline><highlight class="normal">1.<sp/>ClkSlow<sp/>&amp;<sp/>ClkPeri<sp/>must<sp/>both<sp/>run<sp/>at<sp/>the<sp/>same<sp/>speed</highlight></codeline>
<codeline><highlight class="normal">2.<sp/>ClkSlow<sp/>&amp;<sp/>ClkPeri<sp/>must<sp/>run<sp/>at<sp/>4x<sp/>the<sp/>desired<sp/>SDIO<sp/>speed</highlight></codeline>
<codeline><highlight class="normal">3.<sp/>The<sp/>first<sp/>8-bit<sp/>peripheral<sp/>clock<sp/>divider<sp/>must<sp/>be<sp/>reserved<sp/>for<sp/>use<sp/>by<sp/>this<sp/>driver</highlight></codeline>
<codeline><highlight class="normal">4.<sp/>The<sp/>following<sp/>DMA<sp/>channels<sp/>must<sp/>be<sp/>reserved<sp/>for<sp/>use<sp/>by<sp/>this<sp/>driver</highlight></codeline>
<codeline><highlight class="normal">*<sp/>DataWire<sp/>0<sp/>channel<sp/>0</highlight></codeline>
<codeline><highlight class="normal">*<sp/>DataWire<sp/>0<sp/>channel<sp/>1</highlight></codeline>
<codeline><highlight class="normal">*<sp/>DataWire<sp/>1<sp/>channel<sp/>1</highlight></codeline>
<codeline><highlight class="normal">*<sp/>DataWire<sp/>1<sp/>channel<sp/>3</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">**NOTE:**<sp/>The<sp/>optimal<sp/>configuration<sp/>is<sp/>to<sp/>have<sp/>ClkSlow<sp/>and<sp/>ClkPeri<sp/>running<sp/>at<sp/>100<sp/>MHz<sp/>and<sp/>for<sp/>the<sp/>SDIO<sp/>to<sp/>run<sp/>at<sp/>25<sp/>MHz.</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">###<sp/>More<sp/>information</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">*<sp/>[API<sp/>Reference<sp/>Guide](https://cypresssemiconductorco.github.io/udb-sdio-whd/html/index.html)</highlight></codeline>
<codeline><highlight class="normal">*<sp/>[Cypress<sp/>Semiconductor,<sp/>an<sp/>Infineon<sp/>Technologies<sp/>Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp/>[Cypress<sp/>Semiconductor<sp/>GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
<codeline><highlight class="normal">*<sp/>[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline><highlight class="normal">*<sp/>[Wi-Fi<sp/>Host<sp/>Driver](https://github.com/cypresssemiconductorco/wifi-host-driver)</highlight></codeline>
<codeline><highlight class="normal">*<sp/>[PSoC<sp/>6<sp/>Code<sp/>Examples<sp/>using<sp/>ModusToolbox<sp/>IDE](https://github.com/cypresssemiconductorco/Code-Examples-for-ModusToolbox-Software)</highlight></codeline>
<codeline><highlight class="normal">*<sp/>[PSoC<sp/>6<sp/>Middleware](https://github.com/cypresssemiconductorco/psoc6-middleware)</highlight></codeline>
<codeline><highlight class="normal">*<sp/>[PSoC<sp/>6<sp/>Resources<sp/>-<sp/>KBA223067](https://community.cypress.com/docs/DOC-14644)</highlight></codeline>
<codeline></codeline>
<codeline><highlight class="normal">---</highlight></codeline>
<codeline><highlight class="normal">©<sp/>Cypress<sp/>Semiconductor<sp/>Corporation,<sp/>2019-2020.</highlight></codeline>
    </programlisting>
    <location file="bsp/libs/udb-sdio-whd/README.doxygen.md"/>
  </compounddef>
</doxygen>
