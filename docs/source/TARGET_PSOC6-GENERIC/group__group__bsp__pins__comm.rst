===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_PSOC6-GENERIC
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
