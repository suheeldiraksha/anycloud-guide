=======
Macros
=======
.. doxygengroup:: group_wcm_macros
   :project: wifi-connection-manager
   :members:
   :protected-members:
   :private-members:
   :undoc-members:



.. toctree::

   group__generic__wcm__defines.rst

