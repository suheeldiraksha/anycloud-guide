==================================
cy_wcm_wps_device_detail_t Struct
==================================


.. doxygenstruct:: cy_wcm_wps_device_detail_t
   :project: wifi-connection-manager
   :members:
   :protected-members:
   :private-members:
   :undoc-members: