============================
cy_wcm_scan_result_t Struct
============================

.. doxygenstruct:: cy_wcm_scan_result_t
   :project: wifi-connection-manager
   :members:
   :protected-members:
   :private-members:
   :undoc-members: