===========================
cy_wcm_wps_config_t Struct
===========================

.. doxygenstruct:: cy_wcm_wps_config_t
   :project: wifi-connection-manager
   :members:
   :protected-members:
   :private-members:
   :undoc-members: