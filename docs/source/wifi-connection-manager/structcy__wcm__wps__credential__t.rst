===============================
cy_wcm_wps_credential_t Struct
===============================

.. doxygenstruct:: cy_wcm_wps_credential_t
   :project: wifi-connection-manager
   :members:
   :protected-members:
   :private-members:
   :undoc-members: