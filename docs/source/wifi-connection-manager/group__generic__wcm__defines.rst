========================
WCM results/error codes
========================

.. doxygengroup:: generic_wcm_defines
   :project: wifi-connection-manager
   :members:
   :protected-members:
   :private-members:
   :undoc-members: