============================================================================
cy_tko_ol_cfg_t Struct Reference
============================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         User uses configurator to set these.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`interval <structcy__tko__ol__cfg__t.html#ac869b5fce49f3fb81b86b0998e40c930>`__
 
Interval (in seconds) between keepalives.
 
uint16_t 
`retry_interval <structcy__tko__ol__cfg__t.html#a8fab23a40b0f3bc0a4dbc920c9dfaa2f>`__
 
If a keepalive is not Acked, retry after this many seconds.
 
uint16_t 
`retry_count <structcy__tko__ol__cfg__t.html#a33a573993dd0a6f5ddb2c247b381c585>`__
 
Retry up to this many times.
 
`cy_tko_ol_connect_t <structcy__tko__ol__connect__t.html>`__ 
`ports <structcy__tko__ol__cfg__t.html#ad4979c12e1b68e5432f3c806ef2da447>`__
[MAX_TKO]
 
Port and IP address ofr each connection.
 


