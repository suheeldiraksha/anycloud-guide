=========================================================================
cy_pf_port_t Struct Reference
=========================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Usually a single port is filtered but port ranges are also
         supported, where the range is from portnum thru portnum +
         range.

         Port ranges are expected to be used with short lived ephemeral
         source port numbers. Port numbers are described in numerous
         places such
         https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 

`portnum <structcy__pf__port__t.html#aaee6c242184bccef5dee844c3e7a61bf>`__

 

| Port number. `More... <#aaee6c242184bccef5dee844c3e7a61bf>`__

 

uint16_t 

`range <structcy__pf__port__t.html#ac90eb053e2c28a2506b59dae1f4f7520>`__

 

| Range: Allows a block of portnumbers to be filtered.
  `More... <#ac90eb053e2c28a2506b59dae1f4f7520>`__

 

`cy_pn_direction_t <group__group__lpa__enums.html#ga0093febf4de86e258f6c65668742c050>`__ 

`direction <structcy__pf__port__t.html#ab4abd771b871b449f9002692206860c3>`__

 

| Source or Destination port.
  `More... <#ab4abd771b871b449f9002692206860c3>`__

 

Field Documentation
===================

`◆  <#aaee6c242184bccef5dee844c3e7a61bf>`__\ portnum
====================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------+
      | uint16_t cy_pf_port_t::portnum |
      +--------------------------------+

   .. container:: memdoc

      Port number.

      Any 16 bit port number can be specified. For a description and
      list of port numbers see:
      https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers

`◆  <#ac90eb053e2c28a2506b59dae1f4f7520>`__\ range
==================================================

.. container:: memitem

   .. container:: memproto

      +------------------------------+
      | uint16_t cy_pf_port_t::range |
      +------------------------------+

   .. container:: memdoc

      Range: Allows a block of portnumbers to be filtered.

      Range must be of the form 2^y - 1 (511, 1023, etc) and must be
      less than portnum. When using range, portnum must be of the form
      2^x (256, 512, etc). Example: portnum == 16384, range = 1023 will
      filter ports 16384-17407

      If any of these checks for using ranges fails, fallback to
      filtering for just 'portnum'.

`◆  <#ab4abd771b871b449f9002692206860c3>`__\ direction
======================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_pn_direction_t <                                                 |
      | group__group__lpa__enums.html#ga0093febf4de86e258f6c65668742c050>`__ |
      | cy_pf_port_t::direction                                              |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Source or Destination port.

      Dest is default unless source port override is on


