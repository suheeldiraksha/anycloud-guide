======================================================================
ol_info_t Struct Reference
======================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Offload information.

         Context pointers that an offload may use to accomplish its
         function.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

whd_t \* 

`whd <structol__info__t.html#a81146b644c5eb9b2c6719976d9ef25d9>`__

 

| Wireless Host Driver pointer.
  `More... <#a81146b644c5eb9b2c6719976d9ef25d9>`__

 

void \* 

`ip <structol__info__t.html#ad7bf118a31201493e8f5c3dd550981d6>`__

 

| IP network stack pointer.
  `More... <#ad7bf118a31201493e8f5c3dd550981d6>`__

 

void \* 

`worker <structol__info__t.html#ac8e21f872766ab95620106fb4461b8a2>`__

 

| Worker Thread info pointer.

 

Field Documentation
===================

`◆  <#a81146b644c5eb9b2c6719976d9ef25d9>`__\ whd
================================================

.. container:: memitem

   .. container:: memproto

      +------------------------+
      | whd_t\* ol_info_t::whd |
      +------------------------+

   .. container:: memdoc

      Wireless Host Driver pointer.

`◆  <#ad7bf118a31201493e8f5c3dd550981d6>`__\ ip
===============================================

.. container:: memitem

   .. container:: memproto

      +----------------------+
      | void\* ol_info_t::ip |
      +----------------------+

   .. container:: memdoc

      IP network stack pointer.


