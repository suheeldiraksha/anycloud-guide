=====================================================================
arp_ol_t Struct Reference
=====================================================================


.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         ARP Offload context - this is a private structure; visible to
         allow for static definition.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

char 
`name <structarp__ol__t.html#a5c5d17fd2782ce52a39ebe95c985a849>`__ [4]
 
ARP.
 
const `arp_ol_cfg_t <structarp__ol__cfg__t.html>`__ \* 
`config <structarp__ol__t.html#a80b1b8f2693769dc8a768d4ecbd06ee4>`__
 
pointer to configuration from Configurator
`arp_ol_cfg_t <structarp__ol__cfg__t.html>`__
 
`ol_info_t <structol__info__t.html>`__ \* 
`ol_info_ptr <structarp__ol__t.html#a44385b0f96b0108b308c80b2a480d4ee>`__
 
Offload Manager Info structure `ol_info_t <structol__info__t.html>`__.
 
uint32_t 
`ip_address <structarp__ol__t.html#a8ade24a1d65f167cd373b8222ebc421b>`__
 
IP Address for this interface.
 
`arp_ol_config_state_t <group__group__lpa__enums.html#gacaa1d0b548141e9016a65d6e49f1cc46>`__ 
`state <structarp__ol__t.html#aa2e6c553b6b7c1c536131a5174403e76>`__
 
Currently written state in Device
`arp_ol_config_state_t <group__group__lpa__enums.html#gacaa1d0b548141e9016a65d6e49f1cc46>`__.
 


