================================================================================
cy_pf_ethtype_cfg_t Struct Reference
================================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Describes an ethertype filter (type CY_PF_OL_FEAT_ETHTYPE)

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 

`eth_type <structcy__pf__ethtype__cfg__t.html#a6b8fe67c64577674f6870fa084512df1>`__

 

| 16 bit value in bytes 13 & 14 of Ethernet header.
  `More... <#a6b8fe67c64577674f6870fa084512df1>`__

 

Field Documentation
===================

`◆  <#a6b8fe67c64577674f6870fa084512df1>`__\ eth_type
=====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------+
      | uint16_t cy_pf_ethtype_cfg_t::eth_type |
      +----------------------------------------+

   .. container:: memdoc

      16 bit value in bytes 13 & 14 of Ethernet header.

      For description and list of ethertypes see
      https://en.wikipedia.org/wiki/EtherType


