===========================================================================
cy_pf_ip_cfg_t Struct Reference
===========================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Describes an IP type filter (type CY_PF_OL_FEAT_IPTYPE)

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint8_t 

`ip_type <structcy__pf__ip__cfg__t.html#ad1a118337de3c5303357615d5c1d2505>`__

 

| 8 bit value in byte 10 of ipv4 header.
  `More... <#ad1a118337de3c5303357615d5c1d2505>`__

 

Field Documentation
===================

`◆  <#ad1a118337de3c5303357615d5c1d2505>`__\ ip_type
====================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------------+
      | uint8_t cy_pf_ip_cfg_t::ip_type |
      +---------------------------------+

   .. container:: memdoc

      8 bit value in byte 10 of ipv4 header.

      For a list of IP protocol numbers see
      https://en.wikipedia.org/wiki/List_of_IP_protocol_numbers


