=====================================================================
tko_ol_t Struct Reference
=====================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Keep pointers to config space, system handle, etc.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_tko_ol_cfg_t <structcy__tko__ol__cfg__t.html>`__ \* 
`cfg <structtko__ol__t.html#ab9645b98cf75b8e3c694f20e7dbd0cc3>`__
 
Pointer to config space.
 
void \* 
`whd <structtko__ol__t.html#ad3dadda42d16f6f71396c72e2c0f2987>`__
 
Pointer to system handle.
 

