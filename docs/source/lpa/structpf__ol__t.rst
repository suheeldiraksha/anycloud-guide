====================================================================
pf_ol_t Struct Reference
====================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Keep pointers to config space, system handle, etc.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_pf_ol_cfg_t <structcy__pf__ol__cfg__t.html>`__ \* 
`cfg <structpf__ol__t.html#a914b22be2a3c8240fecd61895f36c0b7>`__
 
Pointer to config space.
 
void \* 
`whd <structpf__ol__t.html#a3fa4c35aca34e12f15bf8ab5ac29af2f>`__
 
Pointer to system handle.
 


