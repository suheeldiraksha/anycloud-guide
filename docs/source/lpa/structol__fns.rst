=====================================================================
ol_fns_t Struct Reference
=====================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Offload power management notification.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`ol_init_t <group__group__lpa__internal.html#ga9def4ca12e21880657322882de19622b>`__
\* 

`init <structol__fns.html#a20426e784f4c9470faa6bf9bff362491>`__

 

| Offload initialization function.
  `More... <#a20426e784f4c9470faa6bf9bff362491>`__

 

`ol_deinit_t <group__group__lpa__internal.html#ga430866a43c3edc038156f38680ede8fd>`__
\* 

`deinit <structol__fns.html#a79f79f19d34ce0bfaff153744cdb994d>`__

 

| Offload deinitialization function.
  `More... <#a79f79f19d34ce0bfaff153744cdb994d>`__

 

`ol_pm_t <group__group__lpa__internal.html#ga7b77ef0d7e6402fab179216dd39ec35b>`__
\* 

`pm <structol__fns.html#a8668c8781d64582231c8454f863c8a4b>`__

 

| Offload host power-mode changed handler function.
  `More... <#a8668c8781d64582231c8454f863c8a4b>`__

 

Field Documentation
===================

`◆  <#a20426e784f4c9470faa6bf9bff362491>`__\ init
=================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `ol_init_t <group__                                                  |
      | group__lpa__internal.html#ga9def4ca12e21880657322882de19622b>`__\ \* |
      | ol_fns_t::init                                                       |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Offload initialization function.

`◆  <#a79f79f19d34ce0bfaff153744cdb994d>`__\ deinit
===================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `ol_deinit_t <group__                                                |
      | group__lpa__internal.html#ga430866a43c3edc038156f38680ede8fd>`__\ \* |
      | ol_fns_t::deinit                                                     |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Offload deinitialization function.

`◆  <#a8668c8781d64582231c8454f863c8a4b>`__\ pm
===============================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `ol_pm_t <group__                                                    |
      | group__lpa__internal.html#ga7b77ef0d7e6402fab179216dd39ec35b>`__\ \* |
      | ol_fns_t::pm                                                         |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Offload host power-mode changed handler function.


