================================================================================
cy_tko_ol_connect_t Struct Reference
================================================================================


.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         User uses configurator to set these.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`local_port <structcy__tko__ol__connect__t.html#a0c22be2daaa3a4baa61bccc472d7b245>`__
 
Local port num for this socket.
 
uint16_t 
`remote_port <structcy__tko__ol__connect__t.html#a9e92176a589e0faea7665b3fbc1f35da>`__
 
Remote port num for this socket.
 
char 
`remote_ip <structcy__tko__ol__connect__t.html#a0b8520047f2469edea33da124e857cf5>`__
[16]
 
IP address of remote side.
 



