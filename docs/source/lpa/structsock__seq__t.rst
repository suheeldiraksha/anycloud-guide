=======================================================================
sock_seq_t Struct Reference
=======================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Get TCP socket sequence number info from network stack.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

whd_mac_t 
`src_mac <structsock__seq__t.html#ab16109c333c6f2db454617d7eca72fb6>`__
 
local mac address
 
whd_mac_t 
`dst_mac <structsock__seq__t.html#a731fd9832c22bb23f1ca2ff5cee5c9d1>`__
 
remote mac address
 
uint32_t 
`srcip <structsock__seq__t.html#a8bdf5e64c5112d560db5ab0bbee5e28a>`__
 
local IP address
 
uint32_t 
`dstip <structsock__seq__t.html#a1e03cb3c0bea2ed3cad3c3eac7e5c2c0>`__
 
destination IP address
 
uint16_t 
`srcport <structsock__seq__t.html#a4b8255623d7844722ce842fd87006458>`__
 
Local TCP port.
 
uint16_t 
`dstport <structsock__seq__t.html#a0055efec1a4faedb80ecba0efc2a81ab>`__
 
Remote TCP port.
 
uint32_t 
`seqnum <structsock__seq__t.html#adf9acf90ca9ecc5b6dad93b5e7cce77b>`__
 
Current sequence number for this socket.
 
uint32_t 
`acknum <structsock__seq__t.html#a74d598287c7c57706b4f158708282bd6>`__
 
Current ack number for this socket.
 
uint16_t 
`rx_window <structsock__seq__t.html#a6e6a11a613192c4d761bca2df89d1307>`__
 
Current TCP rx window size.
 


