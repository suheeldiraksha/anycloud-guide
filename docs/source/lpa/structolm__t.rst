==================================================================
olm_t Struct Reference
==================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Offload Manager context.

         Private structure; visible to allow for static definition.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

const struct ol_desc \* 
`ol_list <structolm__t.html#a2b190f623aefb6f3c6e51cc62b751789>`__
 
Offload Assist list.
 
`ol_info_t <structol__info__t.html>`__ 
`ol_info <structolm__t.html#aee9525f191806455dab9798b5d520e29>`__
 
Offload info.
 

