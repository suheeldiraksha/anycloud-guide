====================
Arduino Header Pins
====================

.. doxygengroup:: group_bsp_pins_arduino
   :project: TARGET_CY8CKIT-062-WIFI-BT
   :members:
   :protected-members:
   :private-members:
   :undoc-members: