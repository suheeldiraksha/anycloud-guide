=========================================
cy_stc_csdidac_config_t Struct Reference
=========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         The CSDIDAC configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

CSD_Type \* 

`base <structcy__stc__csdidac__config__t.html#aa729f9b1a91feb0cc616285bdcf3a0ed>`__

 

| The pointer to the CSD HW Block.
  `More... <#aa729f9b1a91feb0cc616285bdcf3a0ed>`__

 

cy_stc_csd_context_t \* 

`csdCxtPtr <structcy__stc__csdidac__config__t.html#aca952be1774cfb9764d60e156ce073ac>`__

 

| The pointer to the context of the CSD driver.
  `More... <#aca952be1774cfb9764d60e156ce073ac>`__

 

`cy_en_csdidac_channel_config_t <group__group__csdidac__enums.html#ga5042fa555ce33ff612236511a5397f13>`__ 

`configA <structcy__stc__csdidac__config__t.html#a964cd16c1873e15bb4b57e4ad26bbd36>`__

 

| The IDAC A channel configuration.
  `More... <#a964cd16c1873e15bb4b57e4ad26bbd36>`__

 

`cy_en_csdidac_channel_config_t <group__group__csdidac__enums.html#ga5042fa555ce33ff612236511a5397f13>`__ 

`configB <structcy__stc__csdidac__config__t.html#acf742f173a42c4b904bf4e566cee520e>`__

 

| The IDAC B channel configuration.
  `More... <#acf742f173a42c4b904bf4e566cee520e>`__

 

const `cy_stc_csdidac_pin_t <structcy__stc__csdidac__pin__t.html>`__ \* 

`ptrPinA <structcy__stc__csdidac__config__t.html#a2fe45dd3a2b9871a4cbfc3072eaf10fc>`__

 

| The pointer to the IDAC A pin structure.
  `More... <#a2fe45dd3a2b9871a4cbfc3072eaf10fc>`__

 

const `cy_stc_csdidac_pin_t <structcy__stc__csdidac__pin__t.html>`__ \* 

`ptrPinB <structcy__stc__csdidac__config__t.html#af7e641e0f444461fd40ea41f7401ad8f>`__

 

| The pointer to the IDAC B pin structure.
  `More... <#af7e641e0f444461fd40ea41f7401ad8f>`__

 

uint32_t 

`cpuClk <structcy__stc__csdidac__config__t.html#a863024356c94b372958257d2bc4dd261>`__

 

| CPU Clock in Hz. `More... <#a863024356c94b372958257d2bc4dd261>`__

 

uint8_t 

`csdInitTime <structcy__stc__csdidac__config__t.html#a2935c4fd44bb10a96e63dd8aeb57679f>`__

 

| The CSD HW Block initialization time.
  `More... <#a2935c4fd44bb10a96e63dd8aeb57679f>`__

 

**Field Documentation**

`◆  <#aa729f9b1a91feb0cc616285bdcf3a0ed>`__\ base
=================================================

.. container:: memitem

   .. container:: memproto

      +------------------------------------------+
      | CSD_Type\* cy_stc_csdidac_config_t::base |
      +------------------------------------------+

   .. container:: memdoc

      The pointer to the CSD HW Block.

`◆  <#aca952be1774cfb9764d60e156ce073ac>`__\ csdCxtPtr
======================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------------------------+
      | cy_stc_csd_context_t\* cy_stc_csdidac_config_t::csdCxtPtr |
      +-----------------------------------------------------------+

   .. container:: memdoc

      The pointer to the context of the CSD driver.

`◆  <#a964cd16c1873e15bb4b57e4ad26bbd36>`__\ configA
====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_channel_config_t <grou                                |
      | p__group__csdidac__enums.html#ga5042fa555ce33ff612236511a5397f13>`__ |
      | cy_stc_csdidac_config_t::configA                                     |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The IDAC A channel configuration.

`◆  <#acf742f173a42c4b904bf4e566cee520e>`__\ configB
====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_channel_config_t <grou                                |
      | p__group__csdidac__enums.html#ga5042fa555ce33ff612236511a5397f13>`__ |
      | cy_stc_csdidac_config_t::configB                                     |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The IDAC B channel configuration.

`◆  <#a2fe45dd3a2b9871a4cbfc3072eaf10fc>`__\ ptrPinA
====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | const                                                                |
      | `cy_stc_csdidac_pin_t <structcy__stc__csdidac__pin__t.html>`__\ \*   |
      | cy_stc_csdidac_config_t::ptrPinA                                     |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The pointer to the IDAC A pin structure.

`◆  <#af7e641e0f444461fd40ea41f7401ad8f>`__\ ptrPinB
====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | const                                                                |
      | `cy_stc_csdidac_pin_t <structcy__stc__csdidac__pin__t.html>`__\ \*   |
      | cy_stc_csdidac_config_t::ptrPinB                                     |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The pointer to the IDAC B pin structure.

`◆  <#a863024356c94b372958257d2bc4dd261>`__\ cpuClk
===================================================

.. container:: memitem

   .. container:: memproto

      +------------------------------------------+
      | uint32_t cy_stc_csdidac_config_t::cpuClk |
      +------------------------------------------+

   .. container:: memdoc

      CPU Clock in Hz.

`◆  <#a2935c4fd44bb10a96e63dd8aeb57679f>`__\ csdInitTime
========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------+
      | uint8_t cy_stc_csdidac_config_t::csdInitTime |
      +----------------------------------------------+

   .. container:: memdoc

      The CSD HW Block initialization time.


