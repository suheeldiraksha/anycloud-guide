==================================================================================
cy_stc_csdidac_context_t Struct Reference
==================================================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         The CSDIDAC context structure, that contains the internal
         middleware data.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html>`__ 

`cfgCopy <structcy__stc__csdidac__context__t.html#a681f270f640cb595fe8c769b4228f3e4>`__

 

| A configuration structure copy.
  `More... <#a681f270f640cb595fe8c769b4228f3e4>`__

 

`cy_en_csdidac_polarity_t <group__group__csdidac__enums.html#ga3f1fec1e58163c038446d307985ae8a3>`__ 

`polarityA <structcy__stc__csdidac__context__t.html#aca8105dd6dfecbf98fbbadc5f905d7df>`__

 

| The current IdacA polarity.
  `More... <#aca8105dd6dfecbf98fbbadc5f905d7df>`__

 

`cy_en_csdidac_lsb_t <group__group__csdidac__enums.html#ga6a918f88641c524977f457bcfed6743e>`__ 

`lsbA <structcy__stc__csdidac__context__t.html#af6814c1ee31c845fcd7dfb479a3a2b67>`__

 

| The current IdacA LSB.
  `More... <#af6814c1ee31c845fcd7dfb479a3a2b67>`__

 

uint8_t 

`codeA <structcy__stc__csdidac__context__t.html#a44d7df7bf4b20222be95b71fcca06e61>`__

 

| The current IdacA code.
  `More... <#a44d7df7bf4b20222be95b71fcca06e61>`__

 

`cy_en_csdidac_state_t <group__group__csdidac__enums.html#ga44dd9ae77f74c643d5761e9571870d06>`__ 

`channelStateA <structcy__stc__csdidac__context__t.html#a409e4f885b160c9de4049dfcfebaedec>`__

 

| The IDAC channel A is enabled.
  `More... <#a409e4f885b160c9de4049dfcfebaedec>`__

 

`cy_en_csdidac_polarity_t <group__group__csdidac__enums.html#ga3f1fec1e58163c038446d307985ae8a3>`__ 

`polarityB <structcy__stc__csdidac__context__t.html#ae282aa393ec14c56e3b56c1725de2dfa>`__

 

| The current IdacB polarity.
  `More... <#ae282aa393ec14c56e3b56c1725de2dfa>`__

 

`cy_en_csdidac_lsb_t <group__group__csdidac__enums.html#ga6a918f88641c524977f457bcfed6743e>`__ 

`lsbB <structcy__stc__csdidac__context__t.html#a06c8e469802ccbd8ec9325e730be5706>`__

 

| The current IdacB LSB.
  `More... <#a06c8e469802ccbd8ec9325e730be5706>`__

 

uint8_t 

`codeB <structcy__stc__csdidac__context__t.html#a06edde8424810a1d98b850b3fc41282e>`__

 

| The current IdacB code.
  `More... <#a06edde8424810a1d98b850b3fc41282e>`__

 

`cy_en_csdidac_state_t <group__group__csdidac__enums.html#ga44dd9ae77f74c643d5761e9571870d06>`__ 

`channelStateB <structcy__stc__csdidac__context__t.html#a8f1c29f8f75835887928d206f14fcfe0>`__

 

| The IDAC channel B is enabled.
  `More... <#a8f1c29f8f75835887928d206f14fcfe0>`__

 

Field Documentation
===================

`◆  <#a681f270f640cb595fe8c769b4228f3e4>`__\ cfgCopy
====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_stc_csdidac_config_t <structcy__stc__csdidac__config__t.html>`__ |
      | cy_stc_csdidac_context_t::cfgCopy                                    |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      A configuration structure copy.

`◆  <#aca8105dd6dfecbf98fbbadc5f905d7df>`__\ polarityA
======================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_polarity_t <grou                                      |
      | p__group__csdidac__enums.html#ga3f1fec1e58163c038446d307985ae8a3>`__ |
      | cy_stc_csdidac_context_t::polarityA                                  |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The current IdacA polarity.

`◆  <#af6814c1ee31c845fcd7dfb479a3a2b67>`__\ lsbA
=================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_lsb_t <grou                                           |
      | p__group__csdidac__enums.html#ga6a918f88641c524977f457bcfed6743e>`__ |
      | cy_stc_csdidac_context_t::lsbA                                       |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The current IdacA LSB.

`◆  <#a44d7df7bf4b20222be95b71fcca06e61>`__\ codeA
==================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------+
      | uint8_t cy_stc_csdidac_context_t::codeA |
      +-----------------------------------------+

   .. container:: memdoc

      The current IdacA code.

`◆  <#a409e4f885b160c9de4049dfcfebaedec>`__\ channelStateA
==========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_state_t <grou                                         |
      | p__group__csdidac__enums.html#ga44dd9ae77f74c643d5761e9571870d06>`__ |
      | cy_stc_csdidac_context_t::channelStateA                              |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The IDAC channel A is enabled.

`◆  <#ae282aa393ec14c56e3b56c1725de2dfa>`__\ polarityB
======================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_polarity_t <grou                                      |
      | p__group__csdidac__enums.html#ga3f1fec1e58163c038446d307985ae8a3>`__ |
      | cy_stc_csdidac_context_t::polarityB                                  |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The current IdacB polarity.

`◆  <#a06c8e469802ccbd8ec9325e730be5706>`__\ lsbB
=================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_lsb_t <grou                                           |
      | p__group__csdidac__enums.html#ga6a918f88641c524977f457bcfed6743e>`__ |
      | cy_stc_csdidac_context_t::lsbB                                       |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The current IdacB LSB.

`◆  <#a06edde8424810a1d98b850b3fc41282e>`__\ codeB
==================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------+
      | uint8_t cy_stc_csdidac_context_t::codeB |
      +-----------------------------------------+

   .. container:: memdoc

      The current IdacB code.

`◆  <#a8f1c29f8f75835887928d206f14fcfe0>`__\ channelStateB
==========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_en_csdidac_state_t <grou                                         |
      | p__group__csdidac__enums.html#ga44dd9ae77f74c643d5761e9571870d06>`__ |
      | cy_stc_csdidac_context_t::channelStateB                              |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      The IDAC channel B is enabled.


