=================
Enumerated types
=================

.. doxygengroup:: group_csdidac_enums
   :project: csdidac
   :members:
   :protected-members:
   :private-members:
   :undoc-members: