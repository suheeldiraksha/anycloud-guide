======================================
cy_stc_csdidac_pin_t Struct Reference
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         The CSDIDAC pin structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

GPIO_PRT_Type \* 

`ioPcPtr <structcy__stc__csdidac__pin__t.html#a8b26d10faee8d2524aa368f4f1e9c6ff>`__

 

| The pointer to the channel IO PC register.
  `More... <#a8b26d10faee8d2524aa368f4f1e9c6ff>`__

 

uint8_t 

`pin <structcy__stc__csdidac__pin__t.html#a7265353914e4d586aca722a613cdde3f>`__

 

| The channel IO pin. `More... <#a7265353914e4d586aca722a613cdde3f>`__

 

**Field Documentation**

`◆  <#a8b26d10faee8d2524aa368f4f1e9c6ff>`__\ ioPcPtr
====================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------------+
      | GPIO_PRT_Type\* cy_stc_csdidac_pin_t::ioPcPtr |
      +-----------------------------------------------+

   .. container:: memdoc

      The pointer to the channel IO PC register.

`◆  <#a7265353914e4d586aca722a613cdde3f>`__\ pin
================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------+
      | uint8_t cy_stc_csdidac_pin_t::pin |
      +-----------------------------------+

   .. container:: memdoc

      The channel IO pin.


