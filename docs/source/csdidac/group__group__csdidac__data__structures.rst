================
Data Structures
================
.. doxygengroup:: group_csdidac_data_structures
   :project: csdidac
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
.. toctree::

   structcy__stc__csdidac__pin__t.rst
   structcy__stc__csdidac__config__t.rst
   structcy__stc__csdidac__context__t.rst

