==========
Functions
==========

.. doxygengroup:: group_csdidac_functions
   :project: csdidac
   :members:
   :protected-members:
   :private-members:
   :undoc-members: