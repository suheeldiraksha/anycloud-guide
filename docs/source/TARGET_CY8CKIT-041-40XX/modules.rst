==================
BSP API Reference
==================

.. toctree::
   :hidden:

   group__group__bsp__settings.rst
   group__group__bsp__pin__state.rst
   group__group__bsp__pins.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         The following provides a list of BSP API documentation

      .. container:: directory

         .. container:: levels

            [detail level 12]

          \ `BSP Settings <group__group__bsp__settings.html>`__

.. container:: category

   Peripheral Default HAL Settings:

Resource
Parameter
Value
Remarks
UART
Flow control
No flow control
Data format
8N1
Baud rate
115200
 \ `Pin States <group__group__bsp__pin__state.html>`__
 ▼\ `Pin Mappings <group__group__bsp__pins.html>`__
 \ `LED Pins <group__group__bsp__pins__led.html>`__
 \ `Button Pins <group__group__bsp__pins__btn.html>`__
 \ `Communication Pins <group__group__bsp__pins__comm.html>`__
 \ `Arduino Header Pins <group__group__bsp__pins__arduino.html>`__
 \ `J2 Header Pins <group__group__bsp__pins__j2.html>`__
 \ `Macros <group__group__bsp__macros.html>`__
 \ `Functions <group__group__bsp__functions.html>`__

