===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_CY8CKIT-041-40XX
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
