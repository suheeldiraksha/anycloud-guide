==================
BSP API Reference
==================

.. toctree::
   :hidden:

   group__group__bsp__settings.rst
   group__group__bsp__pin__state.rst
   group__group__bsp__pins__comm.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__led.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         The following provides a list of BSP API documentation

      .. container:: directory

          \ `BSP Settings <group__group__bsp__settings.html>`__

.. container:: category

   Peripheral Default HAL Settings:

Resource
Parameter
Value
Remarks
ADC
VREF
1.2 V
Measurement type
Single Ended
Input voltage range
0 to 2.4 V (0 to 2*VREF)
Output range
0x000 to 0x7FF
DAC
Reference source
VDDA
Input range
0x000 to 0xFFF
Output range
0 to VDDA
Output type
Unbuffered output
I2C
Role
Master
Configurable to slave mode through HAL function
Data rate
100 kbps
Configurable through HAL function
Drive mode of SCL & SDA pins
Open Drain (drives low)
External pull-up resistors are required
LpTimer
Uses WCO (32.768 kHz) as clock source & MCWDT as counter; 1 count =
1/32768 second or 32768 counts = 1 second
SPI
Data rate
100 kpbs
Configurable through HAL function
Slave select polarity
Active low
UART
Flow control
No flow control
Configurable through HAL function
Data format
8N1
Configurable through HAL function
Baud rate
115200
Configurable through HAL function


 \ `Pin States <group__group__bsp__pin__state.html>`__

 \ `Communication Pins <group__group__bsp__pins__comm.html>`__

 \ `Button Pins <group__group__bsp__pins__btn.html>`__

 \ `LED Pins <group__group__bsp__pins__led.html>`__

 \ `Macros <group__group__bsp__macros.html>`__

 \ `Functions <group__group__bsp__functions.html>`__



