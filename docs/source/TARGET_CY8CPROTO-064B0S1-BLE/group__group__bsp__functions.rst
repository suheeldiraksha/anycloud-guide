==========
Functions
==========

.. doxygengroup:: group_bsp_functions
   :project: TARGET_CY8CPROTO-064B0S1-BLE
   :members:
   :protected-members:
   :private-members:
   :undoc-members: