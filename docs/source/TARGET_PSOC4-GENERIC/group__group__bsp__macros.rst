=======
Macros
=======

.. doxygengroup:: group_bsp_macros
   :project: TARGET_PSOC4-GENERIC
   :members:
   :protected-members:
   :private-members:
