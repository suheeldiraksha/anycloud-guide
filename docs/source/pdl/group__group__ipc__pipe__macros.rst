=======
Macros
=======

.. doxygengroup:: group_ipc_pipe_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: