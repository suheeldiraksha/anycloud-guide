=====================================
CAN FD (CAN with Flexible Data-Rate)
=====================================

.. toctree::
   :hidden:
   
   group__group__canfd__macros.rst
   group__group__canfd__functions.rst
   group__group__canfd__data__structures.rst
   group__group__canfd__enums.rst

.. doxygengroup:: group_canfd
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
   


