===============================
PPU Fixed (FIXED) v2 Functions
===============================


.. doxygengroup:: group_prot_functions_ppu_fixed_v2
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: