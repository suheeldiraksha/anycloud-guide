=====================
Shift Register Status
=====================

.. doxygengroup:: group_tcpwm_shiftreg_status
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   