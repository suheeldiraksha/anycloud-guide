=======
Macros
=======

.. doxygengroup:: group_efuse_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: