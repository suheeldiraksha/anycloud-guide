=======================
SIO input buffer mode
=======================

.. doxygengroup:: group_gpio_sioIbuf
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: