=================================
Run-time Configuration Functions
=================================



.. doxygengroup:: group_sar_functions_config
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: