===============================
Range Interrupt Register Enums
===============================




.. doxygengroup:: group_sar_sample_thres_register_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: