==========
Functions
==========

.. doxygengroup:: group_sysclk_ext_funcs
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
