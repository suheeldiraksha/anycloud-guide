=========================
TCPWM (Timer Counter PWM)
=========================

.. doxygengroup:: group_tcpwm
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__tcpwm__common.rst
   group__group__tcpwm__counter.rst
   group__group__tcpwm__pwm.rst
   group__group__tcpwm__quaddec.rst
   group__group__tcpwm__shiftreg.rst
   


