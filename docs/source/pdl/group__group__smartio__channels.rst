============================
Smart I/O channel selection
============================

.. doxygengroup:: group_smartio_channels
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: