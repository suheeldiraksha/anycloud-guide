=======================================
Display/Character Management Functions
=======================================

.. doxygengroup:: group_seglcd_functions_display
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
