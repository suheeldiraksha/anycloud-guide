==========
Functions
==========

.. doxygengroup:: group_sysclk_clk_timer_funcs
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: