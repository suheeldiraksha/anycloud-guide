====================================
Shift Register CLK Prescaler values
====================================

.. doxygengroup:: group_tcpwm_shiftreg_clk_prescalers
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   