==============================
LPComp (Low Power Comparator)
==============================

.. doxygengroup:: group_lpcomp
   :project: pdl
   
.. toctree::
   
   group__group__lpcomp__macros.rst
   group__group__lpcomp__functions.rst
   group__group__lpcomp__data__structures.rst
   group__group__lpcomp__enums.rst
   

