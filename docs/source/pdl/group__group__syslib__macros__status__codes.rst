============
Status codes
============

.. doxygengroup:: group_syslib_macros_status_codes
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: