==========================
Profile (Energy Profiler)
==========================

.. doxygengroup:: group_energy_profiler
   :project: pdl
   

.. toctree::
   
   group__group__profile__macros.rst
   group__group__profile__functions.rst
   group__group__profile__data__structures.rst
   group__group__profile__enums.rst
   
   
   
