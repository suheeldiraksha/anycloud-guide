WDT (Watchdog Timer)
=====================

.. toctree::
   
   group__group__wdt__macros.rst
   group__group__wdt__functions.rst

.. doxygengroup:: group_wdt
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
