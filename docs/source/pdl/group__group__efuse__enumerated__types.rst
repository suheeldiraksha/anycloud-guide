=================
Enumerated Types
=================


.. doxygengroup:: group_efuse_enumerated_types
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: