========================
Common Enumerated Types
========================


.. doxygengroup:: group_crypto_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: