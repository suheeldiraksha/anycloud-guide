==========
Functions
==========

.. doxygengroup:: group_systick_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: