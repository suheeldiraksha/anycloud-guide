================
Transfer Errors
================

.. doxygengroup:: group_usbfs_dev_drv_macros_ep_xfer_err
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: