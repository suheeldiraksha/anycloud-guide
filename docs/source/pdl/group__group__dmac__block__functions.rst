================
Block Functions
================


.. doxygengroup:: group_dmac_block_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: