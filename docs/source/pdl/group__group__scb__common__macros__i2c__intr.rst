=======================
I2C Interrupt Statuses
=======================


.. doxygengroup:: group_scb_common_macros_i2c_intr
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: