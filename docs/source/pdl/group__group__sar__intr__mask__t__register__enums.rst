==============================
Interrupt Mask Register Enums
==============================



.. doxygengroup:: group_sar_mask_t_register_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: