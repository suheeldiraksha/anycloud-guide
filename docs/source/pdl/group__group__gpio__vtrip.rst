==================
Voltage trip mode
==================

.. doxygengroup:: group_gpio_vtrip
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: