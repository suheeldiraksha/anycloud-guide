=====================
Descriptor Functions
=====================


.. doxygengroup:: group_dmac_descriptor_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: