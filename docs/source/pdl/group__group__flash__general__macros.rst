========================
Flash general parameters
========================

.. doxygengroup:: group_flash_general_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: