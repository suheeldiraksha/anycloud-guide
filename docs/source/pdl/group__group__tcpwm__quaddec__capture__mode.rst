====================
QuadDec CaptureMode
====================

.. doxygengroup:: group_tcpwm_quaddec_capture_mode
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: