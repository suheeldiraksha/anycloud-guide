==================================================
Enumerated Types
==================================================

.. doxygengroup:: group_ble_clk_data_type
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: