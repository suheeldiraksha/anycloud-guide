===============================
SysPm (System Power Management)
===============================

.. doxygengroup:: group_syspm
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__syspm__macros.rst
   group__group__syspm__functions.rst
   group__group__syspm__data__structures.rst
   group__group__syspm__data__enumerates.rst
   


