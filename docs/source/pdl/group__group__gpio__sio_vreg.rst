=======================
SIO output buffer mode
=======================

.. doxygengroup:: group_gpio_sioVreg
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: