====================
Low Power Callbacks
====================

.. doxygengroup:: group_syspm_functions_callback
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: