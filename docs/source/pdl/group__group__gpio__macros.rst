======
Macros
======

.. doxygengroup:: group_gpio_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
^^^^^^^^^^^^^^
.. toctree::

   group__group__gpio__drive_modes.rst
   group__group__gpio__vtrip.rst
   group__group__gpio__slew_rate.rst
   group__group__gpio__drive_strength.rst
   group__group__gpio__interrupt_trigger.rst
   group__group__gpio__sio_vreg.rst
   group__group__gpio__sio_ibuf.rst
   group__group__gpio__sio_vtrip.rst
   group__group__gpio__sio_vref.rst
   group__group__gpio__sio_voh.rst
   