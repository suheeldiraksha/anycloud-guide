====================
WiFi Initialization
====================

.. doxygengroup:: group_bsp_wifi
   :project: whd-bsp-integration
   :members:
   :protected-members:
   :private-members:
   :undoc-members: