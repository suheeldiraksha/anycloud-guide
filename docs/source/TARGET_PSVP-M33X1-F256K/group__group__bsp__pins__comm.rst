===================
Communication Pins
===================

.. doxygengroup:: group_bsp_pins_comm
   :project: TARGET_PSVP-M33X1-F256K
   :members:
   :protected-members:
   :private-members:
   :undoc-members: