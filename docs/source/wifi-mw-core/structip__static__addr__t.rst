===============================================================
Cypress WiFi middleware core: ip_static_addr_t Struct Reference
===============================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress WiFi middleware core   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         This structure represents a static IP address.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

ip_addr_t 
`addr <structip__static__addr__t.html#ad33a979bc7e43e1f4269508ee1e13503>`__
 
The IP address for the network interface.
 
ip_addr_t 
`netmask <structip__static__addr__t.html#a70b8e7d6db07889fbb92db8f562de2cb>`__
 
The netmask for the network interface.
 
ip_addr_t 
`gateway <structip__static__addr__t.html#ad135b4541779fed17ef74e30b24d0604>`__
 
The default gateway for network traffic.
 

.. container:: navpath
   :name: nav-path

   -  Generated for **Cypress WiFi middleware core** by **Cypress
      Semiconductor Corporation**. All rights reserved.

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
