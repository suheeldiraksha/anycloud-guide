==============
API Reference
==============

.. raw:: html

   <hr>

.. toctree::
   :hidden:

   group__group__lwip__whd__port.rst
   group__generic__lwip__whd__port__defines.rst
   

The following provides a list of API documentation

+----------------------------------------------------------+----------------------------------+
| \ `lwIP and WHD port                                     | Functions for dealing with       |
| <group_                                                  | linking the lwIP TCP/IP stack    |
| _group__lwip__whd__port.html>`__                         | with WiFi Host Driver            |
+----------------------------------------------------------+----------------------------------+
| \ `Functions <group__group__lwip                         |                                  |
| __whd__port__functions.html>`__                          |                                  |
+----------------------------------------------------------+----------------------------------+
| \ `Structures <group__group__lwip                        |                                  |
| __whd__port__structures.html>`__                         |                                  |
+----------------------------------------------------------+----------------------------------+
| \ `CY generic lwIP WHD glue                              | Cypress middleware APIs return   |
| results/error codes                                      | results of type cy_rslt_t and    |
| <group__generic__lwip                                    | comprise of three parts:         |
| __whd__port__defines.html>`__                            |                                  |
+----------------------------------------------------------+----------------------------------+

