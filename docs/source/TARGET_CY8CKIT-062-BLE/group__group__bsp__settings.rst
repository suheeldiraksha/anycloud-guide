=============
BSP Settings
=============

.. doxygengroup:: group_bsp_settings
   :project: TARGET_CY8CKIT-062-BLE
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
