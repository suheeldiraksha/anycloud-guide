===================
OTA Agent Callback
===================


.. doxygengroup:: group_ota_callback
   :project: anycloud-ota
   :members:
   :protected-members:
   :private-members:
   :undoc-members: