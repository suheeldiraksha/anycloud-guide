======================================
cy_ota_mqtt_params_s Struct Reference
======================================

.. doxygenstruct:: cy_ota_mqtt_params_s
   :project: anycloud-ota
   :members:
   :protected-members:
   :private-members:
   :undoc-members: