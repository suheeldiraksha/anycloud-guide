======================================
cy_ota_server_info_s Struct Reference
======================================

.. doxygenstruct:: cy_ota_server_info_t
   :project: anycloud-ota
   :members:
   :protected-members:
   :private-members:
   :undoc-members: