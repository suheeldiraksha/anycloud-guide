=============
OTA Typedefs
=============




.. doxygengroup:: group_ota_typedefs
   :project: anycloud-ota
   :members:
   :protected-members:
   :private-members:
   :undoc-members: