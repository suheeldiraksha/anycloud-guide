==================
BSP API Reference
==================

.. toctree::
   :hidden:

   group__group__bsp__settings.rst
   group__group__bsp__pin__state.rst
   group__group__bsp__pins.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst
   

The following provides a list of BSP API documentation

+-----------------------------------+-----------------------------------+
| \ `BSP                            | .. container:: category           |
| Settings <gro                     |                                   |
| up__group__bsp__settings.html>`__ |    Clock Settings:                |
+-----------------------------------+-----------------------------------+
| \ `Pin                            |                                   |
| States <group                     |                                   |
| __group__bsp__pin__state.html>`__ |                                   |
+-----------------------------------+-----------------------------------+
| \ `Pin                            |                                   |
| Mappings                          |                                   |
| <group__group__bsp__pins.html>`__ |                                   |
+-----------------------------------+-----------------------------------+
| \ `LED                            |                                   |
| Pins <grou                        |                                   |
| p__group__bsp__pins__led.html>`__ |                                   |
+-----------------------------------+-----------------------------------+
| \ `Button                         |                                   |
| Pins <group__                     |                                   |
| group__bsp__pins__btn.html>`__    |                                   |
+-----------------------------------+-----------------------------------+
| \ `Communication                  |                                   |
| Pins <group                       |                                   |
| __group__bsp__pins__comm.html>`__ |                                   |
+-----------------------------------+-----------------------------------+
| \ `Macros <g                      |                                   |
| roup__group__bsp__macros.html>`_  |                                   |
+-----------------------------------+-----------------------------------+
| \ `Functions <grou                |                                   |
| p__group__bsp__functions.html>`__ |                                   |
+-----------------------------------+-----------------------------------+

