=====
Pins
=====

.. toctree::

   group__group__board__libs__microphone.rst
   group__group__board__libs__codec.rst
   group__group__board__libs__light.rst
   group__group__board__libs__motion.rst
   group__group__board__libs__display.rst

.. doxygengroup:: group_board_libs_pins
   :project: CY8CKIT-028-TFT