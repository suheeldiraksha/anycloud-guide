==============
API Reference
==============

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Structures <#nested-classes>`__ \|
         `Functions <#func-members>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: General Description
         :name: general-description
         :class: groupheader

      APIs for controlling the TFT display on the board.

      .. rubric::  Data Structures
         :name: data-structures
         :class: groupheader

struct  

`mtb_st7789v_pins_t <group__group__board__libs.html#structmtb__st7789v__pins__t>`__

 

| Configuration structure defining the pins used to communicate with the
  TFT display.
  `More... <group__group__board__libs.html#structmtb__st7789v__pins__t>`__

 

Functions
=========

cy_rslt_t 

`mtb_st7789v_init8 <group__group__board__libs.html#ga0c2d54e3132c0ea6e84e1aff7d2d7ee0>`__
(const
`mtb_st7789v_pins_t <group__group__board__libs.html#structmtb__st7789v__pins__t>`__
\*pins)

 

| Initializes GPIOs for the software i8080 8-bit interface.
  `More... <#ga0c2d54e3132c0ea6e84e1aff7d2d7ee0>`__

 

void 

`mtb_st7789v_write_reset_pin <group__group__board__libs.html#gae69a2958a7e3d9542ef4cd4ecc544294>`__
(bool value)

 

| Sets value of the display Reset pin.
  `More... <#gae69a2958a7e3d9542ef4cd4ecc544294>`__

 

void 

`mtb_st7789v_write_command <group__group__board__libs.html#gab92d0972d5d6ebf672ae064531e6beeb>`__
(uint8_t command)

 

| Writes one byte of data to the software i8080 interface with the
  LCD_DC pin set to 0. `More... <#gab92d0972d5d6ebf672ae064531e6beeb>`__

 

void 

`mtb_st7789v_write_data <group__group__board__libs.html#gaaf544b50c24b80d7d2610622ca7f7389>`__
(uint8_t data)

 

| Writes one byte of data to the software i8080 interface with the
  LCD_DC pin set to 1. `More... <#gaaf544b50c24b80d7d2610622ca7f7389>`__

 

void 

`mtb_st7789v_write_command_stream <group__group__board__libs.html#ga7124cb38e0869a4efaac86885f82a2d1>`__
(uint8_t \*data, int num)

 

| Writes multiple command bytes to the software i8080 interface with the
  LCD_DC pin set to 0. `More... <#ga7124cb38e0869a4efaac86885f82a2d1>`__

 

void 

`mtb_st7789v_write_data_stream <group__group__board__libs.html#ga6ae0950f835844e40955849088884b6c>`__
(uint8_t \*data, int num)

 

| Writes multiple bytes of data to the software i8080 interface with the
  LCD_DC pin set to 1. `More... <#ga6ae0950f835844e40955849088884b6c>`__

 

uint8_t 

`mtb_st7789v_read_data <group__group__board__libs.html#ga37af8e22c908fab44d0b2bcfa5f055ed>`__
(void)

 

| Reads one byte of data from the software i8080 interface with the
  LCD_DC pin set to 1. `More... <#ga37af8e22c908fab44d0b2bcfa5f055ed>`__

 

void 

`mtb_st7789v_read_data_stream <group__group__board__libs.html#ga411834efd45fc41eb41cdb82d1cc5d23>`__
(uint8_t \*data, int num)

 

| Reads multiple bytes of data from the software i8080 interface with
  the LCD_DC pin set to 1.
  `More... <#ga411834efd45fc41eb41cdb82d1cc5d23>`__

 

void 

`mtb_st7789v_free <group__group__board__libs.html#ga14844b3396bcc635e10d09f11aa147fc>`__
(void)

 

| Free all resources used for the software i8080 interface.

 


Data Structure Documentation
============================

`◆  <#structmtb__st7789v__pins__t>`__\ mtb_st7789v_pins_t
=========================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------+
      | struct mtb_st7789v_pins_t |
      +---------------------------+

   .. container:: memdoc

      Data Fields

cyhal_gpio_t

db08

Pin for the Display Data8 signal.

cyhal_gpio_t

db09

Pin for the Display Data9 signal.

cyhal_gpio_t

db10

Pin for the Display Data10 signal.

cyhal_gpio_t

db11

Pin for the Display Data11 signal.

cyhal_gpio_t

db12

Pin for the Display Data12 signal.

cyhal_gpio_t

db13

Pin for the Display Data13 signal.

cyhal_gpio_t

db14

Pin for the Display Data14 signal.

cyhal_gpio_t

db15

Pin for the Display Data15 signal.

cyhal_gpio_t

nrd

cyhal_gpio_t

nwr

Pin for the Display Read signal.

cyhal_gpio_t

dc

Pin for the Display Write signal.

cyhal_gpio_t

rst

Pin for the Display D/C signal.

Function Documentation
======================

`◆  <#ga0c2d54e3132c0ea6e84e1aff7d2d7ee0>`__\ mtb_st7789v_init8()
=================================================================

.. container:: memitem

   .. container:: memproto

      +---------------------+---+---------------------+--------+---+---+
      | cy_rslt_t           | ( | const               | *pins* | ) |   |
      | mtb_st7789v_init8   |   | `mtb_st             |        |   |   |
      |                     |   | 7789v_pins_t <group |        |   |   |
      |                     |   | __group__board__lib |        |   |   |
      |                     |   | s.html#structmtb__s |        |   |   |
      |                     |   | t7789v__pins__t>`__ |        |   |   |
      |                     |   | \*                  |        |   |   |
      +---------------------+---+---------------------+--------+---+---+

   .. container:: memdoc

      Initializes GPIOs for the software i8080 8-bit interface.

      Parameters
         ==== ==== ===================================================
         [in] pins Structure providing the pins to use for the display
         ==== ==== ===================================================

      Returns
         CY_RSLT_SUCCESS if successfully initialized, else an error
         about what went wrong

`◆  <#gae69a2958a7e3d9542ef4cd4ecc544294>`__\ mtb_st7789v_write_reset_pin()
===========================================================================

.. container:: memitem

   .. container:: memproto

      ================================ = ===== ======= =
      void mtb_st7789v_write_reset_pin ( bool  *value* ) 
      ================================ = ===== ======= =

   .. container:: memdoc

      Sets value of the display Reset pin.

      Parameters
         ==== ===== ===========================
         [in] value The value to set on the pin
         ==== ===== ===========================

`◆  <#gab92d0972d5d6ebf672ae064531e6beeb>`__\ mtb_st7789v_write_command()
=========================================================================

.. container:: memitem

   .. container:: memproto

      ============================== = ======== ========= =
      void mtb_st7789v_write_command ( uint8_t  *command* ) 
      ============================== = ======== ========= =

   .. container:: memdoc

      Writes one byte of data to the software i8080 interface with the
      LCD_DC pin set to 0.

      Followed by a low pulse on the NWR line to complete the write.

      Parameters
         ==== ======= ===================================
         [in] command The command to issue to the display
         ==== ======= ===================================

`◆  <#gaaf544b50c24b80d7d2610622ca7f7389>`__\ mtb_st7789v_write_data()
======================================================================

.. container:: memitem

   .. container:: memproto

      =========================== = ======== ====== =
      void mtb_st7789v_write_data ( uint8_t  *data* ) 
      =========================== = ======== ====== =

   .. container:: memdoc

      Writes one byte of data to the software i8080 interface with the
      LCD_DC pin set to 1.

      Followed by a low pulse on the NWR line to complete the write.

      Parameters
         ==== ==== =================================
         [in] data The value to issue to the display
         ==== ==== =================================

`◆  <#ga7124cb38e0869a4efaac86885f82a2d1>`__\ mtb_st7789v_write_command_stream()
================================================================================

.. container:: memitem

   .. container:: memproto

      ===================================== = =========== =======
      void mtb_st7789v_write_command_stream ( uint8_t \*  *data*,
      \                                       int         *num* 
      \                                     )             
      ===================================== = =========== =======

   .. container:: memdoc

      Writes multiple command bytes to the software i8080 interface with
      the LCD_DC pin set to 0.

      Parameters
         +------+------+-----------------------------------------------------------------+
         | [in] | data | Pointer to the commands to send to the display                  |
         +------+------+-----------------------------------------------------------------+
         | [in] | num  | The number of commands in the data array to send to the display |
         +------+------+-----------------------------------------------------------------+

`◆  <#ga6ae0950f835844e40955849088884b6c>`__\ mtb_st7789v_write_data_stream()
=============================================================================

.. container:: memitem

   .. container:: memproto

      ================================== = =========== =======
      void mtb_st7789v_write_data_stream ( uint8_t \*  *data*,
      \                                    int         *num* 
      \                                  )             
      ================================== = =========== =======

   .. container:: memdoc

      Writes multiple bytes of data to the software i8080 interface with
      the LCD_DC pin set to 1.

      Parameters
         ==== ==== ============================================================
         [in] data Pointer to the data to send to the display
         [in] num  The number of bytes in the data array to send to the display
         ==== ==== ============================================================

`◆  <#ga37af8e22c908fab44d0b2bcfa5f055ed>`__\ mtb_st7789v_read_data()
=====================================================================

.. container:: memitem

   .. container:: memproto

      ============================= = ===== =
      uint8_t mtb_st7789v_read_data ( void   ) 
      ============================= = ===== =

   .. container:: memdoc

      Reads one byte of data from the software i8080 interface with the
      LCD_DC pin set to 1.

      Returns
         The byte read from the display

`◆  <#ga411834efd45fc41eb41cdb82d1cc5d23>`__\ mtb_st7789v_read_data_stream()
============================================================================

.. container:: memitem

   .. container:: memproto

      ================================= = =========== =======
      void mtb_st7789v_read_data_stream ( uint8_t \*  *data*,
      \                                   int         *num* 
      \                                 )             
      ================================= = =========== =======

   .. container:: memdoc

      Reads multiple bytes of data from the software i8080 interface
      with the LCD_DC pin set to 1.

      Parameters
         ======== ==== =========================================================
         [in,out] data Pointer to where to store the bytes read from the display
         [in]     num  The number of bytes to read from the display
         ======== ==== =========================================================

