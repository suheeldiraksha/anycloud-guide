====================
Platform Port Layer
====================



.. doxygengroup:: mqtt_cyport
   :project: mqtt
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   
General Description
--------------------
Cypress platform port layer functions and configurations.


API Reference
---------------
.. toctree::
   

   group__mqtt__cyport__function.rst
   group__mqtt__cyport__config.rst
 

 
