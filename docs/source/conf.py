

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'AnyCloud Guide'
copyright = '2020, Infineon'
author = 'Infineon'

# The full version, including alpha/beta/rc tags
release = 'rev0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['breathe','sphinx.ext.mathjax']

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

breathe_projects = {'anycloud-ota' : 'anycloud-ota/xml/', 'capsense' : 'capsense/xml/', 'csdadc' : 'csdadc/xml/', 'csdidac' : 'csdidac/xml/', 'dfu' : 'dfu/xml/','emeeprom' : 'emeeprom/xml/','lpa' : 'lpa/xml/','mqtt' : 'mqtt/xml/','secure-sockets' : 'secure-sockets/xml/', 'sensor-atmo-bme680' : 'sensor-atmo-bme680/xml/', 'sensor-motion-bmi160' : 'sensor-motion-bmi160/xml/',
'TARGET_CY8CPROTO-064S1-SB' : 'TARGET_CY8CPROTO-064S1-SB/xml/', 'TARGET_CYBLE-416045-EVAL' : 'TARGET_CYBLE-416045-EVAL/xml/',
 'TARGET_CYW9P62S1-43012EVB-01' : 'TARGET_CYW9P62S1-43012EVB-01/xml/', 'TARGET_CYW9P62S1-43438EVB-01' : 'TARGET_CYW9P62S1-43438EVB-01/xml/' , 
 'TARGET_PSOC4-GENERIC' : 'TARGET_PSOC4-GENERIC/xml/',
 'TARGET_PSOC6-GENERIC' : 'TARGET_PSOC6-GENERIC/xml/', 'TARGET_PSVP-M33X1-F256K' : 'TARGET_PSVP-M33X1-F256K/xml/' , 'TARGET_PSVP-Player28' : 'TARGET_PSVP-Player28/xml/' ,
 'TARGET_PSVP-PSOC4AS4' : 'TARGET_PSVP-PSOC4AS4/xml/', 'TARGET_TEST_CYSDIO_PORT_9' : 'TARGET_TEST_CYSDIO_PORT_9/xml/' , 'thermistor' : 'thermistor/xml/'  ,
 'udb-sdio-whd' : 'udb-sdio-whd/xml/', 'usbdev' : 'usbdev/xml/', 'whd-bsp-integration' : 'whd-bsp-integration/xml/', 
 'wifi-connection-manager' : 'wifi-connection-manager/xml/','serial-flash' : 'serial-flash/xml/', 'TARGET_CY8C4548AZI-S485' : 'TARGET_CY8C4548AZI-S485/xml/',
 'TARGET_CY8CKIT-041-40XX' : 'TARGET_CY8CKIT-041-40XX/xml/', 'TARGET_CY8CKIT-041-41XX' : 'TARGET_CY8CKIT-041-41XX/xml/', 'TARGET_CY8CKIT-062-BLE' : 'TARGET_CY8CKIT-062-BLE/xml/',
 'TARGET_CY8CKIT-062S2-43012' : 'TARGET_CY8CKIT-062S2-43012/xml/', 'TARGET_CY8CKIT-062S4' : 'TARGET_CY8CKIT-062S4/xml/', 
 'TARGET_CY8CKIT-062-WIFI-BT' : 'TARGET_CY8CKIT-062-WIFI-BT/xml/', 'TARGET_CY8CKIT-064B0S2-4343W' : 'TARGET_CY8CKIT-064B0S2-4343W/xml/', 
 'TARGET_CY8CKIT-064S0S2-4343W' : 'TARGET_CY8CKIT-064S0S2-4343W/xml/', 'TARGET_CY8CKIT-145-40XX' : 'TARGET_CY8CKIT-145-40XX/xml/', 'TARGET_CY8CKIT-149' : 'TARGET_CY8CKIT-149/xml/',
 'TARGET_CY8CPROTO-062-4343W' : 'TARGET_CY8CPROTO-062-4343W/xml/', 'TARGET_CY8CPROTO-062S3-4343W' : 'TARGET_CY8CPROTO-062S3-4343W/xml/', 
 'TARGET_CY8CPROTO-063-BLE' : 'TARGET_CY8CPROTO-063-BLE/xml/',
 'TARGET_CY8CPROTO-064B0S1-BLE' : 'TARGET_CY8CPROTO-064B0S1-BLE/xml/', 'TARGET_CY8CPROTO-064B0S3' : 'TARGET_CY8CPROTO-064B0S3/xml/', 'core-lib' : 'core-lib/xml/', 
 'pdl' : 'pdl/xml/','CY8CKIT-028-EPD' : 'CY8CKIT-028-EPD/xml/','display-eink-e2271cs021' : 'CY8CKIT-028-EPD/display-eink-e2271cs021/xml/','rgb-led' : 'rgb-led/xml/',
 'CY8CKIT-028-TFT' : 'CY8CKIT-028-TFT/xml/', 'CY8CKIT-032' : 'CY8CKIT-032/xml/','clib-support' : 'clib-support/xml/' ,'emwin' : 'emwin/xml',
 'display-tft-st7789v' : 'CY8CKIT-028-TFT/display-tft-st7789v/xml/', 'sensor-light' : 'CY8CKIT-028-TFT/sensor-light/xml/', 
 'wifi-mw-core' : 'wifi-mw-core/xml/' 
}

html_theme_options = {
    'display_version': False,
    'prev_next_buttons_location': 'bottom',
    # Toc options
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 6,
    'includehidden': True,
    'titles_only': False
}



breathe_default_project = 'csdadc'\

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


html_logo = '_static/image/IFX_LOGO_RGB.svg'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = '_static/image/IFX_ICON.ico'

def setup(app):
#    app.add_css_file('css/custom.css')
    app.add_css_file('css/theme_overrides.css')
    app.add_js_file('js/custom.js')
    app.add_js_file('js/searchtools.js')
    app.add_js_file('js/hotjar.js')
    app.add_js_file('js/sajari.js')
    