==================
BSP API Reference
==================

.. toctree::
   :hidden:

   group__group__bsp__settings.rst
   group__group__bsp__pin__state.rst
   group__group__bsp__macros.rst
   group__group__bsp__functions.rst

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         The following provides a list of BSP API documentation

      .. container:: directory

          \ `BSP Settings <group__group__bsp__settings.html>`__

.. container:: category

   Peripheral Default HAL Settings:

Resource
Parameter
Value
Remarks
UART
Flow control
No flow control
Data format
8N1
Baud rate
115200

 \ `Pin States <group__group__bsp__pin__state.html>`__

 \ `Macros <group__group__bsp__macros.html>`__

 \ `Functions <group__group__bsp__functions.html>`__


