=============
BSP Settings
=============

.. doxygengroup:: group_bsp_settings
   :project: TARGET_PSVP-PSOC4AS4
   :members:
   :protected-members:
   :private-members:
   :undoc-members: