======================================================
Cypress CSDADC Middleware Library 2.0: Data Structures
======================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CSDADC Middleware      |
      |                                   |    Library 2.0                    |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: headertitle

   .. container:: contents

      .. container:: textblock

         Here are the data structures with brief descriptions:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  C\ `cy                          | CSDADC pin structure             |
         | _stc_csdadc_ch_pin_t <structcy__ |                                  |
         | stc__csdadc__ch__pin__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `c                           | CSDADC configuration structure   |
         | y_stc_csdadc_config_t <structcy_ |                                  |
         | _stc__csdadc__config__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+
         |  C\ `cy_                         | CSDADC context structure that    |
         | stc_csdadc_context_t <structcy__ | contains the internal driver     |
         | stc__csdadc__context__t.html>`__ | data for the CSDADC MW           |
         +----------------------------------+----------------------------------+
         |  C\ `c                           | CSDADC result structure          |
         | y_stc_csdadc_result_t <structcy_ |                                  |
         | _stc__csdadc__result__t.html>`__ |                                  |
         +----------------------------------+----------------------------------+

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
