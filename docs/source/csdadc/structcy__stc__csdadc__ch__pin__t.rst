==============================
cy_stc_csdadc_ch_pin_t Struct
==============================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         CSDADC pin structure.

         This structure contains information about Port/Pin assignment
         of a particular CSDADC channel. The CSDADC channel can be
         assigned to any GPIO that supports the static connection to the
         AMUX-B (refer to the particular device datasheet for
         information).

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

GPIO_PRT_Type \* 
`ioPcPtr <structcy__stc__csdadc__ch__pin__t.html#a779f03fc5a410ad80fce366739f2641c>`__
 
Pointer to channel IO PC register.
 
uint8_t 
`pin <structcy__stc__csdadc__ch__pin__t.html#abd34cbae8a1162d22e4619f031b5c80f>`__
 
Channel IO pin.

