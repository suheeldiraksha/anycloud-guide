===============================
cy_stc_csdadc_context_t Struct
===============================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         CSDADC context structure that contains the internal driver data
         for the CSDADC MW.

         The context structure should be allocated by the user and
         passed to all CSDADC MW functions.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

`cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html>`__ 

`cfgCopy <structcy__stc__csdadc__context__t.html#a4f8d78cb554f542f9a75092dd0e74e66>`__

 

| Configuration structure copy.

 

`cy_stc_csdadc_result_t <structcy__stc__csdadc__result__t.html>`__ 

`adcResult <structcy__stc__csdadc__context__t.html#a6e383bc56a5d2f62797f6c403f1eeba7>`__
[`CY_CSDADC_MAX_CHAN_NUM <group__group__csdadc__macros.html#ga7a2c27abd3dd05754a37d9accbc7fa49>`__]

 

| CSDADC result array.

 

`cy_csdadc_callback_t <group__group__csdadc__data__structures.html#gae57e5fc50d39777f67c3cb8d9c176315>`__ 

`ptrEOCCallback <structcy__stc__csdadc__context__t.html#a9eabf78f640fbc46f50676cb1678d2f8>`__

 

| Pointer to a user's End Of Conversion callback function.
  `More... <#a9eabf78f640fbc46f50676cb1678d2f8>`__

 

uint32_t 

`chMask <structcy__stc__csdadc__context__t.html#ae0f76d5f90a88c848185cb498b07a8c6>`__

 

| Active mask of channels to convert.

 

uint32_t 

`counter <structcy__stc__csdadc__context__t.html#aefe62db3203b9c75ad63dc2b86151c34>`__

 

| Counter for CSDADC operations:
  `More... <#aefe62db3203b9c75ad63dc2b86151c34>`__

 

volatile uint16_t 

`status <structcy__stc__csdadc__context__t.html#a640ef5a8ad40a654cedf8e9ab3304c0a>`__

 

| Current CSDADC status:
  `More... <#a640ef5a8ad40a654cedf8e9ab3304c0a>`__

 

uint16_t 

`codeMax <structcy__stc__csdadc__context__t.html#aab3a9f8d792737f5df397ea8d76dbd2d>`__

 

| Max CSDADC code value.

 

uint16_t 

`vMaxMv <structcy__stc__csdadc__context__t.html#abdf04370aa730b05d3065a202a4e2556>`__

 

| Max CSDADC input voltage in mV.

 

uint16_t 

`tFull <structcy__stc__csdadc__context__t.html#ad736c4241f7fb1e4dbda9d069607587b>`__

 

| Calibration data.

 

uint16_t 

`tVssa2Vref <structcy__stc__csdadc__context__t.html#a0ad124d61469eb01715dcf0da463a47b>`__

 

| Calibration data.

 

uint16_t 

`tVdda2Vref <structcy__stc__csdadc__context__t.html#a5eeca82a30d56501d957e2c9814bd5ff>`__

 

| Calibration data.

 

uint16_t 

`tRecover <structcy__stc__csdadc__context__t.html#abe88be29090b4cb9706d59ef884bba56>`__

 

| Calibration data.

 

uint16_t 

`vddaMv <structcy__stc__csdadc__context__t.html#abdbf95d6c9d2929fa6b8b3230df8880c>`__

 

| Measured Vdda voltage in mV.

 

uint16_t 

`vBusBMv <structcy__stc__csdadc__context__t.html#a75d456b25b6f157cf69064c9f8bf4423>`__

 

| Measured voltage of the analog muxbusB in mV.

 

uint16_t 

`vRefMv <structcy__stc__csdadc__context__t.html#a57dd55db68b8df92fee8a535b5de89c7>`__

 

| Vref value in mV.

 

uint8_t 

`vRefGain <structcy__stc__csdadc__context__t.html#a71583fa394151889778b58d0b0d94f11>`__

 

| Vref gain.

 

uint8_t 

`activeCh <structcy__stc__csdadc__context__t.html#a5ae94561284c9ae3b7be9c0fda75c1f2>`__

 

| ID of the channel is being converted:
  `More... <#a5ae94561284c9ae3b7be9c0fda75c1f2>`__

 

uint8_t 

`snsClkDivider <structcy__stc__csdadc__context__t.html#a157f4e14e95c451c10254bc2b5199284>`__

 

| Divider of sense clock.

 

uint8_t 

`acqCycles <structcy__stc__csdadc__context__t.html#ac7fe40ef8aa327f04cbd243b4dabdd3d>`__

 

| Acquisition time in Sns cycles.

 

uint8_t 

`azCycles <structcy__stc__csdadc__context__t.html#a885b36110ce22b1cfd674355ff74e9ac>`__

 

| Auto-zero time in in Sns cycles.

 

**Field Documentation**

`◆  <#a9eabf78f640fbc46f50676cb1678d2f8>`__\ ptrEOCCallback
===========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_csdadc_callback_t <group__group__                                |
      | csdadc__data__structures.html#gae57e5fc50d39777f67c3cb8d9c176315>`__ |
      | cy_stc_csdadc_context_t::ptrEOCCallback                              |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Pointer to a user's End Of Conversion callback function.

      Refer to `Callback <group__group__csdadc__callback.html>`__
      section

`◆  <#aefe62db3203b9c75ad63dc2b86151c34>`__\ counter
====================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------+
      | uint32_t cy_stc_csdadc_context_t::counter |
      +-------------------------------------------+

   .. container:: memdoc

      Counter for CSDADC operations:

      -  bit [0:26] - current enabled channels cycle number:

         -  In the continuous mode, sets to 0 u with the conversion
            start and increments with every enabled channel cycle
         -  In the single shot mode, is equal to 0 u

      -  bits [27:31] - current channel number inside the current cycle

`◆  <#a640ef5a8ad40a654cedf8e9ab3304c0a>`__\ status
===================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------------------------------+
      | volatile uint16_t cy_stc_csdadc_context_t::status |
      +---------------------------------------------------+

   .. container:: memdoc

      Current CSDADC status:

      -  bit [0] - if set to 1, then CSDADC initialization is done
      -  bit [1] - 0 single shot mode, 1 continuous mode
      -  bit [2] - 0 CSDADC idle state, 1 CSDADC busy state
      -  bit [3] - 0 ADC_RESULT did not overflow, 1 ADC_RESULT overflow
      -  bit [4:7] - FSM status:

         -  0 - CY_CSDADC_STATUS_FSM_IDLE
         -  1 - CY_CSDADC_STATUS_CALIBPH1
         -  2 - CY_CSDADC_STATUS_CALIBPH2
         -  3 - CY_CSDADC_STATUS_CALIBPH3
         -  4 - CY_CSDADC_STATUS_CONVERTING

      -  bit [9] - stop conversion mode

         -  0 - stop after current channel conversion
         -  1 - stop after all enabled channels in chMask

`◆  <#a5ae94561284c9ae3b7be9c0fda75c1f2>`__\ activeCh
=====================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------+
      | uint8_t cy_stc_csdadc_context_t::activeCh |
      +-------------------------------------------+

   .. container:: memdoc

      ID of the channel is being converted:

      -  bit [0:4] - ID of current measured channel;
      -  CY_CSDADC_NO_CHANNEL - no active channel.

