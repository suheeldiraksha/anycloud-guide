==============================
cy_stc_csdadc_config_t Struct
==============================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         CSDADC configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

const
`cy_stc_csdadc_ch_pin_t <structcy__stc__csdadc__ch__pin__t.html>`__ \* 

`ptrPinList <structcy__stc__csdadc__config__t.html#af0839917f36aa586434fd2c07a042043>`__

 

| Array of pointers to the channel IO structures.

 

CSD_Type \* 

`base <structcy__stc__csdadc__config__t.html#a1f7d7b2ae9bf58c2ed4754ee1ef130cc>`__

 

| Pointer to the CSD HW Block.

 

cy_stc_csd_context_t \* 

`csdCxtPtr <structcy__stc__csdadc__config__t.html#ac22807f5b6caf29aaef89bbed27ade3e>`__

 

| Pointer to the CSD context driver.

 

uint32_t 

`cpuClk <structcy__stc__csdadc__config__t.html#a9fca865bbb6a6eac1d74f4eca5730011>`__

 

| CPU Clock in Hz.

 

uint32_t 

`periClk <structcy__stc__csdadc__config__t.html#a741a2776e5dd698eaa9816cce8192f87>`__

 

| Peri Clock in Hz.

 

int16_t 

`vref <structcy__stc__csdadc__config__t.html#a6ad9e565c92f9e2cfda8018ce151bd19>`__

 

| Voltage Reference in mV.

 

uint16_t 

`vdda <structcy__stc__csdadc__config__t.html#a70dcda504cca76fccc718cfe567cb643>`__

 

| Analog Power Voltage in mV.

 

uint16_t 

`calibrInterval <structcy__stc__csdadc__config__t.html#a5f3794db656b8dcabeec9314cc8c1056>`__

 

| Interval for auto-calibration.
  `More... <#a5f3794db656b8dcabeec9314cc8c1056>`__

 

`cy_en_csdadc_range_t <group__group__csdadc__enums.html#ga9a51f670bcf385f7f2cbaa3f1491cc7f>`__ 

`range <structcy__stc__csdadc__config__t.html#a65ce5b914a728dd7f11623342392a18c>`__

 

| Mode of ADC operation.

 

`cy_en_csdadc_resolution_t <group__group__csdadc__enums.html#ga53be63f99ad2c3e775496df7c8957bc7>`__ 

`resolution <structcy__stc__csdadc__config__t.html#a5419abcb7049711f0a174ec7dcfee78b>`__

 

| Resolution.

 

cy_en_divider_types_t 

`periDivTyp <structcy__stc__csdadc__config__t.html#a8eb894da889de622b62002b79658a52f>`__

 

| Peri Clock divider type.

 

uint8_t 

`numChannels <structcy__stc__csdadc__config__t.html#a23dde4c610164db57e735247cae2a614>`__

 

| Number of ADC channels.

 

uint8_t 

`idac <structcy__stc__csdadc__config__t.html#a0eb0a009aefebab573606ac86659d18c>`__

 

| IDAC code.

 

uint8_t 

`operClkDivider <structcy__stc__csdadc__config__t.html#ad52ccca632d93990d9d823558f620984>`__

 

| Divider of Peri Clock frequency for the CSDADC operation clock.

 

uint8_t 

`azTime <structcy__stc__csdadc__config__t.html#ab2acac32d7240e0c4ca0e1656d64802d>`__

 

| CSDADC auto-zero time in us.

 

uint8_t 

`acqTime <structcy__stc__csdadc__config__t.html#ad90f7ff0f0609800d03664c9d318e119>`__

 

| CSDADC acquisition time in us.

 

uint8_t 

`csdInitTime <structcy__stc__csdadc__config__t.html#a4d4ba449bf0340bb424d18b4c345f24b>`__

 

| CSD HW Block Initialization time in us.

 

uint8_t 

`idacCalibrationEn <structcy__stc__csdadc__config__t.html#aae6659775d8a017da3672160ba7de9bf>`__

 

| Enables run-time IDAC calibration.
  `More... <#aae6659775d8a017da3672160ba7de9bf>`__

 

uint8_t 

`periDivInd <structcy__stc__csdadc__config__t.html#aaa975ceb0fc31d67a1c4190a06ecd374>`__

 

| Peri Clock divider index.

 

**Field Documentation**

`◆  <#a5f3794db656b8dcabeec9314cc8c1056>`__\ calibrInterval
===========================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------------+
      | uint16_t cy_stc_csdadc_config_t::calibrInterval |
      +-------------------------------------------------+

   .. container:: memdoc

      Interval for auto-calibration.

      In this version not supported

`◆  <#aae6659775d8a017da3672160ba7de9bf>`__\ idacCalibrationEn
==============================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------------------------------+
      | uint8_t cy_stc_csdadc_config_t::idacCalibrationEn |
      +---------------------------------------------------+

   .. container:: memdoc

      Enables run-time IDAC calibration.

      In this version not supported


