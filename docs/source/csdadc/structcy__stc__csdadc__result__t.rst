==============================
cy_stc_csdadc_result_t Struct
==============================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         CSDADC result structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`code <structcy__stc__csdadc__result__t.html#a6b22704dfb24e5af0435277b3dc27bd1>`__
 
Channel conversion result as ADC code.
 
uint16_t 
`mVolts <structcy__stc__csdadc__result__t.html#a990e2904dba2fa3e32618d95f35223ee>`__
 
Channel conversion result as input voltage in mV.

