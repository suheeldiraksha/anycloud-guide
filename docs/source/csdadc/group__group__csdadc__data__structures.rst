================
Data Structures
================

   
.. toctree::

   structcy__stc__csdadc__ch__pin__t.rst
   structcy__stc__csdadc__config__t.rst
   structcy__stc__csdadc__result__t.rst
   structcy__stc__csdadc__context__t.rst
   
   
.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Structures <#nested-classes>`__ \|
         `Typedefs <#typedef-members>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: General Description
         :name: general-description
         :class: groupheader

      Describes the data structures defined by the CSDADC.

      The CSDADC MW use structures for the input channel pins,
      conversion results, MW configuration and context. The pin
      structure is included into the configuration structure and both
      can be defined by the user with the CSD personality in Device
      Configurator or manually if the user does not use ModusToolbox.
      The result structure is included into the context structure and
      contains voltages and ADC codes for all 32 input channels of more
      recent conversions. Besides the result structure, the context
      structure contains a copy of the configuration structure, the
      current CSDADC MW state data and calibration data. The context
      structure is allocated by the user and passed to all CSDADC MW
      functions. The CSDADC MW structure sizes are shown in the table
      below:

      +---------------------------------------+-----------------------------+
      | Structure                             | Size in bytes (w/o padding) |
      +=======================================+=============================+
      | `cy_stc_csdadc_ch_pin_t <struc        | 5                           |
      | tcy__stc__csdadc__ch__pin__t.html>`__ |                             |
      +---------------------------------------+-----------------------------+
      | `cy_stc_csdadc_config_t <stru         | 157                         |
      | ctcy__stc__csdadc__config__t.html>`__ |                             |
      +---------------------------------------+-----------------------------+
      | `cy_stc_csdadc_result_t <stru         | 4                           |
      | ctcy__stc__csdadc__result__t.html>`__ |                             |
      +---------------------------------------+-----------------------------+
      | `cy_stc_csdadc_context_t <struc       | 322                         |
      | tcy__stc__csdadc__context__t.html>`__ |                             |
      +---------------------------------------+-----------------------------+

      .. rubric::  Data Structures
         :name: data-structures
         :class: groupheader

struct  

`cy_stc_csdadc_ch_pin_t <structcy__stc__csdadc__ch__pin__t.html>`__

 

| CSDADC pin structure.
  `More... <structcy__stc__csdadc__ch__pin__t.html#details>`__

 

struct  

`cy_stc_csdadc_config_t <structcy__stc__csdadc__config__t.html>`__

 

| CSDADC configuration structure.
  `More... <structcy__stc__csdadc__config__t.html#details>`__

 

struct  

`cy_stc_csdadc_result_t <structcy__stc__csdadc__result__t.html>`__

 

| CSDADC result structure.
  `More... <structcy__stc__csdadc__result__t.html#details>`__

 

struct  

`cy_stc_csdadc_context_t <structcy__stc__csdadc__context__t.html>`__

 

| CSDADC context structure that contains the internal driver data for
  the CSDADC MW.
  `More... <structcy__stc__csdadc__context__t.html#details>`__

 

Typedefs
========

typedef void(* 

`cy_csdadc_callback_t <group__group__csdadc__data__structures.html#gae57e5fc50d39777f67c3cb8d9c176315>`__)
(void \*ptrCxt)

 

| Provides the typedef for the callback function that is intended to be
  called when the "End Of Conversion" cycle callback event occurs.

