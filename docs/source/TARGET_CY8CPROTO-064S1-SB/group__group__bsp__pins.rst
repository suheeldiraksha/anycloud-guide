=============
Pin Mappings
=============

.. doxygengroup:: group_bsp_bsp_pins
   :project: TARGET_CY8CPROTO-064S1-SB
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

.. toctree::

   group__group__bsp__pins__led.rst
   group__group__bsp__pins__btn.rst
   group__group__bsp__pins__comm.rst