=================================================================
Cypress CapSense Middleware Library 2.10: Data Fields - Variables
=================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

       
      .. rubric:: - t -
         :name: t--

      -  testResultMask :
         `cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html#a85203f9070ae007e98d8e86b716ea971>`__
      -  tfscContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a56adc5033fdd287a8fefb4a6c8b1a251>`__
      -  tfslContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a3e8c82cbc4289674f444114478fc1a7b>`__
      -  tfzmContext :
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#abdb25e19025b77d4115b551bf54454b5>`__
      -  timestamp :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a770a1fafdbe45e1e3355abca3caa859c>`__
         ,
         `cy_stc_capsense_gesture_context_t <structcy__stc__capsense__gesture__context__t.html#a2a24ea7e25b9910d7f02e7e19e43195a>`__
      -  timestampInterval :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a6178b4758bc7095bbafe2bf8bc62783a>`__
      -  touchNumber :
         `cy_stc_capsense_ballistic_context_t <structcy__stc__capsense__ballistic__context__t.html#a8b2a6fe6bc8bf68463bf94a93c7e3a2b>`__
      -  touchStartPosition1 :
         `cy_stc_capsense_ofcd_context_t <structcy__stc__capsense__ofcd__context__t.html#aa31dd67b76a421f313721950384c14ab>`__
         ,
         `cy_stc_capsense_ofdc_context_t <structcy__stc__capsense__ofdc__context__t.html#a22e58ee9031099dd6d48aee61a1aadc2>`__
         ,
         `cy_stc_capsense_ofes_context_t <structcy__stc__capsense__ofes__context__t.html#ac6dceb387c709e7425014e0b6cbba69d>`__
         ,
         `cy_stc_capsense_offl_context_t <structcy__stc__capsense__offl__context__t.html#aa4bcab6ceb424cae93b48af4ba2ef1c0>`__
         ,
         `cy_stc_capsense_ofrt_context_t <structcy__stc__capsense__ofrt__context__t.html#a3655e2f6e6fccd33a99ae0919965a02d>`__
         ,
         `cy_stc_capsense_ofsc_context_t <structcy__stc__capsense__ofsc__context__t.html#a9015721f99554c92a7dc46e2c21f94ed>`__
         ,
         `cy_stc_capsense_ofsl_context_t <structcy__stc__capsense__ofsl__context__t.html#ab6b900847f84996ed25c000469c6cba4>`__
         ,
         `cy_stc_capsense_tfsc_context_t <structcy__stc__capsense__tfsc__context__t.html#ac2bb0da57282c795cbf0f3d4c1c50e94>`__
         ,
         `cy_stc_capsense_tfsl_context_t <structcy__stc__capsense__tfsl__context__t.html#aaf2bf408c8cea2a3ad8331676a487aa3>`__
         ,
         `cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html#a60cf7f1c4af96f6113fbe04db092a80e>`__
      -  touchStartPosition2 :
         `cy_stc_capsense_tfsc_context_t <structcy__stc__capsense__tfsc__context__t.html#a8b6bd5906acd0001b1ceae797db7a8a3>`__
         ,
         `cy_stc_capsense_tfsl_context_t <structcy__stc__capsense__tfsl__context__t.html#abda60903e50cdc56ca4bc60476c2fedf>`__
         ,
         `cy_stc_capsense_tfzm_context_t <structcy__stc__capsense__tfzm__context__t.html#a47690ba09642262dc60c9996471cf3a7>`__
      -  touchStartTime1 :
         `cy_stc_capsense_ofcd_context_t <structcy__stc__capsense__ofcd__context__t.html#adac662a3fdcbff352d05b47127722437>`__
         ,
         `cy_stc_capsense_ofdc_context_t <structcy__stc__capsense__ofdc__context__t.html#a4301e8f87ce8a40ecc73c415ebd56eab>`__
         ,
         `cy_stc_capsense_ofes_context_t <structcy__stc__capsense__ofes__context__t.html#aaf7cec8bf10a12f82431b1cfdc76fa3c>`__
         ,
         `cy_stc_capsense_offl_context_t <structcy__stc__capsense__offl__context__t.html#a3c51b81c7a63872b85d1abafc9f3e35d>`__
         ,
         `cy_stc_capsense_ofsc_context_t <structcy__stc__capsense__ofsc__context__t.html#a659fadd332317157ed92646ab617c108>`__
         ,
         `cy_stc_capsense_tfsc_context_t <structcy__stc__capsense__tfsc__context__t.html#a8c266f5d2b658bb7e3fa61d33c02d02e>`__
      -  touchStartTime2 :
         `cy_stc_capsense_tfsc_context_t <structcy__stc__capsense__tfsc__context__t.html#ad05feec06a31053de7213da66168c4ff>`__
      -  tunerCmd :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#afae9853b830d6549b35bafc214acc040>`__
      -  tunerCnt :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#ac6502f0565b37d6ba97cce1c81a773b3>`__
      -  tunerSt :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a26f741c17c454d43f6a07dbf87d8154e>`__
      -  twoFingersEn :
         `cy_stc_capsense_advanced_centroid_config_t <structcy__stc__capsense__advanced__centroid__config__t.html#a541c1a949ee1bb818933bbb95f153ada>`__
      -  txIndex :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#a61757766f7b312242d258d584b8f62a9>`__
      -  type :
         `cy_stc_capsense_electrode_config_t <structcy__stc__capsense__electrode__config__t.html#a3f987328f5df9cd9fe4f6b4daf80ec8e>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
