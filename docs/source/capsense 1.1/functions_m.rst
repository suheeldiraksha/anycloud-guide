=====================================================
Cypress CapSense Middleware Library 2.10: Data Fields
=====================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented struct and union fields with
         links to the struct/union documentation for each field:

      .. rubric:: - m -
         :name: m--

      -  markIndicesMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a895f5397bf9d29ad2c0cbd4fadb111fe>`__
      -  maxK :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#a917e444f748280dc2e325f062600e878>`__
      -  maxRawCount :
         `cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html#a5bacc4b698a7982c3fb687a22b9e3287>`__
      -  mfsChannelIndex :
         `cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html#a159ba1e2d4ff19b16d0aa6695b8f1499>`__
      -  mfsEn :
         `cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html#a4c99796a489c0221243e079cc01ae1d3>`__
      -  minK :
         `cy_stc_capsense_adaptive_filter_config_t <structcy__stc__capsense__adaptive__filter__config__t.html#a8202d5f56b17bd01a1957a05bea2f489>`__
      -  minsMap :
         `cy_stc_capsense_csx_touch_buffer_t <structcy__stc__capsense__csx__touch__buffer__t.html#a4478e1addf6cd6f858c74a742a76f79f>`__
      -  modClk :
         `cy_stc_capsense_bist_custom_parameters_t <structcy__stc__capsense__bist__custom__parameters__t.html#aafb50e8ce6fad46c3a8b0224ce1714b6>`__
      -  modCsdClk :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a867653a126683191970d79f8b6005926>`__
      -  modCsxClk :
         `cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html#a7284658d282945e1ed7f624683677829>`__

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
