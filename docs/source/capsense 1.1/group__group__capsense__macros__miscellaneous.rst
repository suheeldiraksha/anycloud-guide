=====================
Miscellaneous Macros
=====================


.. doxygengroup:: group_capsense_macros_miscellaneous
   :project: capsense
   :members:
   :protected-members:
   :private-members:
   :undoc-members: