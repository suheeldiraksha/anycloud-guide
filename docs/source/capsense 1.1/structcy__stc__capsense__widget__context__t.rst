========================================
cy_stc_capsense_widget_context_t Struct
========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Widget context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 

`fingerCap <structcy__stc__capsense__widget__context__t.html#aaddfea3cc416d040629708d1723c12f6>`__

 

| Widget finger capacitance parameter used for the CSD widgets only when
  SmartSense is enabled.

 

uint16_t 

`sigPFC <structcy__stc__capsense__widget__context__t.html#a818a66cb212ab10f9e4fb4f25a5b8a14>`__

 

| The 75% of signal per user-defined finger capacitance.

 

uint16_t 

`resolution <structcy__stc__capsense__widget__context__t.html#a68e0c56527b8e16513c149bfc5adb623>`__

 

| Provides scan resolution for the CSD Widgets.
  `More... <#a68e0c56527b8e16513c149bfc5adb623>`__

 

uint16_t 

`maxRawCount <structcy__stc__capsense__widget__context__t.html#a5bacc4b698a7982c3fb687a22b9e3287>`__

 

| Calculated maximum raw count of widget.

 

uint16_t 

`fingerTh <structcy__stc__capsense__widget__context__t.html#ab1730b817a861922f4375527d7a1d3b4>`__

 

| Widget Finger Threshold.

 

uint16_t 

`proxTh <structcy__stc__capsense__widget__context__t.html#ac28a8ad287f401bde36886e1995ebd5d>`__

 

| Widget Proximity Threshold.

 

uint16_t 

`lowBslnRst <structcy__stc__capsense__widget__context__t.html#abcf89959f49d28d2d396d8beaae05710>`__

 

| The widget low baseline reset count.
  `More... <#abcf89959f49d28d2d396d8beaae05710>`__

 

uint16_t 

`snsClk <structcy__stc__capsense__widget__context__t.html#ac2c184d4476a32763d29fd3c1813d533>`__

 

| Sense Clock Divider. `More... <#ac2c184d4476a32763d29fd3c1813d533>`__

 

uint16_t 

`rowSnsClk <structcy__stc__capsense__widget__context__t.html#a2dd4e90d01407e1c2a0ed33f637e3b4f>`__

 

| Row Sense Clock Divider for the Matrix Buttons and Touchpad widgets.

 

uint16_t 

`gestureDetected <structcy__stc__capsense__widget__context__t.html#a1abcf6d1b22d6cc09006eb81576f81f6>`__

 

| Mask of detected gestures.

 

uint16_t 

`gestureDirection <structcy__stc__capsense__widget__context__t.html#a1bd21ada41343af3e4102b85ab62edab>`__

 

| Mask of directions of detected gestures.

 

int16_t 

`xDelta <structcy__stc__capsense__widget__context__t.html#a723b1a11630c801ad30546fd35cec715>`__

 

| The filtered by Ballistic Multiplier X-displacement.

 

int16_t 

`yDelta <structcy__stc__capsense__widget__context__t.html#a869b23837236045aba13b4fbde082e95>`__

 

| The filtered by Ballistic Multiplier Y-displacement.

 

uint8_t 

`noiseTh <structcy__stc__capsense__widget__context__t.html#a8bd80c8460e60bc9437e229329ce63fc>`__

 

| Widget Noise Threshold.

 

uint8_t 

`nNoiseTh <structcy__stc__capsense__widget__context__t.html#a6498ae887b0e0a6d8a174e79ed5aacf5>`__

 

| Widget Negative Noise Threshold.

 

uint8_t 

`hysteresis <structcy__stc__capsense__widget__context__t.html#a6db8e6c147c6b3da087fa96508d8704a>`__

 

| Widget Hysteresis for the signal crossing finger threshold.

 

uint8_t 

`onDebounce <structcy__stc__capsense__widget__context__t.html#a594bf2fb432dbeaa9b23545a9ff3d55a>`__

 

| Widget Debounce for the signal above the finger threshold 1 to 255.
  `More... <#a594bf2fb432dbeaa9b23545a9ff3d55a>`__

 

uint8_t 

`snsClkSource <structcy__stc__capsense__widget__context__t.html#ab9a639859b9bb45518c041e37bfca15a>`__

 

| Widget clock source: `More... <#ab9a639859b9bb45518c041e37bfca15a>`__

 

uint8_t 

`idacMod <structcy__stc__capsense__widget__context__t.html#acfd1cb8d218cf32a3d9466fa55a4065a>`__
[`CY_CAPSENSE_FREQ_CHANNELS_NUM <group__group__capsense__macros__settings.html#gaf7c608a0139fc64fce397a0ac580e191>`__]

 

| Sets the current of the modulation IDAC for the CSD widgets.
  `More... <#acfd1cb8d218cf32a3d9466fa55a4065a>`__

 

uint8_t 

`idacGainIndex <structcy__stc__capsense__widget__context__t.html#aca335177119844084a40aab5a8e4022e>`__

 

| Index of IDAC gain in table
  `cy_stc_capsense_idac_gain_table_t <structcy__stc__capsense__idac__gain__table__t.html>`__.

 

uint8_t 

`rowIdacMod <structcy__stc__capsense__widget__context__t.html#a685987ab8fcf308ba48669866de44274>`__
[`CY_CAPSENSE_FREQ_CHANNELS_NUM <group__group__capsense__macros__settings.html#gaf7c608a0139fc64fce397a0ac580e191>`__]

 

| Sets the current of the modulation IDAC for the row sensors for the
  CSD Touchpad and Matrix Button widgets.
  `More... <#a685987ab8fcf308ba48669866de44274>`__

 

uint8_t 

`bslnCoeff <structcy__stc__capsense__widget__context__t.html#abc22672d34d27dc281a8401b7b94544f>`__

 

| Baseline IIR filter coefficient.
  `More... <#abc22672d34d27dc281a8401b7b94544f>`__

 

uint8_t 

`status <structcy__stc__capsense__widget__context__t.html#ad7b0478d50436b8f1cdb982b5d731a68>`__

 

| Contains masks: `More... <#ad7b0478d50436b8f1cdb982b5d731a68>`__

 

`cy_stc_capsense_touch_t <structcy__stc__capsense__touch__t.html>`__ 

`wdTouch <structcy__stc__capsense__widget__context__t.html#a8278c09fe62e33e5e460f3064b90e614>`__

 

| Widget touch structure used for Matrix Buttons, Sliders, and
  Touchpads.

 

Field Documentation
--------------------

`◆  <#a68e0c56527b8e16513c149bfc5adb623>`__\ resolution
=======================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------------------+
      | uint16_t cy_stc_capsense_widget_context_t::resolution |
      +-------------------------------------------------------+

   .. container:: memdoc

      Provides scan resolution for the CSD Widgets.

      Provides number of sub-conversions for the CSX Widgets

`◆  <#abcf89959f49d28d2d396d8beaae05710>`__\ lowBslnRst
=======================================================

.. container:: memitem

   .. container:: memproto

      +-------------------------------------------------------+
      | uint16_t cy_stc_capsense_widget_context_t::lowBslnRst |
      +-------------------------------------------------------+

   .. container:: memdoc

      The widget low baseline reset count.

      Specifies the number of samples the sensor signal must be below
      the Negative Noise Threshold
      `nNoiseTh <structcy__stc__capsense__widget__context__t.html#a6498ae887b0e0a6d8a174e79ed5aacf5>`__
      to trigger a baseline reset

`◆  <#ac2c184d4476a32763d29fd3c1813d533>`__\ snsClk
===================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------------------------------+
      | uint16_t cy_stc_capsense_widget_context_t::snsClk |
      +---------------------------------------------------+

   .. container:: memdoc

      Sense Clock Divider.

      For the Matrix Buttons and Touchpad widgets specifies the column
      sense clock divider

`◆  <#a594bf2fb432dbeaa9b23545a9ff3d55a>`__\ onDebounce
=======================================================

.. container:: memitem

   .. container:: memproto

      +------------------------------------------------------+
      | uint8_t cy_stc_capsense_widget_context_t::onDebounce |
      +------------------------------------------------------+

   .. container:: memdoc

      Widget Debounce for the signal above the finger threshold 1 to
      255.

      -  1 - touch reported immediately as soon as detected
      -  2 - touch reported on the second consecutive detection
      -  3 - touch reported on the third consecutive detection

`◆  <#ab9a639859b9bb45518c041e37bfca15a>`__\ snsClkSource
=========================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------------+
      | uint8_t cy_stc_capsense_widget_context_t::snsClkSource |
      +--------------------------------------------------------+

   .. container:: memdoc

      Widget clock source:

      -  bit[7] - Indicates auto mode of clock source selection
      -  bit[0:6] - Clock source:

         -  0 - Direct (CY_CAPSENSE_CLK_SOURCE_DIRECT)
         -  1 - SSC6 (CY_CAPSENSE_CLK_SOURCE_SSC6)
         -  2 - SSC7 (CY_CAPSENSE_CLK_SOURCE_SSC7)
         -  3 - SSC9 (CY_CAPSENSE_CLK_SOURCE_SSC9)
         -  4 - SSC10 (CY_CAPSENSE_CLK_SOURCE_SSC10)
         -  5 - PRS8 (CY_CAPSENSE_CLK_SOURCE_PRS8)
         -  6 - PRS12 (CY_CAPSENSE_CLK_SOURCE_PRS12)

`◆  <#acfd1cb8d218cf32a3d9466fa55a4065a>`__\ idacMod
====================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | uint8_t                                                              |
      | cy_stc_capsense_widget_                                              |
      | context_t::idacMod[`CY_CAPSENSE_FREQ_CHANNELS_NUM <group__group__cap |
      | sense__macros__settings.html#gaf7c608a0139fc64fce397a0ac580e191>`__] |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Sets the current of the modulation IDAC for the CSD widgets.

      For the CSD Touchpad and Matrix Button widgets sets the current of
      the modulation IDAC for the column sensors. Not used for the CSX
      widgets.

`◆  <#a685987ab8fcf308ba48669866de44274>`__\ rowIdacMod
=======================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | uint8_t                                                              |
      | cy_stc_capsense_widget_con                                           |
      | text_t::rowIdacMod[`CY_CAPSENSE_FREQ_CHANNELS_NUM <group__group__cap |
      | sense__macros__settings.html#gaf7c608a0139fc64fce397a0ac580e191>`__] |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Sets the current of the modulation IDAC for the row sensors for
      the CSD Touchpad and Matrix Button widgets.

      Not used for the CSX widgets.

`◆  <#abc22672d34d27dc281a8401b7b94544f>`__\ bslnCoeff
======================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------------------+
      | uint8_t cy_stc_capsense_widget_context_t::bslnCoeff |
      +-----------------------------------------------------+

   .. container:: memdoc

      Baseline IIR filter coefficient.

      Lower value leads to higher filtering.

`◆  <#ad7b0478d50436b8f1cdb982b5d731a68>`__\ status
===================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------+
      | uint8_t cy_stc_capsense_widget_context_t::status |
      +--------------------------------------------------+

   .. container:: memdoc

      Contains masks:

      -  bit[0] - Widget Active (CY_CAPSENSE_WD_ACTIVE_MASK)
      -  bit[1] - Widget Disabled (CY_CAPSENSE_WD_DISABLE_MASK)
      -  bit[2] - Widget Working (CY_CAPSENSE_WD_WORKING_MASK)


