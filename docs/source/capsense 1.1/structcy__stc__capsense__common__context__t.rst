========================================
cy_stc_capsense_common_context_t Struct
========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares top-level Context Data Structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 

`configId <structcy__stc__capsense__common__context__t.html#aa9d6aeaa1e73aac634d4ca090d353af2>`__

 

| 16-bit CRC calculated by the CapSense Configurator tool for the
  CapSense configuration.
  `More... <#aa9d6aeaa1e73aac634d4ca090d353af2>`__

 

uint16_t 

`tunerCmd <structcy__stc__capsense__common__context__t.html#afae9853b830d6549b35bafc214acc040>`__

 

| Tuner Command Register
  `cy_en_capsense_tuner_cmd_t <group__group__capsense__enums.html#ga049118d05a2bdf4f2d37d7b75c511f79>`__.
  `More... <#afae9853b830d6549b35bafc214acc040>`__

 

uint16_t 

`scanCounter <structcy__stc__capsense__common__context__t.html#a6406229ffd99b912c09ca0c80ea3f4ee>`__

 

| This counter increments after each scan.
  `More... <#a6406229ffd99b912c09ca0c80ea3f4ee>`__

 

uint8_t 

`tunerSt <structcy__stc__capsense__common__context__t.html#a26f741c17c454d43f6a07dbf87d8154e>`__

 

| State of CapSense middleware tuner module.
  `More... <#a26f741c17c454d43f6a07dbf87d8154e>`__

 

uint8_t 

`initDone <structcy__stc__capsense__common__context__t.html#a0181ebd06d8e03e3b18ac5c08ca14742>`__

 

| Keep information whether initialization was done or not.

 

`cy_capsense_callback_t <group__group__capsense__structures.html#ga908447ccc2720f31c861d9eb79ff75d2>`__ 

`ptrSSCallback <structcy__stc__capsense__common__context__t.html#ac3a7fcf3be3a2fc7491cdb9c002eb55e>`__

 

| Pointer to a user's Start Sample callback function.
  `More... <#ac3a7fcf3be3a2fc7491cdb9c002eb55e>`__

 

`cy_capsense_callback_t <group__group__capsense__structures.html#ga908447ccc2720f31c861d9eb79ff75d2>`__ 

`ptrEOSCallback <structcy__stc__capsense__common__context__t.html#ae5ddccd94ecc0ee71482f6c6cbba947f>`__

 

| Pointer to a user's End Of Scan callback function.
  `More... <#ae5ddccd94ecc0ee71482f6c6cbba947f>`__

 

`cy_capsense_tuner_send_callback_t <group__group__capsense__structures.html#gabf58e6ce283cb73f9b8a398abf6bad17>`__ 

`ptrTunerSendCallback <structcy__stc__capsense__common__context__t.html#ae0c310162f4df691e7d5ac8ec7937491>`__

 

| Pointer to a user's tuner callback function.
  `More... <#ae0c310162f4df691e7d5ac8ec7937491>`__

 

`cy_capsense_tuner_receive_callback_t <group__group__capsense__structures.html#gade1f00c852892b0fc498c9a3b9d28594>`__ 

`ptrTunerReceiveCallback <structcy__stc__capsense__common__context__t.html#a18d43c62af42453bbd7a41778691a7e2>`__

 

| Pointer to a user's tuner callback function.
  `More... <#a18d43c62af42453bbd7a41778691a7e2>`__

 

volatile uint32_t 

`status <structcy__stc__capsense__common__context__t.html#ae823664536f36878278584c2e8967035>`__

 

| Middleware status information, scan in progress or not.

 

uint32_t 

`timestampInterval <structcy__stc__capsense__common__context__t.html#a6178b4758bc7095bbafe2bf8bc62783a>`__

 

| Timestamp interval used at increasing the timestamp by
  `Cy_CapSense_IncrementGestureTimestamp() <group__group__capsense__high__level.html#ga904eb69e694e4daf45bc778281b39d9e>`__

 

uint32_t 

`timestamp <structcy__stc__capsense__common__context__t.html#a770a1fafdbe45e1e3355abca3caa859c>`__

 

| Current timestamp should be kept updated and operational, which is
  vital for the operation of Gesture and Ballistic multiplier features.

 

uint8_t 

`modCsdClk <structcy__stc__capsense__common__context__t.html#a867653a126683191970d79f8b6005926>`__

 

| The modulator clock divider for the CSD widgets.

 

uint8_t 

`modCsxClk <structcy__stc__capsense__common__context__t.html#a7284658d282945e1ed7f624683677829>`__

 

| The modulator clock divider for the CSX widgets.

 

uint8_t 

`tunerCnt <structcy__stc__capsense__common__context__t.html#ac6502f0565b37d6ba97cce1c81a773b3>`__

 

| Command counter of CapSense middleware tuner module.

 

Field Documentation
--------------------

`◆  <#aa9d6aeaa1e73aac634d4ca090d353af2>`__\ configId
=====================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------------------+
      | uint16_t cy_stc_capsense_common_context_t::configId |
      +-----------------------------------------------------+

   .. container:: memdoc

      16-bit CRC calculated by the CapSense Configurator tool for the
      CapSense configuration.

      Used by the CapSense Tuner tool to identify if the FW corresponds
      to the specific user configuration.

`◆  <#afae9853b830d6549b35bafc214acc040>`__\ tunerCmd
=====================================================

.. container:: memitem

   .. container:: memproto

      +-----------------------------------------------------+
      | uint16_t cy_stc_capsense_common_context_t::tunerCmd |
      +-----------------------------------------------------+

   .. container:: memdoc

      Tuner Command Register
      `cy_en_capsense_tuner_cmd_t <group__group__capsense__enums.html#ga049118d05a2bdf4f2d37d7b75c511f79>`__.

      Used for the communication between the CapSense Tuner tool and the
      middleware

`◆  <#a6406229ffd99b912c09ca0c80ea3f4ee>`__\ scanCounter
========================================================

.. container:: memitem

   .. container:: memproto

      +--------------------------------------------------------+
      | uint16_t cy_stc_capsense_common_context_t::scanCounter |
      +--------------------------------------------------------+

   .. container:: memdoc

      This counter increments after each scan.

`◆  <#a26f741c17c454d43f6a07dbf87d8154e>`__\ tunerSt
====================================================

.. container:: memitem

   .. container:: memproto

      +---------------------------------------------------+
      | uint8_t cy_stc_capsense_common_context_t::tunerSt |
      +---------------------------------------------------+

   .. container:: memdoc

      State of CapSense middleware tuner module.

      `cy_en_capsense_tuner_state_t <group__group__capsense__enums.html#gaadb1f5ee70c7d52a5375cdae765093dc>`__

`◆  <#ac3a7fcf3be3a2fc7491cdb9c002eb55e>`__\ ptrSSCallback
==========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_capsense_callback_t <group__gro                                  |
      | up__capsense__structures.html#ga908447ccc2720f31c861d9eb79ff75d2>`__ |
      | cy_stc_capsense_common_context_t::ptrSSCallback                      |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Pointer to a user's Start Sample callback function.

      Refer to `Callbacks <group__group__capsense__callbacks.html>`__
      section

`◆  <#ae5ddccd94ecc0ee71482f6c6cbba947f>`__\ ptrEOSCallback
===========================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_capsense_callback_t <group__gro                                  |
      | up__capsense__structures.html#ga908447ccc2720f31c861d9eb79ff75d2>`__ |
      | cy_stc_capsense_common_context_t::ptrEOSCallback                     |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Pointer to a user's End Of Scan callback function.

      Refer to `Callbacks <group__group__capsense__callbacks.html>`__
      section

`◆  <#ae0c310162f4df691e7d5ac8ec7937491>`__\ ptrTunerSendCallback
=================================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_capsense_tuner_send_callback_t <group__gro                       |
      | up__capsense__structures.html#gabf58e6ce283cb73f9b8a398abf6bad17>`__ |
      | cy_stc_capsense_common_context_t::ptrTunerSendCallback               |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Pointer to a user's tuner callback function.

      Refer to `Callbacks <group__group__capsense__callbacks.html>`__
      section

`◆  <#a18d43c62af42453bbd7a41778691a7e2>`__\ ptrTunerReceiveCallback
====================================================================

.. container:: memitem

   .. container:: memproto

      +----------------------------------------------------------------------+
      | `cy_capsense_tuner_receive_callback_t <group__gro                    |
      | up__capsense__structures.html#gade1f00c852892b0fc498c9a3b9d28594>`__ |
      | cy_stc_capsense_common_context_t::ptrTunerReceiveCallback            |
      +----------------------------------------------------------------------+

   .. container:: memdoc

      Pointer to a user's tuner callback function.

      Refer to `Callbacks <group__group__capsense__callbacks.html>`__
      section

