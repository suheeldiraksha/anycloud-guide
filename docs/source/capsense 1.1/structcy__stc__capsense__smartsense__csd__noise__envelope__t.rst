=======================================================
cy_stc_capsense_smartsense_csd_noise_envelope_t Struct
=======================================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares Noise envelope data structure for CSD widgets when
         SmartSense is enabled.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`param0 <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a05b74489afc68fe873e70519784f8763>`__
 
Parameter 0 configuration.
 
uint16_t 
`param1 <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#aa946864ff892df81a594e217ceba1d55>`__
 
Parameter 1 configuration.
 
uint16_t 
`param2 <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#abd13d4d25441135f993b97aa47e2c230>`__
 
Parameter 2 configuration.
 
uint16_t 
`param3 <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a366c60ce71fcb0fce32659a16fa09e45>`__
 
Parameter 3 configuration.
 
uint16_t 
`param4 <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a836e0975942aa929fabd0e74a8628e33>`__
 
Parameter 4 configuration.
 
uint8_t 
`param5 <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#aa10ef391278327840a384056d062ae41>`__
 
Parameter 5 configuration.
 
uint8_t 
`param6 <structcy__stc__capsense__smartsense__csd__noise__envelope__t.html#a45d5c40b50ac10537e35b7e55dcc88f4>`__
 
Parameter 6 configuration.


