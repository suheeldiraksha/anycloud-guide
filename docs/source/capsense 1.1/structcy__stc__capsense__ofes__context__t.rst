======================================
cy_stc_capsense_ofes_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture One Finger Edge Swipe context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`touchStartTime1 <structcy__stc__capsense__ofes__context__t.html#aaf7cec8bf10a12f82431b1cfdc76fa3c>`__
 
Touchdown time.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__ofes__context__t.html#ac6dceb387c709e7425014e0b6cbba69d>`__
 
Touchdown position.
 
uint8_t 
`state <structcy__stc__capsense__ofes__context__t.html#aceb2451dce6a58a27a57aad7567d96de>`__
 
Gesture state.
 
uint8_t 
`edge <structcy__stc__capsense__ofes__context__t.html#ac157aeb4721e704a38dff878b34f4bbf>`__
 
Detected edge.

