==========================================
cy_stc_capsense_auto_tune_config_t Struct
==========================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares HW SmartSense data structure for CSD widgets.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`sensorCap <structcy__stc__capsense__auto__tune__config__t.html#a1d0e22bde6cc10e84ce2e37524486fc1>`__
 
Sensor parasitic capacitance in fF 10^-15.
 
uint32_t 
`iDacGain <structcy__stc__capsense__auto__tune__config__t.html#aaa6b7ff2794e44d8f333ecf6048dd6f6>`__
 
IDAC gain in pA.
 
uint16_t \* 
`ptrSenseClk <structcy__stc__capsense__auto__tune__config__t.html#ad6aa32b14c98f74107b4230446e35fed>`__
 
Pointer to SnsClk divider.
 
uint16_t \* 
`sigPFC <structcy__stc__capsense__auto__tune__config__t.html#a02620f92d5236d731cfd30c52b3881c5>`__
 
Pointer to sigPFC value (Signal Per Finger Capacitance)
 
uint16_t 
`snsClkConstantR <structcy__stc__capsense__auto__tune__config__t.html#a6ec095fe5aeb9289f98829815219ea37>`__
 
Resistance in series to a sensor.
 
uint16_t 
`vRef <structcy__stc__capsense__auto__tune__config__t.html#a50522d0350068cbfba0aeebaff115d9c>`__
 
Vref in mVolts.
 
uint16_t 
`fingerCap <structcy__stc__capsense__auto__tune__config__t.html#a1cfd6c229a3feee37da2016b69128be8>`__
 
Finger capacitance in fF 10^-15 (Set in Basic tab in pF 10^-12)
 
uint16_t 
`snsClkInputClock <structcy__stc__capsense__auto__tune__config__t.html#a104c0df5a13ed859885dd8be3d274dc2>`__
 
Frequency for sense clock divider in kHz.
 
uint16_t 
`calTarget <structcy__stc__capsense__auto__tune__config__t.html#a48acefee1f92f23853cc7d58511327a2>`__
 
Calibration target in percentage.
 
uint8_t 
`iDacMod <structcy__stc__capsense__auto__tune__config__t.html#adbec8688c917a7a25a7cc4b14b1e49ed>`__
 
Modulation idac code.
 
uint8_t 
`iDacComp <structcy__stc__capsense__auto__tune__config__t.html#a7c937fca893ce53a81dd29d5541c2b75>`__
 
Compensation idac code.



