============================================================================================
Cypress CapSense Middleware Library 2.10: cy_stc_capsense_alp_fltr_config_t Struct Reference
============================================================================================

.. container::
   :name: top

   .. container::
      :name: titlearea

      +-----------------------------------+-----------------------------------+
      | |Logo|                            | .. container::                    |
      |                                   |    :name: projectname             |
      |                                   |                                   |
      |                                   |    Cypress CapSense Middleware    |
      |                                   |    Library 2.10                   |
      +-----------------------------------+-----------------------------------+

   .. container::
      :name: main-nav

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares ALP filter configuration structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint16_t 
`configParam0 <structcy__stc__capsense__alp__fltr__config__t.html#a1899bfbe169a38238c06904b75dede02>`__
 
Parameter 0 configuration.
 
uint16_t 
`configParam1 <structcy__stc__capsense__alp__fltr__config__t.html#a5c8856d048cb9985f4ce1f3f437d8408>`__
 
Parameter 1 configuration.
 
uint16_t 
`configParam2 <structcy__stc__capsense__alp__fltr__config__t.html#a591b9d314484d0279c3addcbbe3f334a>`__
 
Parameter 2 configuration.
 
uint8_t 
`configParam3 <structcy__stc__capsense__alp__fltr__config__t.html#ad3d404cffa7e3a5dc7a3f473fee0b2ab>`__
 
Parameter 3 configuration.
 
uint8_t 
`configParam4 <structcy__stc__capsense__alp__fltr__config__t.html#a6e088e048f665d7517d05ca7c1742832>`__
 
Parameter 4 configuration.
 
uint8_t 
`configParam5 <structcy__stc__capsense__alp__fltr__config__t.html#aea8c1e50bc2f69b60a0ac593571baeb7>`__
 
Parameter 5 configuration.
 

.. |Logo| image:: cypress_logo.png
   :target: http://www.cypress.com/
