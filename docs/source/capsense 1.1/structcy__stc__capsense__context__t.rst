=================================
cy_stc_capsense_context_t Struct
=================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Declares top-level CapSense context data structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

const
`cy_stc_capsense_common_config_t <structcy__stc__capsense__common__config__t.html>`__
\* 
`ptrCommonConfig <structcy__stc__capsense__context__t.html#a7e55b9983341848b270166d72d9b50ac>`__
 
Pointer to the common configuration structure.
 
`cy_stc_capsense_common_context_t <structcy__stc__capsense__common__context__t.html>`__
\* 
`ptrCommonContext <structcy__stc__capsense__context__t.html#a2f4f7d25d0816cf7fe6f04103c4d647e>`__
 
Pointer to the common context structure.
 
`cy_stc_capsense_internal_context_t <structcy__stc__capsense__internal__context__t.html>`__
\* 
`ptrInternalContext <structcy__stc__capsense__context__t.html#a9b84076b589b363d35097eb12420fd4b>`__
 
Pointer to the internal context structure.
 
const
`cy_stc_capsense_widget_config_t <structcy__stc__capsense__widget__config__t.html>`__
\* 
`ptrWdConfig <structcy__stc__capsense__context__t.html#a9b12863ecde08d94f68285a852e6f69b>`__
 
Pointer to the widget configuration structure.
 
`cy_stc_capsense_widget_context_t <structcy__stc__capsense__widget__context__t.html>`__
\* 
`ptrWdContext <structcy__stc__capsense__context__t.html#a1686259414950156dca1818d0db45922>`__
 
Pointer to the widget context structure.
 
const
`cy_stc_capsense_pin_config_t <structcy__stc__capsense__pin__config__t.html>`__
\* 
`ptrPinConfig <structcy__stc__capsense__context__t.html#a9f676c61d0088ba82b3d7adaea8459e0>`__
 
Pointer to the pin configuration structure.
 
const
`cy_stc_capsense_pin_config_t <structcy__stc__capsense__pin__config__t.html>`__
\* 
`ptrShieldPinConfig <structcy__stc__capsense__context__t.html#a2a2bab67c44107594f5c43c785b0cb2e>`__
 
Pointer to the shield pin configuration structure.
 
`cy_stc_active_scan_sns_t <structcy__stc__active__scan__sns__t.html>`__
\* 
`ptrActiveScanSns <structcy__stc__capsense__context__t.html#a2719dc3e059d012106c5f4476faec66d>`__
 
Pointer to the current active sensor structure.
 
const void \* 
`ptrFptrConfig <structcy__stc__capsense__context__t.html#a00787c47f768578185fc57e6035ff68a>`__
 
Pointer to the function pointers structure.
 
`cy_stc_capsense_bist_context_t <structcy__stc__capsense__bist__context__t.html>`__
\* 
`ptrBistContext <structcy__stc__capsense__context__t.html#a92125bdfe35272f254fcc58d857670ce>`__
 
Pointer to the BIST context structure.

