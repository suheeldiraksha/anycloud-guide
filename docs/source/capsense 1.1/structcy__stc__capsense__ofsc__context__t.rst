======================================
cy_stc_capsense_ofsc_context_t Struct
======================================

.. container:: ui-resizable side-nav-resizable
   :name: side-nav

   .. container::
      :name: nav-tree

      .. container::
         :name: nav-tree-contents

         .. container:: sync
            :name: nav-sync

   .. container:: ui-resizable-handle
      :name: splitbar

.. container::
   :name: doc-content

   .. container::
      :name: MSearchSelectWindow

   .. container::
      :name: MSearchResultsWindow

   .. container:: header

      .. container:: summary

         `Data Fields <#pub-attribs>`__

      .. container:: headertitle

   .. container:: contents

      .. rubric:: Description
         :name: description
         :class: groupheader

      .. container:: textblock

         Gesture One Finger Single Click context structure.

      .. rubric::  Data Fields
         :name: data-fields
         :class: groupheader

uint32_t 
`touchStartTime1 <structcy__stc__capsense__ofsc__context__t.html#a659fadd332317157ed92646ab617c108>`__
 
Touchdown time.
 
`cy_stc_capsense_gesture_position_t <structcy__stc__capsense__gesture__position__t.html>`__ 
`touchStartPosition1 <structcy__stc__capsense__ofsc__context__t.html#a9015721f99554c92a7dc46e2c21f94ed>`__
 
Touchdown position.
 
uint8_t 
`state <structcy__stc__capsense__ofsc__context__t.html#a8b9f3178a921e342a800d39f2c276911>`__
 
Gesture state.

