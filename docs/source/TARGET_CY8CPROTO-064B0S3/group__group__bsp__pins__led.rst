=========
LED Pins
=========

.. doxygengroup:: group_bsp_pins_led
   :project: TARGET_CY8CPROTO-064B0S3
   :members:
   :protected-members:
   :private-members:
   :undoc-members: